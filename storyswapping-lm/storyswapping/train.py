#!/usr/bin/env python
'''
Created on Apr 5, 2017

@author: reid
'''

import argparse
import storyswapping as ss
import logging
import numpy as np
import sys
import time

from keras.callbacks import ReduceLROnPlateau, Callback, ModelCheckpoint
from keras.layers import Embedding
from keras.layers.core import Dense, Activation
from keras.layers.recurrent import LSTM
from keras.models import Sequential
from keras.optimizers import RMSprop

from storyswapping.data import WordEmbeddingIterator, Vocabulary
from keras.layers.wrappers import TimeDistributed

class Generate(Callback):
    def __init__(self, vocabulary, seqlen, genlen=100, initial_text=None, period=10):
        self.vocabulary = vocabulary
        self.seqlen = seqlen
        self.genlen = genlen
        self.initial_text = ["\x03"] if initial_text is None else initial_text
        self.period = period
        self.epoch_count = 0
        
    def on_epoch_end(self, epoch, logs=None):
        if self.epoch_count % self.period == 0:
            sequence = [w for w in self.initial_text]
            generated = [w for w in self.initial_text]
            
            for _ in xrange(self.genlen):
                x = np.zeros((1, self.seqlen), dtype="int32")
                
                for j, w in enumerate(sequence):
                    x[0, j] = self.vocabulary.index_of(w)
                    
                preds = self.model.predict(x, verbose=0)
                next_index = ss.data.sample(preds[0, -1, ], 1.0)
                next_word = self.vocabulary.word_at(next_index)
                
                generated += [next_word]
                
                if len(sequence) < self.seqlen:
                    sequence += [next_word]
                else:
                    sequence = sequence[1:self.seqlen] + [next_word]
            
            print ""
            logging.info("%s", " ".join(generated))
        
        self.epoch_count += 1


def build_model(args, vocab_size):
    model = Sequential()

    # Embedding layer    
    model.add(
        Embedding(
            vocab_size, 
            args.embedding_dim,
            input_length=args.sequence_length, 
            mask_zero=True
        )
    )
    
    # LSTM layers
    for _ in xrange(args.num_layers):
        model.add(LSTM(args.hidden_units, return_sequences=True))
        
    model.add(TimeDistributed(Dense(vocab_size)))
    model.add(Activation('softmax'))
    
    optimizer = RMSprop(lr=0.01)
    
    model.compile(loss='categorical_crossentropy', optimizer=optimizer)
    
    return model

def datagen(itr):
    while True:
        for x, y in itr:
            yield x, y
        
        logging.debug("Resetting iterator")
        itr.reset()
        
def train(args):
    # Vocabulary
    vocab = Vocabulary(args.vocab_file)
    
    # Data Iterators
    trainItr = WordEmbeddingIterator(args.training_file, vocab.size(), args.batch_size, args.sequence_length, limit=args.limit)
    valItr = None if args.validation_file is None else WordEmbeddingIterator(args.validation_file, vocab.size(), args.batch_size, args.sequence_length)
    
    # The model
    model = build_model(args, vocab.size())
    
    # Callbacks
    lrcb = ReduceLROnPlateau(factor=0.5, patience=args.patience, verbose=1, min_lr=1e-7)
    gencb = Generate(vocab, args.sequence_length, period=args.period)
    chkcb = ModelCheckpoint(args.model_file, save_best_only=True, period=args.period)
    callbacks = [lrcb, gencb, chkcb]
    
    # Other parameters
    steps_per_epoch = args.samples_per_epoch // args.batch_size
    max_epochs = int((vocab.total_words / args.samples_per_epoch) * args.max_epochs)
    
    model.fit_generator(datagen(trainItr), steps_per_epoch, max_epochs, validation_data=datagen(valItr), validation_steps=100, callbacks=callbacks)
    
def benchmark(args):
    # Vocabulary
    vocab = Vocabulary(args.vocab_file)
    
    # The model
    model = build_model(args, vocab.size())
    
    for bsz in xrange(1, 400):
        trainItr = WordEmbeddingIterator(args.training_file, vocab.size(), bsz, args.sequence_length, limit=args.limit)

        i = 0
        for x, y in trainItr:
            model.train_on_batch(x, y)
            if i >= 20: break
            i += 1
        
        i = 0    
        start = time.time()
        for x, y in trainItr:
            model.train_on_batch(x, y)
            
            if i >= 50: break
            i += 1
        stop = time.time()
        secs = stop - start
        
        sys.stdout.write("keras/theano {} {} {} {} {} {}\n".format(args.num_layers, args.hidden_units, bsz, (bsz * 50), secs, (bsz * 50) / secs))
        sys.stdout.flush()
        
        trainItr.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    
    parser.add_argument("--training-file", required=True, help="The input training data file")
    parser.add_argument("--validation-file", required=False, help="The input validation data file")
    parser.add_argument("--model-file", required=False, help="The output model file")
    parser.add_argument("--vocab-file", required=True, help="The vocabulary file")
    parser.add_argument("--batch-size", type=int, default=16, help="The standard batch size")
    parser.add_argument("--max-epochs", type=int, default=10, help="The maximum number of epochs")
    parser.add_argument("--sequence-length", type=int, default=10, help="The sequence length")
    parser.add_argument("--num-layers", type=int, default=2, help="The number of hidden LSTM layers")
    parser.add_argument("--embedding-dim", type=int, default=500, help="The embedding dimension")
    parser.add_argument("--hidden-units", type=int, default=800, help="The number of hidden units in an LSTM layer")
    parser.add_argument("--samples-per-epoch", type=int, default=10000, help="The number of hidden units in an LSTM layer")
    parser.add_argument("--limit", type=int, default=sys.maxint, help="Limit the number of documents to process from the data file")
    parser.add_argument("--patience", type=int, default=5, help="The learning rate decay patience")
    parser.add_argument("--period", type=int, default=5, help="The default period to generate sentences and checkpoint the model")
    parser.add_argument("--benchmark", action="store_true", help="Limit the number of documents to process from the data file")
    
    args = parser.parse_args()
    
    logging.getLogger().setLevel(logging.DEBUG)
    
    if args.benchmark:
        benchmark(args)
    else:
        train(args)
    