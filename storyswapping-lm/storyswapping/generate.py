#!/usr/bin/env python
'''
Created on Apr 11, 2017

@author: reid
'''

import argparse
import numpy as np
import sys

import data
import storyswapping as ss
import train
import logging

from depq import DEPQ

class State:
    START = ("\x03", 0)
    EOS = [".", "?", "!"]
    
    def __init__(self, template, template_idx, sequence=None, text=None, heuristic=-8.0):
        self.template = template
        self.template_idx = template_idx
        self.sequence = [State.START] if sequence is None else sequence
        self.text = [State.START] if text is None else text
        self.heuristic = heuristic
        self.score = self.f() + self.g()
        
    def is_goal(self):
        return (
            self.template_idx == len(self.template)
            and self.text[-1][0] in State.EOS
        )
        
    def current_word(self):
        """
        Return the current word under consideration in the template
        
        Returns:
            string: The word in the template at the current template_idx
        """
        return self.template[self.template_idx]
    
    def matches(self, candidate):
        return candidate == self.current_word()
    
    def f(self):
        return sum(logp for _, logp in self.text)
    
    def g(self):
        return self.heuristic * (len(self.template) - self.template_idx)
    
    def __lt__(self, other):
        return self.score < other.score 
    
    def __repr__(self):
        return "({:7.3f}: {})".format(self.score, " ".join(p[0] for p in self.text[1:]))
        
def generate(args, model, vocab, branching=500, max_size=10000, nbest=50, heuristic=-7.0):
    """Generate a document from a template
    
    Args:
        args (Namespace): The basic arguments (from the command line)
        used for generation.
        model (Model): The Keras neural network (LSTM) model used for 
        prediction.
        vocab (data.Vocabulary): The vocabulary used to map indexes
        to words.
        branching (int): The branching factor of the search.
        max_size (int): The maximum number of states to keep in the
        priority queue after an iteration of the main loop.
        heuristic (float): A heuristic value used to estimate the 
        remaining cost to complete the template. The total heuristic
        value for the A* search will be this value times the remaining
        number of words in the template. No matter the value it is
        unlikely to be provably admissible, but I think it is unlikely
        to overestimate the total probability of any completed template
        since it ignores all the intermediate words that will be necessary.
        
    Returns:
        State: The state containing the most likely generated text that
        completes the template
    """
    # Read the template into a string
    template = args.template.read().lower()
    
    logging.info("Generating text from template: {}".format(template))
    
    template = [token.split("_") for token in template.split()]
    template = [token[0] if token[0] in vocab else "<" + token[1] + ">" for token in template]
    seed_word = template[0] 
    sequence = [State.START, (seed_word, 0)]
    text = [State.START, (seed_word, 0)]
    
    logging.info("Generating text from template: {}".format(template))
    
    # Add the initial state to the priority queue
    initial_state = State(template, 1, sequence, text, heuristic)
    
    q = DEPQ(maxlen=max_size)
    q.addfirst(initial_state, initial_state.score)
    
    # The main loop of the search, which is terminated when we've
    # completed the template. The efficiency could potentially be
    # improved with a stateful RNN, but would be much more complex
    results = []
    maxlen = 0
    while len(q) > 0:
        top, _ = q.popfirst()
        
        if top.is_goal():
            results.append(top)
            
            if len(results) == nbest:
                return results
            else:
                continue
        
        # Initialize the input vector based on the current history
        # we've seen so far
        x = np.zeros((1, args.sequence_length), dtype="int32")
        for j, w in enumerate(top.sequence):
            idx = vocab.index_of(w[0])
            
            x[0, j] = vocab.index_of(w[0])
        
        # Make our prediction
        preds = model.predict(x, verbose=0)
        
        # Get the n-best word indexes and their log probabilities
        next_indexes = ss.data.nbest(preds[0, -1, ], branching)
        
        # For each of the n-best words create a new State with the
        # updated sequence and augmented text
        for idx, logp in next_indexes:
        #for _ in xrange(n):
            #idx = ss.data.sample(preds[0, -1, ], 0.1)
            #logp = preds[0, -1, idx]
            next_word = vocab.word_at(idx)
            token = (next_word, logp)
            
            # The new text is always the old text plus the new token
            text = top.text + [token]
            
            # The new sequence may have to be truncated if it's longer
            # than the model's sequence length
            if len(top.sequence) < args.sequence_length:
                sequence = top.sequence + [token]
            else:
                sequence = top.sequence[1:args.sequence_length] + [token]
            
            # If the generated word matches current template word then
            # increment the template index
            template_idx = top.template_idx + 1 if top.matches(next_word) else top.template_idx
            state = State(template, template_idx, sequence, text, heuristic)
            
            q.insert(state, state.score)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    
    parser.add_argument('template', nargs='?', type=argparse.FileType('r'), default=sys.stdin, help="An ordered list of tokens (pos tagged words separated by an '_') that must be included in the final generated story")
    parser.add_argument("--model-file", required=False, help="The trained model")
    parser.add_argument("--vocab-file", required=True, help="The vocabulary file")
    parser.add_argument("--sequence-length", type=int, default=10, help="The sequence length used to train the model")
    parser.add_argument("--num-layers", type=int, default=2, help="The number of hidden LSTM layers")
    parser.add_argument("--embedding-dim", type=int, default=500, help="The embedding dimension")
    parser.add_argument("--hidden-units", type=int, default=800, help="The number of hidden units in an LSTM layer")
    parser.add_argument("--heuristic", type=float, default=-5, help="The weight used in the A* heuristic. Smaller (more negative) values will speed up search but result in less optimal solutions.")
    parser.add_argument("--n-best", type=int, default=50, help="The number of solutions to return (each on a separate line).")
    parser.add_argument("--branching", type=int, default=500, help="At each expansion only consider this many words to add to the search queue.")
    parser.add_argument("--beam-size", type=int, default=10000, help="The maximum size for the search queue.")
    
    args = parser.parse_args()
    
    vocab = data.Vocabulary(args.vocab_file)
    model = train.build_model(args, vocab.size())
    model.load_weights(args.model_file)
    
    results = generate(args, model, vocab, branching=args.branching, max_size=args.beam_size, nbest=args.n_best, heuristic=args.heuristic)
    
    for result in results:
        print "{:7.3f}: {}".format(result.score, " ".join(p[0] for p in result.text[1:]))