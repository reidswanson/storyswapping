'''
Created on Apr 5, 2017

@author: reid
'''

import gzip
import numpy as np
import struct
import sys

from bidict import bidict
from keras.utils.np_utils import to_categorical

# From https://github.com/fchollet/keras/blob/master/examples/lstm_text_generation.py
def sample(preds, temperature=1.0):
    # helper function to sample an index from a probability array
    preds = np.asarray(preds).astype('float64')
    preds = np.log(preds) / temperature
    exp_preds = np.exp(preds)
    preds = exp_preds / np.sum(exp_preds)
    probas = np.random.multinomial(1, preds, 1)
    
    return np.argmax(probas)

def nbest(preds, n=10):
    """ Get the *n* most probable words from the prediction
    
    Args:
        preds (ndarray): The one-hot predicted distribution of the 
        model.
        n (int): The number of elements to return.
        
    Returns:
        array: Each element in the array is a tuple whose first 
        element is the index of the *n* best word and the second
        element is the log probability of that word.
    """
    subset = preds[47:]
    indexes = np.argpartition(subset, -n)[-n:]
    values = subset[indexes]
    
    return zip(np.add(indexes, 47), np.log(values))
    
def read_int(fh):
    return struct.unpack(">i", fh.read(4))[0]

class Vocabulary(object):
    def __init__(self, filename):
        self.vocab = bidict()
        self.counts = dict()
                
        with gzip.open(filename, 'rb') as f:
            self.total_words = int(f.readline())
            for line in f:
                label, idx, count = line.strip().split()
                self.vocab[label] = int(idx)
                self.counts[idx] = int(count)
                
    def __str__(self):
        return "\n".join(["{0} {1}".format(x, y) for x, y in self.vocab.iteritems()])
    
    def index_of(self, word):
        return self.vocab.get(word, -1)
    
    def word_at(self, idx):
        return self.vocab.inv.get(idx, "<unk>")
    
    def size(self):
        return len(self.vocab)
    
    def __contains__(self, word):
        return self.index_of(word) >= 0

class WordEmbeddingIterator:
    def __init__(self, filename, vocab_size, batch_size=16, sequence_length=10, limit=sys.maxint):
        self.filename = filename
        self.vocab_size = vocab_size
        self.batch_size = batch_size
        self.sequence_length = sequence_length
        self.limit = limit
        self.xdata = np.empty((0, self.sequence_length), dtype='int32')
        self.ydata = np.empty((0, self.sequence_length), dtype='int32')
        self.file = None
        
        self.reset()
        
    def reset(self):
        self.close()
        self.finished = False
        
        self.batch_index = 0
        self.file = gzip.open(self.filename, 'rb')
        
        self.xdata = np.empty((0, self.sequence_length), dtype='int32')
        self.ydata = np.empty((0, self.sequence_length), dtype='int32')
        
        read_int(self.file)
        
    def __iter__(self):
        while True:
            while self.xdata.shape[0] < self.sequence_length * self.batch_size and self.batch_index < self.limit and not self.finished:
                self._read_document()
            
            rows = min(self.xdata.shape[0], self.batch_size)
            
            if rows == 0: break
            
            xbatch = self.xdata[:rows, ]
            ybatch = self.ydata[:rows, ]
            
            self.xdata = self.xdata[rows:, ].copy()
            self.ydata = self.ydata[rows:, ].copy()
            
            yout = np.zeros((ybatch.shape[0], ybatch.shape[1], self.vocab_size))
            for i in xrange(ybatch.shape[0]):
                yout[i] = to_categorical(ybatch[i], self.vocab_size)
            
            yield xbatch, yout
            
    def _read_document(self):
        size = read_int(self.file)
        
        if size == 0 or self.batch_index >= self.limit:
            self.finished = True
            return
            
        self.batch_index += 1
        
        padlen = self.sequence_length - (size % self.sequence_length) + 1
        tokens = [read_int(self.file) for _ in xrange(size)]
        xtokens = np.asarray(tokens[:-1] + [0 for _ in xrange(padlen)], dtype='int32')
        ytokens = np.asarray(tokens[1:] + [0 for _ in xrange(padlen)], dtype='int32')

        xtokens = xtokens.reshape((-1, self.sequence_length))
        ytokens = ytokens.reshape((-1, self.sequence_length))
        
        self.xdata = np.append(self.xdata, xtokens, axis=0)
        self.ydata = np.append(self.ydata, ytokens, axis=0)
    
    def close(self):
        if self.file is not None:
            self.file.close()
    
    def next(self):
        pass