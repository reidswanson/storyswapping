import xml.etree.ElementTree

with open("present.verbs.txt") as f:
    present_verbs = f.readlines()
# you may also want to remove whitespace characters like `\n` at the end of each line
present_verbs = [x.strip() for x in present_verbs]

with open("simple.past.verbs.txt") as f:
    past_verbs = f.readlines()
# you may also want to remove whitespace characters like `\n` at the end of each line
past_verbs = [x.strip() for x in past_verbs]
# print(past_verbs)

file_path = "/home/igna/Documents/story-s/storyswapping/storyswapping-core/target/classes/edu/usc/ict/nld/storyswap/dela/dela-en-public-u8.dic1.xml"
# file_path = "test.xml"

tree = xml.etree.ElementTree.parse(file_path)
e = tree.getroot()

for entry in e.findall('entry'):
    # print(atype.findall('inflected'))
    for x in entry.findall('inflected'):
        form = x.find('form').text
        # print(form)
        for y in x.findall('feat'):
            if y.get('name') == "tense":
                if y.get('value') == 'ind':
                    if form in present_verbs:
                        # y.value.text = "present"
                        y.set('value', 'present')
                    if form in past_verbs:
                        y.set('value', 'spast')
tree.write("/home/igna/Documents/story-s/storyswapping/storyswapping-core/target/classes/edu/usc/ict/nld/storyswap/dela/dela-en-public-u8.dic.xml")