/**
 * 
 */
package edu.usc.ict.nld.storyswapping.cli;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * @author reid
 *
 */
public class SplitDataset {
    /**
     * @param args
     * @throws IOException 
     * @throws FileNotFoundException 
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        String inputFilename = args[0];
        String aname = inputFilename.replaceAll("(\\S+?)\\.gz", "$1.a.gz");
        String bname = inputFilename.replaceAll("(\\S+?)\\.gz", "$1.b.gz");
        double percent = Double.parseDouble(args[1]);
        
        DataInputStream dis = null;
        DataOutputStream dos1 = null;
        DataOutputStream dos2 = null;
        
        Random rng = new Random();
        
        try {
            dis = new DataInputStream(new GZIPInputStream(new FileInputStream(inputFilename), 1 << 18));
            dos1 = new DataOutputStream(new GZIPOutputStream(new FileOutputStream(aname), 1 << 18));
            dos2 = new DataOutputStream(new GZIPOutputStream(new FileOutputStream(bname), 1 << 18));
            
            int vocabSize = dis.readInt();
            dos1.writeInt(vocabSize);
            dos2.writeInt(vocabSize);
            
            int size = dis.readInt();
            
            while (size > 0) {
                DataOutputStream dos = rng.nextDouble() < percent ? dos1 : dos2;
                dos.writeInt(size);
                for (int i = 0; i < size; ++i) { dos.writeInt(dis.readInt()); }

                size = dis.readInt();
            }
            
            dos1.writeInt(0);
            dos2.writeInt(0);
        } finally {
            if (dis != null) { dis.close(); }
            if (dos1 != null) { dos1.close(); }
            if (dos2 != null) { dos2.close(); }
        }
    }
}
