/**
 * 
 */
package edu.usc.ict.nld.storyswapping.cli;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.converters.FileConverter;

import edu.stanford.nlp.coref.CorefCoreAnnotations.CorefChainAnnotation;
import edu.stanford.nlp.coref.data.CorefChain;
import edu.stanford.nlp.coref.data.CorefChain.CorefMention;
import edu.stanford.nlp.coref.data.Dictionaries.MentionType;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.ProtobufAnnotationSerializer;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.simple.Sentence;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.Pair;
import edu.stanford.nlp.util.TypesafeMap.Key;
import edu.usc.ict.nld.storyswap.PersonalPronouns;
import edu.usc.ict.nld.storyswap.PointOfView;
import edu.usc.ict.nld.storyswap.PovTransformer;
import edu.usc.ict.nld.storyswap.io.IOUtils;

/**
 * @author rswanson
 *
 */
public class TransformStory {
    //-----------------------------------------------------------------------//
    //-- Nested Classes -----------------------------------------------------//
    //-----------------------------------------------------------------------//
    private static Class<? extends Key<List<CoreMap>>> SENTENCES = SentencesAnnotation.class;
    private static Class<? extends Key<Map<Integer, CorefChain>>> COREF = CorefChainAnnotation.class;
    public static final class Options {
        @Parameter(
            names={"-h", "--help"},
            help=true,
            description="Print these help options"
        )
        boolean help;
        
        @Parameter(
            names={"-f", "--input-text"},
            required=false,
            converter=FileConverter.class,
            description="A file containing a list of stories you would like transformed (separate each story by a blank line). "
        )
        File inputTextFile;
        
        @Parameter(
            names={"-o", "--output-annotations"},
            required=false,
            converter=FileConverter.class,
            description="TODO"
        )
        File outputAnnotationsFile;
        
        @Parameter(
            names={"-a", "--input-annotations"},
            required=false,
            converter=FileConverter.class,
            description="A file containing a list of pre-annotated stories (by the Stanford Core NLP toolkit and serialized using the ProtobufAnnotationSerializer). Note: these must be gzipped."
        )
        File inputAnnotationsFile;
        
        @Parameter(
            names={"-g", "--target-gender"},
            required=false,
            description="The target gender for the output story. (Note: Not all combinations of person, number and gender are possible. E.g., the target gender for first and second person should always be DEITIC for both SINGULAR and PLURAL. PLURAL should also always be DEICTIC.). "
        )
        PersonalPronouns.Gender targetGender;
        
        @Parameter(
            names={"-n", "--target-number"},
            required=false,
            description="The target number for the output story."
        )
        PersonalPronouns.Number targetNumber;
        
        @Parameter(
            names={"-p", "--target-person"},
            required=false,
            description="The target person for the output story."
        )
        PersonalPronouns.Person targetPerson;
        
        @Parameter(
                names={"-c", "--characters"},
                required=false,
                description="List of characters of the transformed story separated by commas. The protagonist of the story is in the first position."
            )
        File charactersFile;
        
        @Parameter(
                names={"-s", "--storytype"},
                required=false,
                description="This option contains the type of the story. Initially the default type is set to A (story with only one protagonist)."
            )
        String storyType;
        
        @Parameter(
                names={"-i", "--introduction"},
                required=false,
                description="This is the file that contains the opening lines, and the possible type of stories."
            )
        File openingLines;
    }

    //-----------------------------------------------------------------------//
    //-- Static Fields ------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Managed Properties -------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Properties ---------------------------------------------------------//
    //-----------------------------------------------------------------------//
    //-- Stative ------------------------------------------------------------//

    //-- Functional ---------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Initialization -----------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Getters & Setters --------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Methods ------------------------------------------------------------//
    //-----------------------------------------------------------------------//
    /**
     * @param args
     * @throws IOException 
     * @throws FileNotFoundException 
     * @throws ClassCastException 
     * @throws ClassNotFoundException 
     */
    public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException, ClassCastException {
        Options options = new Options();
        JCommander jc = new JCommander(options);
        StringBuilder usage = new StringBuilder();
        
        jc.setProgramName(TransformStory.class.getSimpleName());
        jc.usage(usage);
        
        try {
            jc.parse(args);
        } catch (ParameterException e) {
            System.err.println(e.getMessage());
            System.err.println(usage);
            System.exit(-1);
        }
        
        if (options.help) {
            System.err.println(usage.toString());
            return;
        }
        
        if (options.inputTextFile != null && options.inputAnnotationsFile != null) {
            System.err.println("You can only specify input from a text file or an annotation file, but not both");
            return;
        }
        
        if (options.inputTextFile != null && options.outputAnnotationsFile != null) {
            parse(options.inputTextFile, options.outputAnnotationsFile);
            return;
        } else if (options.targetGender == null || options.targetNumber == null || options.targetPerson == null) {
            System.err.println("You must specify the target number, gender and persion");
            System.err.println(usage.toString());
        }
        
        if (options.storyType == null) {
        	options.storyType = "A";
        }
    	String[] newCharacters = loadNewCharacters(options.charactersFile);
        if (options.targetPerson == PersonalPronouns.Person.THIRD) {
            String opener = loadOpener(options.storyType, newCharacters);
            System.out.println(opener);
        }
        
        PovTransformer transformer = new PovTransformer();
        PointOfView targetPov = new PointOfView(options.targetNumber, options.targetPerson, options.targetGender);
        
        List<Annotation> storyAnnotations = getAnnotations(options);
        //List<String> POVtransformedStory = new ArrayList<String>();
        String POVtransformedStory = "";
        
        for (Annotation story : storyAnnotations) {
            String transformed = transformer.transform(story, targetPov);
            POVtransformedStory = POVtransformedStory + transformed;
        }
        //System.out.println(POVtransformedStory);
        String ans = replaceCharacters(newCharacters, POVtransformedStory);
        System.out.println(ans);
    }

    //-----------------------------------------------------------------------//
    //-- Actions ------------------------------------------------------------//
    //-----------------------------------------------------------------------//

    private static String replaceCharacters(String[] newCharacters, String story) {
    	String finalStory = "";

        Annotation document = new Annotation(story);
        Properties props = new Properties();
        props.setProperty("annotators", "tokenize,ssplit,pos,lemma,ner,parse,mention,coref");
        StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
        pipeline.annotate(document);
        List <CorefChain> chains = findChains(document);
        
        Set<String> initialCharacters = new HashSet<String>();
        
        // Check if there is more than 1 corefchain in the story
        if (chains.size() == 1) {
        	Sentence sent = new Sentence(story);
        	List<String> nerTags = sent.nerTags();
        	for (int i = 0; i < nerTags.size(); i++) {
        		String tag = nerTags.get(i).toString();
    			if (tag.equals("PERSON")) {
    				initialCharacters.add(sent.word(i).toString());
    			}
    		}
        	List<String> charactersList = new ArrayList<String>(initialCharacters);
        	for (int i = 1; i < newCharacters.length ; i++) {
        		story = story.replace(charactersList.get(i - 1), newCharacters[i]);
        	}
        	finalStory = story;
        } else {
        	// Go through the sentences and replace the secondary characters.
        }

        return finalStory;
    }


    /**
     * Finds the corefchains of the story
     */
    public static List<CorefChain> findChains(Annotation annotation) {
        Map<Integer, CorefChain> corefMap = annotation.get(COREF);

        // TODO enhance this heuristic
        List<CorefChain> chains = new ArrayList<>(corefMap.values());
        Collections.sort(chains, (cc1, cc2) -> cc2.getMentionsInTextualOrder().size() - cc1.getMentionsInTextualOrder().size());
        
        return chains;
    }
    
    //-----------------------------------------------------------------------//
    //-- Utility Methods ----------------------------------------------------//
    //-----------------------------------------------------------------------//
    
    private static String loadOpener(String type, String[] characters) throws FileNotFoundException, IOException {
    	String opener = "";
    	File openers = new File("../storyswapping-lm/data/opening.lines.txt");
    	String text = IOUtils.toString(openers);
    	
    	Map<String, List<String>> typeOpening = new HashMap<>();
    	
        for (String line : text.split("\\r?\\n")) {
        	//System.out.println(line);
        	String key = line.split("---")[0];
        	String value = line.split("---")[1];
        	if (typeOpening.get(key) != null) {
        		List<String> listOfOpeners = typeOpening.get(key);
        		listOfOpeners.add(value);
        		typeOpening.put(key, listOfOpeners);
        		
        	} else {
        		List<String> listOfOpeners = new ArrayList<String>();; 
        		listOfOpeners.add(value);
        		typeOpening.put(key, listOfOpeners);
        	}
        }
        //TODO: Get different introductions.
        opener = typeOpening.get(type).get(0);
        opener = opener.replaceAll("<P>", characters[0]);
        return opener;
    }
    
    private static String[] loadNewCharacters(File file) throws FileNotFoundException, IOException {
    	String[] newCharacters = null;
    	String text = IOUtils.toString(file);
        newCharacters = text.split(",");
    	
        return newCharacters;
    }
    
    private static StanfordCoreNLP loadPipeline() {
        Properties props = new Properties();
        props.setProperty("annotators", "tokenize, ssplit, pos, lemma, ner, parse, depparse, dcoref");
        props.setProperty("coref.algorithm", "neural");
        
        StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
        
        return pipeline;
    }
    
    private static List<String> loadStories(File file) throws FileNotFoundException, IOException {
        List<String> stories = new ArrayList<>();
        String text = IOUtils.toString(file);
        
        StringBuilder sb = new StringBuilder();
        for (String line : text.split("\\r?\\n")) {
            if (line.matches("^\\s*$") && sb.length() > 0) {
                stories.add(sb.toString());
                sb = new StringBuilder();
            } else {
                sb.append(new String(line)).append('\n');
            }
        }
        
        if (sb.length() > 0) {
            stories.add(sb.toString());
        }
        
        return stories;
    }
    
    private static List<Annotation> getAnnotations(Options options) throws FileNotFoundException, IOException, ClassNotFoundException, ClassCastException {
        if (options.inputAnnotationsFile == null) {
            List<String> stories = loadStories(options.inputTextFile);
            List<Annotation> annotations = stories.stream().map(s -> new Annotation(s)).collect(Collectors.toList());
            
            StanfordCoreNLP pipeline = loadPipeline();
            pipeline.annotate(annotations);
            
            return annotations;
        }
        
        ProtobufAnnotationSerializer serializer = new ProtobufAnnotationSerializer();
        List<Annotation> annotations = new ArrayList<>();
        try (
            InputStream is = new FileInputStream(options.inputAnnotationsFile);
            GZIPInputStream cis = new GZIPInputStream(is);
            
        ) {
            Pair<Annotation, InputStream> pair = null;
            
            try {
                pair = serializer.read(cis);
                annotations.add(pair.first);
                
                while (true) {
                    pair = serializer.read(pair.second);
                    annotations.add(pair.first);
                }
            } catch (Throwable t) {
                // No annotations to read?
                return annotations;
            }
        }
    }
    
    private static void parse(File inputFile, File outputFile) throws IOException {
        List<String> textStories = loadStories(inputFile);
        List<Annotation> annotations = textStories.stream().map(s -> new Annotation(s)).collect(Collectors.toList());
        
        StanfordCoreNLP pipeline = loadPipeline();
        
        pipeline.annotate(annotations);
        
        OutputStream os = null;
        ProtobufAnnotationSerializer serializer = new ProtobufAnnotationSerializer();
        try (
            FileOutputStream fos = new FileOutputStream(outputFile);
            GZIPOutputStream gos = new GZIPOutputStream(fos, 1 << 16);
        ) {
            os = gos;
            
            for (Annotation a : annotations) {
                os = serializer.write(a, os);
            }
        }
    }
}
