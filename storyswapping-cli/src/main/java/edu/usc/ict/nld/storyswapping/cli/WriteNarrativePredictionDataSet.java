/**
 * 
 */
package edu.usc.ict.nld.storyswapping.cli;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.GZIPOutputStream;

import org.deeplearning4j.models.word2vec.VocabWord;
import org.deeplearning4j.models.word2vec.wordstore.VocabCache;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.converters.FileConverter;
import com.reidswanson.dl4j.nlp.SimpleWordVectors;
import com.reidswanson.dl4j.nlp.VocabSerializer;

import edu.usc.ict.nld.storyswap.io.DatabaseConnectionProperties;
import edu.usc.ict.nld.storyswap.nn.DocumentIterator;
import edu.usc.ict.nld.storyswap.nn.StandardTokenPreprocess;
import edu.usc.ict.nld.storyswap.nn.Tag2WordTokenizerFactory;
import edu.usc.ict.nld.storyswap.nn.TaggedDocumentIterator;

/**
 * @author reid
 *
 */
public class WriteNarrativePredictionDataSet {
    static final Logger LOG = LoggerFactory.getLogger(WriteNarrativePredictionDataSet.class);

    //-----------------------------------------------------------------------//
    //-- Nested Classes -----------------------------------------------------//
    //-----------------------------------------------------------------------//
    public static class Options {
        @Parameter(
            names={"-h", "--help"},
            help=true,
            required=false,
            description="Show these help options"
        )
        public boolean help;
        
        @Parameter(
            names={"-u", "--unk"},
            required=false,
            description="The token to use for unknown words."
        )
        public String unk = "<unk>";
        
        @Parameter(
            names={"-c", "--connection-properties"},
            required=true,
            converter=FileConverter.class,
            description="The properties file containing the database connection parameters."
        )
        public File connectionPropertiesFile = null;
        
        @Parameter(
            names={"-1", "--one-hot"},
            required=false,
            description="Use a one-hot encoding instead of the word embedding vector."
        )
        public boolean useOneHot = false;
        
        @Parameter(
            names={"-S", "--max-length"},
            required=false,
            description="Filter stories that have more than this many words."
        )
        public int maxStoryLength = 1000;
        
        @Parameter(
            names={"-W", "--min-length"},
            required=false,
            description="Filter stories that have fewer than this many words."
        )
        public int minStoryLength = 1000;
        
        @Parameter(
            names={"-Q", "--filter-quality"},
            required=false,
            description="Filter stories using some simple quality heuristics."
        )
        public boolean useQualityHeuristics = true;
        
        
        @Parameter(
            names={"-V", "--vocab-file"},
            required=true,
            converter=FileConverter.class,
            description="The vocabulary file to use for 1-hot encodings."
        )
        public File vocabFile;
        
        @Parameter(
            names={"-w", "--w2v-file"},
            required=false,
            converter=FileConverter.class,
            description="The word vectors file, assumed to be in binary format (gzipped or uncompressed)."
        )
        public File wordVectorsFile = null;
        
        @Parameter(
            names={"-o", "--output-file"},
            required=true,
            converter=FileConverter.class,
            description="The file where the data set will be written to."
        )
        public File outputFile = null;
    }

    //-----------------------------------------------------------------------//
    //-- Static Fields ------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Managed Properties -------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Properties ---------------------------------------------------------//
    //-----------------------------------------------------------------------//
    //-- Stative ------------------------------------------------------------//
    private Options options;
    private DatabaseConnectionProperties connectionProperties;
    private SimpleWordVectors w2v;
    private VocabCache<VocabWord> vocabCache;
    private TaggedDocumentIterator iterator;
    private TokenizerFactory tokenizerFactory;
    //-- Functional ---------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Initialization -----------------------------------------------------//
    //-----------------------------------------------------------------------//
    /**
     * 
     * @param options
     */
    public WriteNarrativePredictionDataSet(Options options) {
        this.options = options;
        this.connectionProperties = DatabaseConnectionProperties.load(options.connectionPropertiesFile);
        
        this.vocabCache = VocabSerializer.loadVocab(options.vocabFile);
        if (!options.useOneHot) {
            this.w2v = SimpleWordVectors.load(options.wordVectorsFile);
        }
        
        this.iterator = new TaggedDocumentIterator(connectionProperties, options.minStoryLength, options.maxStoryLength, options.useQualityHeuristics);
        this.tokenizerFactory = new Tag2WordTokenizerFactory(new StandardTokenPreprocess(), this.vocabCache, options.unk);
    }

    //-----------------------------------------------------------------------//
    //-- Getters & Setters --------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Methods ------------------------------------------------------------//
    //-----------------------------------------------------------------------//
    /**
     * @param args
     */
    public static void main(String[] args) {
        Options options = new Options();
        JCommander jc = new JCommander(options);
        StringBuilder usage = new StringBuilder();
        
        jc.setProgramName(WriteNarrativePredictionDataSet.class.getSimpleName());
        jc.usage(usage);
        
        try {
            jc.parse(args);
        } catch (ParameterException e) {
            System.err.println(e.getMessage());
            System.err.println(usage);
            System.exit(-1);
        }
        
        if (options.help) {
            System.err.println(usage.toString());
            return;
        }
        
        if (!options.useOneHot && options.wordVectorsFile == null) {
            System.err.println("You must specify a word vectors file when writing a vector space file.");
            System.err.println(usage.toString());
            return;
        }
        
        WriteNarrativePredictionDataSet writer = new WriteNarrativePredictionDataSet(options);
        writer.go();

    }
    
    public void go() {
        try (
            FileOutputStream fos = new FileOutputStream(options.outputFile);
            GZIPOutputStream cos = new GZIPOutputStream(fos, 1 << 18);
            DataOutputStream dos = new DataOutputStream(cos);
        ) {
            if (options.useOneHot) {
                writeOneHot(dos);
            } else {
                writeSimilarity(dos);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    private void writeOneHot(DataOutputStream dos) throws IOException {
        int ndocs = 0;
        dos.writeInt(vocabCache.numWords());
        while (iterator.hasNext()) {
            List<String> document = iterator.next();
            
            List<String> tokens =
                document.stream()
                    .flatMap(s -> tokenizerFactory.create(s).getTokens().stream())
                    .collect(Collectors.toList());
            
            dos.writeInt(tokens.size());
            for (String t : tokens) {
                dos.writeInt(vocabCache.indexOf(t));
            }
            
            ndocs++;
        }
        
        dos.writeInt(0);
        
        LOG.info("Wrote {} documents.", ndocs);
    }
    
    private void writeSimilarity(DataOutputStream dos) throws IOException {
        dos.writeInt(w2v.getLayerSize());
        
        while (iterator.hasNext()) {
            List<String> document = iterator.next();
            
            List<String> tokens =
                document.stream()
                    .flatMap(s -> tokenizerFactory.create(s).getTokens().stream())
                    .collect(Collectors.toList());
            
            dos.writeInt(tokens.size());
            for (String w : tokens) {
                INDArray array = toArray(w, w2v);
                writeArray(array, dos);
            }
        }
        
        dos.writeInt(0);
    }

    //-----------------------------------------------------------------------//
    //-- Actions ------------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Utility Methods ----------------------------------------------------//
    //-----------------------------------------------------------------------//
    private void writeArray(INDArray array, DataOutputStream dos) throws IOException {
        for (int i = 0; i < array.length(); ++i) {
            dos.writeFloat(array.getFloat(i));
        }
    }
    
    private static INDArray toArray(String w, SimpleWordVectors w2v) {
        if (w.equals(DocumentIterator.DOCUMENT_START) || w.equals(DocumentIterator.DOCUMENT_END)) {
            INDArray array = Nd4j.zeros(w2v.getLayerSize());
            array.putScalar((int) w.charAt(0), 1.0);
            
            return array;
        }
        
        if (w2v.hasWord(w)) {
            return w2v.getWordVectorMatrixNormalized(w);
        }
        
        // TODO handle unknown words
        return Nd4j.rand(new int[] { w2v.getLayerSize() });
    }
}
