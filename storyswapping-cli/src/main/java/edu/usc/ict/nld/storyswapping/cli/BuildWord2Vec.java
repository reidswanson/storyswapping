/**
 * 
 */
package edu.usc.ict.nld.storyswapping.cli;

import java.io.File;
import java.io.IOException;

import org.deeplearning4j.models.word2vec.VocabWord;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.models.word2vec.wordstore.VocabCache;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.converters.FileConverter;
import com.reidswanson.dl4j.nlp.VocabSerializer;

import edu.usc.ict.nld.storyswap.io.DatabaseConnectionProperties;
import edu.usc.ict.nld.storyswap.io.SerializationUtils;
import edu.usc.ict.nld.storyswap.nn.StandardTokenPreprocess;
import edu.usc.ict.nld.storyswap.nn.Tag2WordTokenizerFactory;
import edu.usc.ict.nld.storyswap.nn.TaggedDocumentIterator;
import edu.usc.ict.nld.storyswap.nn.TaggedDocumentSentenceIterator;

/**
 * @author rswanson
 *
 */
public class BuildWord2Vec {
    static final Logger LOG = LoggerFactory.getLogger(BuildWord2Vec.class);

    //-----------------------------------------------------------------------//
    //-- Nested Classes -----------------------------------------------------//
    //-----------------------------------------------------------------------//
    public static class Options {
        @Parameter(
            names={"-p", "--connection-properties"},
            required=true,
            converter=FileConverter.class,
            description="The file containing the database connection properties."
        )
        public File connectionProperties;
        
        @Parameter(
            names={"-u", "--unk"},
            required=false,
            description="The name of the unknown token in the vocabulary."
        )
        public String unk = "<unk>";
        
        @Parameter(
            names= {"-d", "--dimension"},
            required=false,
            description="The number of dimensions for the word2vec vectors."
        )
        public int dimension = 100;
        
        @Parameter(
            names={"-c", "--context"},
            required=false,
            description="The amount of context for the skip n-gram model."
        )
        public int context = 5;
        
        @Parameter(
            names={"-b", "--batch-size"},
            required=false,
            description="The mini-batch size."
        )
        public int batchSize = 10000;
        
        @Parameter(
            names={"-i", "--iterations"},
            required=false,
            description="The number of iterations to loop through the data."
        )
        public int iterations = 1;
        
        @Parameter(
            names={"-t", "--frequency-threshold"},
            required=false,
            description="Only include words that appear this many times in the corpus."
        )
        public int threshold = 20;
        
        @Parameter(
            names={"-s", "--negative-sampling"},
            required=false,
            description="The amount of negative sampling."
        )
        public int negativeSampling = 15;
        
        @Parameter(
            names={"-V", "--vocab-file"},
            required=true,
            converter=FileConverter.class,
            description="The location of the serialized VocabCache to use."
        )
        public File vocabFile;
        
        @Parameter(
            names={"-o", "--output-file"},
            required = true,
            converter=FileConverter.class,
            description="The file to write the vectors to."
        )
        public File outputFile;
    }

    //-----------------------------------------------------------------------//
    //-- Static Fields ------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Managed Properties -------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Properties ---------------------------------------------------------//
    //-----------------------------------------------------------------------//
    //-- Stative ------------------------------------------------------------//

    //-- Functional ---------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Initialization -----------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Getters & Setters --------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Methods ------------------------------------------------------------//
    //-----------------------------------------------------------------------//
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Options options = new Options();
        JCommander jc = new JCommander(options);
        StringBuilder usage = new StringBuilder();

        jc.setProgramName(BuildWord2Vec.class.getSimpleName());
        jc.usage(usage);

        try {
            jc.parse(args);
        } catch (ParameterException e) {
            System.err.println(e.getMessage());
            System.err.println(usage);
            System.exit(-1);
        }
        
        DatabaseConnectionProperties connectionProperties = DatabaseConnectionProperties.load(options.connectionProperties);        
        VocabCache<VocabWord> vocab = VocabSerializer.loadVocab(options.vocabFile);
        
        TaggedDocumentIterator docItr = new TaggedDocumentIterator(connectionProperties);
        TaggedDocumentSentenceIterator senItr = new TaggedDocumentSentenceIterator(docItr);
        TokenizerFactory tf = new Tag2WordTokenizerFactory(new StandardTokenPreprocess(), vocab, options.unk);
        
        LOG.debug("Start building word2vec model using batch size: {}", options.batchSize);
        Word2Vec w2v = new Word2Vec.Builder()
             .seed(5)
             .negativeSample(options.negativeSampling)
             .vocabCache(vocab)
             .batchSize(options.batchSize)
             .layerSize(options.dimension)
             .windowSize(options.context)
             .iterate(senItr)
             .tokenizerFactory(tf)
             .unknownElement(vocab.tokenFor(options.unk))
             .useUnknown(true)
             .resetModel(false)
             .build();
        
        for (int i = 0; i < options.iterations; ++i) {
            w2v.fit();
            senItr.reset();
            SerializationUtils.writeCompressedBinary(w2v, options.outputFile);
            LOG.debug("Finished epoch: {}", (i + 1));
        }
        
        senItr.finish();
        docItr.close();
        LOG.debug("Finish building word2vec model");
    }

    //-----------------------------------------------------------------------//
    //-- Actions ------------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Utility Methods ----------------------------------------------------//
    //-----------------------------------------------------------------------//
}
