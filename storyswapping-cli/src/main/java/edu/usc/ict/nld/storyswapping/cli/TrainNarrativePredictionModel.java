/**
 * 
 */
package edu.usc.ict.nld.storyswapping.cli;

import static com.reidswanson.dl4j.nn.conf.NetworkUtils.getLearningRatesAsString;
import static com.reidswanson.dl4j.nn.conf.NetworkUtils.getMomentumAsString;
import static edu.usc.ict.nld.storyswap.nn.DatabaseDataSetIterator.DOCUMENT_BOUNDARY;
import static java.util.Arrays.asList;
import static org.nd4j.linalg.indexing.NDArrayIndex.all;
import static org.nd4j.linalg.indexing.NDArrayIndex.point;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.deeplearning4j.datasets.iterator.AsyncDataSetIterator;
import org.deeplearning4j.models.word2vec.VocabWord;
import org.deeplearning4j.models.word2vec.wordstore.VocabCache;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.INDArrayIndex;
import org.nd4j.linalg.lossfunctions.ILossFunction;
import org.nd4j.linalg.lossfunctions.LossFunctions.LossFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.ParametersDelegate;
import com.beust.jcommander.converters.FileConverter;
import com.reidswanson.dl4j.nlp.SimpleWordVectors;
import com.reidswanson.dl4j.nlp.VocabSerializer;
import com.reidswanson.dl4j.nn.conf.NetworkOptions;
import com.reidswanson.dl4j.nn.conf.RecurrentNetworkBuilder;
import com.reidswanson.dl4j.nn.conf.RecurrentNetworkOptions;
import com.reidswanson.dl4j.optimize.listeners.AverageScoreIterationListener;

import edu.usc.ict.nld.storyswap.nn.NarrativeIterator;
import edu.usc.ict.nld.storyswap.nn.NarrativePredictionSimilarityFileDataSetIterator;
import edu.usc.ict.nld.storyswap.nn.OneHotDocumentFileIterator;

/**
 * @author rswanson
 *
 */
public class TrainNarrativePredictionModel {
    static final Logger LOG = LoggerFactory.getLogger(TrainNarrativePredictionModel.class);

    //-----------------------------------------------------------------------//
    //-- Nested Classes -----------------------------------------------------//
    //-----------------------------------------------------------------------//
    public static class Options {
        @Parameter(
            names={"-h", "--help"},
            help=true,
            description="Print these help options."
        )
        boolean help;
        
        @Parameter(
            names={"-c", "--connection-properties"},
            required=false,
            converter=FileConverter.class,
            description="A file containing the database connection properties."
        )
        File connectionPropertiesFile;
        
        @Parameter(
            names={"-z", "--random-seed"},
            required=false,
            description="The random seed to use."
        )
        Long seed = null;
        
        @Parameter(
            names={"-Z", "--batch-buffer-size"},
            required=false,
            description="How many batches to load asynchronously."
        )
        public int batchBufferSize = 8;
        
        @Parameter(
            names={"-1", "--one-hot"},
            required=false,
            description="Set this parameter if the dataset file is encoded as one-hot vectors instead of similarity vectors."
        )
        public boolean useOneHot = false;
        
        @ParametersDelegate
        public NetworkOptions network = new NetworkOptions();
        
        @ParametersDelegate
        public RecurrentNetworkOptions recurrent = new RecurrentNetworkOptions();
        
        @Parameter(
            names={"-w", "--word-vectors-file"},
            required=false,
            converter=FileConverter.class,
            description="A file containing the simple word vectors."
        )
        public File wordVectorsFile;
        
        @Parameter(
            names={"-C", "--vocabulary-file"},
            required=true,
            converter=FileConverter.class,
            description="A file containing the vocabulary."
        )
        public File vocabularyFile;
        
        @Parameter(
            names={"-F", "--model-file"},
            required=true,
            converter=FileConverter.class,
            description="The file to save the trained model to."
        )
        public File modelFile = null;
        
        @Parameter(
            names={"-f", "--dataset-file"},
            required=true,
            converter=FileConverter.class,
            description="The dataset file."
        )
        public File dataSetFile = null;
    }

    //-----------------------------------------------------------------------//
    //-- Static Fields ------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Managed Properties -------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Properties ---------------------------------------------------------//
    //-----------------------------------------------------------------------//
    //-- Stative ------------------------------------------------------------//
    private Options options;
    private DataSetIterator iterator;
    private NarrativeIterator baseItr;
    private MultiLayerConfiguration networkConfig;
    private MultiLayerNetwork network;
    private SimpleWordVectors w2v;
    private VocabCache<VocabWord> vocab;
    private Random rng;

    //-- Functional ---------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Initialization -----------------------------------------------------//
    //-----------------------------------------------------------------------//
    /**
     * 
     * @param options
     */
    public TrainNarrativePredictionModel(Options options) {
        if (options.useOneHot) {
            //baseItr = new NarrativePredictionOneHotFileDataSetIterator(options.dataSetFile, options.recurrent.sequenceLength, options.network.miniBatchSize);
            baseItr = new OneHotDocumentFileIterator(options.dataSetFile, options.recurrent.sequenceLength, options.network.miniBatchSize);
        } else {
            this.w2v = new SimpleWordVectors(); 
            this.w2v.read(options.wordVectorsFile);
            baseItr = new NarrativePredictionSimilarityFileDataSetIterator(options.dataSetFile, options.recurrent.sequenceLength, options.network.miniBatchSize);
        }
        this.iterator = new AsyncDataSetIterator(baseItr, options.batchBufferSize);
        this.options = options;
        
        this.vocab = VocabSerializer.loadVocab(options.vocabularyFile);
        this.networkConfig = makeNetworkConfig();
        this.network = makeNetwork();
        this.rng = options.seed == null ? new Random() : new Random(options.seed);
    }

    //-----------------------------------------------------------------------//
    //-- Getters & Setters --------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Methods ------------------------------------------------------------//
    //-----------------------------------------------------------------------//
    /**
     * @param args
     * @throws IOException 
     */
    public static void main(String[] args) throws IOException {
        Options options = new Options();
        JCommander jc = new JCommander(options);
        StringBuilder usage = new StringBuilder();
        
        jc.setProgramName(TransformStory.class.getSimpleName());
        jc.usage(usage);
        
        try {
            jc.parse(args);
        } catch (ParameterException e) {
            System.err.println(e.getMessage());
            System.err.println(usage);
            System.exit(-1);
        }
        
        if (options.help) {
            System.err.println(usage.toString());
            return;
        }
        
        if (options.seed != null) {
            Nd4j.getRandom().setSeed(options.seed);
        }
        
        Nd4j.getMemoryManager().setAutoGcWindow(500);

        TrainNarrativePredictionModel trainer = new TrainNarrativePredictionModel(options);
        
        trainer.go();
    }
    
    public void go() throws IOException {
        network.setListeners(new AverageScoreIterationListener(100, 1, 10, 20, 50));
        
        LOG.debug("Start training");
        for (int epoch = 0, miniBatch = 0; epoch < options.network.maxEpochs; ++epoch) {
            for (; iterator.hasNext(); ++miniBatch) {
                DataSet data = iterator.next();
                
                //tokens(data.getFeatures()).forEach(l -> System.err.println(l));
                
                network.fit(data);
                
                if (miniBatch % 50 == 0) {
                    LOG.debug("Current learning rates: {} momentum: {}", getLearningRatesAsString(networkConfig), getMomentumAsString(networkConfig));
                    List<String> sample = sample(network, miniBatch, 50);
                    LOG.info(String.join(" ", sample));
                    
                    ModelSerializer.writeModel(network, options.modelFile, true);
                }
            }
            
            iterator.reset();
            LOG.debug("Finished epoch: {}", epoch);
        }
        
//        uiServer.stop();
    }
    
    private List<String> tokens(INDArray features) {
        List<String> inputs = new ArrayList<>();
        
        for (int i = 0; i < features.size(0); ++i) {
            List<String> sequence = new ArrayList<>(features.size(2));
            for (int k = 0; k < features.size(2); ++k) {
                for (int j = 0; j < features.size(1); ++j) {
                    if (features.getInt(i, j, k) == 1) {
                        sequence.add(vocab.wordAtIndex(j));
                        break;
                    }
                }
            }
            inputs.add(String.join(" ", sequence));
        }
        
        return inputs;
    }

    //-----------------------------------------------------------------------//
    //-- Actions ------------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Utility Methods ----------------------------------------------------//
    //-----------------------------------------------------------------------//    
    private MultiLayerConfiguration makeNetworkConfig() {        
        int nIn = iterator.inputColumns();
        int nOut = iterator.totalOutcomes();
        int nHidden = options.network.nHiddenUnits;
        
        ILossFunction lossFunction = 
            options.useOneHot 
                ? LossFunction.MCXENT.getILossFunction() 
                : LossFunction.MSE.getILossFunction();
                
        Activation outputActivation =
            options.useOneHot
                ? Activation.SOFTMAX
                : Activation.IDENTITY;
        return
            new RecurrentNetworkBuilder(options.network, options.recurrent, lossFunction, nIn, nHidden, nOut)
                .outputActivation(outputActivation)
                .build();
    }
    
    private MultiLayerNetwork makeNetwork() {
        MultiLayerNetwork network = new MultiLayerNetwork(networkConfig);
        network.init();
        LOG.info("Created network with {} parameters", network.numParams());
        
        return network;
    }
    
    private List<String> sample(MultiLayerNetwork network, int miniBatch, int length) {
        if (options.useOneHot) {
            return sampleOneHot(network, miniBatch, length);
        } else {
            return sampleSimilarity(network, miniBatch, length);
        }
    }
    
    public List<String> sampleOneHot(MultiLayerNetwork network, int miniBatch, int length) {
        List<String> generatedWords = new ArrayList<>(asList(Character.toString(DOCUMENT_BOUNDARY), "once", "upon", "a", "time"));
        
        INDArray initialArray = Nd4j.zeros(1, iterator.inputColumns(), generatedWords.size());
        for (int i = 0; i < generatedWords.size(); ++i) {
            String w = generatedWords.get(i);
            int index = vocab.indexOf(w);
            if (index < 0) { index = vocab.indexOf("<unk>"); } // TODO use unk variable
            
            initialArray.putScalar(new int[] { 0,  index, i }, 1.0);
        }
        
        network.rnnClearPreviousState();
        INDArray output = network.rnnTimeStep(initialArray);
        output = output.tensorAlongDimension(output.size(2) - 1,  1, 0); // get last time step
        
        for (int i = 0; i < length; ++i) {
            INDArray nextInput = Nd4j.zeros(1, iterator.inputColumns());
            
            double[] outputProbDistribution = new double[iterator.totalOutcomes()];
            for( int j=0; j<outputProbDistribution.length; j++ ) outputProbDistribution[j] = output.getDouble(j);
            int sampledWordIndex = sampleFromDistribution(outputProbDistribution);
            
            nextInput.putScalar(new int[] { 0 , sampledWordIndex }, 1.0);
            
            generatedWords.add(vocab.wordAtIndex(sampledWordIndex));
            
            /*
            if (sampledWordIndex == w2v.vocabulary().size()) {
                generatedWords.add(DocumentIterator.DOCUMENT_START);
            } else if (sampledWordIndex == w2v.vocabulary().size() + 1) {
                generatedWords.add(DocumentIterator.DOCUMENT_END);
            } else if (sampledWordIndex == w2v.vocabulary().size() + 2) {
                generatedWords.add(w2v.sampleWord());
            } else {
                generatedWords.add(w2v.wordAt(sampledWordIndex));
            }
            */
            
            output = network.rnnTimeStep(nextInput);
        }
        
        return generatedWords;
    }
    
    private int sampleFromDistribution(double[] distribution) {
        double d = 0.0;
        double sum = 0.0;
        for (int t = 0; t < 10; t++) {
            d = rng.nextDouble();
            sum = 0.0;
            for (int i = 0; i < distribution.length; i++) {
                sum += distribution[i];
                if (d <= sum)
                    return i;
            }
            
            // If we haven't found the right index yet, maybe the sum is
            // slightly
            // lower than 1 due to rounding error, so try again.
        }
        
        // Should be extremely unlikely to happen if distribution is a valid
        // probability distribution
        throw new IllegalArgumentException("Distribution is invalid? d=" + d + ", sum=" + sum);
    }
    
    public List<String> sampleSimilarity(MultiLayerNetwork network, int miniBatch, int length) {
        // Sample from the n nearest words.
        int nearest = 1;        
        List<String> generatedWords = new ArrayList<>(asList(Character.toString(DOCUMENT_BOUNDARY), "once", "upon", "a", "time"));
        
        INDArray initialArray = Nd4j.zeros(1, w2v.getDimension(), generatedWords.size());
        for (int i = 0; i < generatedWords.size(); ++i) {
            initialArray.put(new INDArrayIndex[] { point(0), all(), point(i) }, w2v.getWordVectorMatrixNormalized(generatedWords.get(i)));
        }
        
        network.rnnClearPreviousState();
        INDArray output = network.rnnTimeStep(initialArray);
        output = output.tensorAlongDimension(output.size(2) - 1,  1, 0); // get last time step
        
        for (int i = 0; i < length; ++i) {
            List<String> nearestWords = w2v.wordsNearest(output, nearest);
            String sampledWord = sample(nearestWords);
            INDArray nextInput = w2v.getWordVectorMatrixNormalized(sampledWord);
            generatedWords.add(sampledWord);
            
            output = network.rnnTimeStep(nextInput);
        }
        
        return generatedWords;
    }
    
    private <T> T sample(List<T> list) {
        int idx = rng.nextInt(list.size());
        
        return list.get(idx);
    }
}
