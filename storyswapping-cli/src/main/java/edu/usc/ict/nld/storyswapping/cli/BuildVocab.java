/**
 * 
 */
package edu.usc.ict.nld.storyswapping.cli;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.GZIPOutputStream;

import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.converters.FileConverter;

import edu.usc.ict.nld.storyswap.io.DatabaseConnectionProperties;
import edu.usc.ict.nld.storyswap.nn.StandardTokenPreprocess;
import edu.usc.ict.nld.storyswap.nn.Tag2WordTokenizer;
import edu.usc.ict.nld.storyswap.nn.Tag2WordTokenizerFactory;
import edu.usc.ict.nld.storyswap.nn.TaggedDocumentIterator;
import edu.usc.ict.nld.storyswap.nn.TaggedSentenceIterator;
import it.unimi.dsi.fastutil.objects.Object2LongOpenHashMap;

/**
 * @author reid
 *
 */
public class BuildVocab {
    static final Logger LOG = LoggerFactory.getLogger(BuildVocab.class);

    //-----------------------------------------------------------------------//
    //-- Nested Classes -----------------------------------------------------//
    //-----------------------------------------------------------------------//
    public static final class Options {
        @Parameter(
            names={"-u", "--unk"},
            required=false,
            description="The token to use for unknown words."
        )
        public String unk = "<unk>";
        
        @Parameter(
            names={"-n", "--top-n"},
            required=false,
            description="Use the top N most frequent words for the vocabulary."
        )
        public int topN = 500000;
        
        @Parameter(
            names={"-c", "--connection-properties"},
            required=true,
            description="A file containing the database connection properties."
        )
        public File connPropFile = null;
        
        @Parameter(
            names={"-V", "--vocab-file"},
            required=true,
            converter=FileConverter.class,
            description="The location of the serialized VocabCache to use."
        )
        public File vocabFile;
    }

    //-----------------------------------------------------------------------//
    //-- Static Fields ------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Managed Properties -------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Properties ---------------------------------------------------------//
    //-----------------------------------------------------------------------//
    //-- Stative ------------------------------------------------------------//

    //-- Functional ---------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Initialization -----------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Getters & Setters --------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Methods ------------------------------------------------------------//
    //-----------------------------------------------------------------------//
    /**
     * @param args
     * @throws IOException 
     */
    public static void main(String[] args) throws IOException {
        Options options = new Options();
        JCommander jc = new JCommander(options);
        StringBuilder usage = new StringBuilder();

        jc.setProgramName(BuildVocab.class.getSimpleName());
        jc.usage(usage);

        try {
            jc.parse(args);
        } catch (ParameterException e) {
            System.err.println(e.getMessage());
            System.err.println(usage);
            System.exit(-1);
        }
        
        DatabaseConnectionProperties connectionProperties = DatabaseConnectionProperties.load(options.connPropFile);

        TaggedDocumentIterator docItr = new TaggedDocumentIterator(connectionProperties);
        TaggedSentenceIterator senItr = new TaggedSentenceIterator(docItr);
        TokenizerFactory tf = new Tag2WordTokenizerFactory(new StandardTokenPreprocess(), options.unk);
        
        // Count all the words
        long total = 0;
        Object2LongOpenHashMap<String> counts = new Object2LongOpenHashMap<>();
        for (int i = 0; senItr.hasNext(); ++i) {
            String sentence = senItr.nextSentence();
            List<String> tokens = tf.create(sentence).getTokens();

            total += tokens.size();
            tokens.forEach(w -> counts.addTo(w, 1));
            if (i % 1000000 == 0) {
                LOG.debug("Finished processing sentence: {}", (i + 1));
            }
        }
        
        // Write the vocab to disk
        try (
            FileOutputStream fos = new FileOutputStream(options.vocabFile);
            GZIPOutputStream gos = new GZIPOutputStream(fos, 1 << 18);
            OutputStreamWriter osw = new OutputStreamWriter(gos);
            PrintWriter pw = new PrintWriter(osw);
        ) {
            AtomicInteger idx = new AtomicInteger(0);
            pw.printf("%d%n", total);
            pw.printf("%s %d %d%n", options.unk, idx.getAndIncrement(), 1);
            
            for (String tag : Tag2WordTokenizer.getTagSet()) {
                pw.printf("%s %d %d%n", tag, idx.getAndIncrement(), 1);
            }
            
            counts.entrySet().stream()
                .sorted((e1, e2) -> (int) (e2.getValue() - e1.getValue()))
                .filter(e -> !e.getKey().equals(options.unk))
                .filter(e -> !Tag2WordTokenizer.getTagSet().contains(e.getKey()))
                .limit(options.topN)
                .forEach(e -> pw.printf("%s %d %d%n", e.getKey(), idx.getAndIncrement(), e.getValue()));
        }
    }

    //-----------------------------------------------------------------------//
    //-- Actions ------------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Utility Methods ----------------------------------------------------//
    //-----------------------------------------------------------------------//

}
