/**
 * 
 */
package edu.usc.ict.nld.storyswapping.cli;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.converters.FileConverter;

import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations.EnhancedPlusPlusDependenciesAnnotation;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;
import edu.stanford.nlp.util.CoreMap;
import edu.usc.ict.nld.storyswap.io.IOUtils;

/**
 * @author rswanson
 *
 */
public class ExtractTemplate {
    //-----------------------------------------------------------------------//
    //-- Nested Classes -----------------------------------------------------//
    //-----------------------------------------------------------------------//
    public static final class Options {
        @Parameter(
            names={"-h", "--help"},
            help=true,
            description="Print these help options"
        )
        boolean help;
        
        @Parameter(
            names={"-f", "--input-text"},
            required=false,
            converter=FileConverter.class,
            description="A file containing a list of stories you would like transformed (separate each story by a blank line). "
        )
        File inputTextFile;
    }

    //-----------------------------------------------------------------------//
    //-- Static Fields ------------------------------------------------------//
    //-----------------------------------------------------------------------//
    public static final Set<String> VALID_RELATIONS = new HashSet<>(Arrays.asList("nsubj", "dobj", "iobj"));

    //-----------------------------------------------------------------------//
    //-- Managed Properties -------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Properties ---------------------------------------------------------//
    //-----------------------------------------------------------------------//
    //-- Stative ------------------------------------------------------------//

    //-- Functional ---------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Initialization -----------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Getters & Setters --------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Methods ------------------------------------------------------------//
    //-----------------------------------------------------------------------//
    /**
     * @param args
     * @throws IOException 
     * @throws FileNotFoundException 
     * @throws ClassCastException 
     * @throws ClassNotFoundException 
     */
    public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException, ClassCastException {
        Options options = new Options();
        JCommander jc = new JCommander(options);
        StringBuilder usage = new StringBuilder();
        
        jc.setProgramName(ExtractTemplate.class.getSimpleName());
        jc.usage(usage);
        
        try {
            jc.parse(args);
        } catch (ParameterException e) {
            System.err.println(e.getMessage());
            System.err.println(usage);
            System.exit(-1);
        }
        
        if (options.help) {
            System.err.println(usage.toString());
            return;
        }
                
        List<Annotation> storyAnnotations = getAnnotations(options);
        
        for (Annotation a : storyAnnotations) {
            List<CoreMap> sentences = a.get(SentencesAnnotation.class);
            for (CoreMap sentence : sentences) {
                SemanticGraph parse = sentence.get(EnhancedPlusPlusDependenciesAnnotation.class);
                List<IndexedWord> vertices = parse.vertexListSorted();
                for (IndexedWord w : vertices) {
                    List<SemanticGraphEdge> edges = parse.getIncomingEdgesSorted(w);
                    
                    // If the word is an end of sentence marker, a root word or
                    // one of the valid relations then output the word
                    if (w.tag().equals(".") || edges.isEmpty() || valid(edges)) {
                        System.out.printf("%s_%s ", w.word(), w.tag());
                    }
                }
            }
        }
    }
    
    private static boolean valid(List<SemanticGraphEdge> edges) {
        for (SemanticGraphEdge e : edges) {
            String rel = e.getRelation().getShortName();
            if (VALID_RELATIONS.contains(rel)) {
                return true;
            }
        }
        
        return false;
    }

    //-----------------------------------------------------------------------//
    //-- Actions ------------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Utility Methods ----------------------------------------------------//
    //-----------------------------------------------------------------------//
    private static StanfordCoreNLP loadPipeline() {
        Properties props = new Properties();
        props.setProperty("annotators", "tokenize, ssplit, pos, lemma, ner, depparse");
        
        StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
        
        return pipeline;
    }
    
    private static List<String> loadStories(File file) throws FileNotFoundException, IOException {
        List<String> stories = new ArrayList<>();
        String text = IOUtils.toString(file);
        
        StringBuilder sb = new StringBuilder();
        for (String line : text.split("\\r?\\n")) {
            if (line.matches("^\\s*$") && sb.length() > 0) {
                stories.add(sb.toString());
                sb = new StringBuilder();
            } else {
                sb.append(new String(line)).append('\n');
            }
        }
        
        if (sb.length() > 0) {
            stories.add(sb.toString());
        }
        
        return stories;
    }
    
    private static List<Annotation> getAnnotations(Options options) throws FileNotFoundException, IOException, ClassNotFoundException, ClassCastException {
        List<String> stories = loadStories(options.inputTextFile);
        List<Annotation> annotations = stories.stream().map(s -> new Annotation(s)).collect(Collectors.toList());
        
        StanfordCoreNLP pipeline = loadPipeline();
        pipeline.annotate(annotations);
        
        return annotations;
    }
}
