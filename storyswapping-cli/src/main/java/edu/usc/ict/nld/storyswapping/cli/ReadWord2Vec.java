/**
 * 
 */
package edu.usc.ict.nld.storyswapping.cli;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.deeplearning4j.models.word2vec.Word2Vec;

import edu.usc.ict.nld.storyswap.io.SerializationUtils;

/**
 * @author rswanson
 *
 */
public class ReadWord2Vec {
    /**
     * @param args
     */
    public static void main(String[] args) {
        File file = new File(args[0]);

        Word2Vec w2v = SerializationUtils.readCompressedBinary(file);
        
        int n = 20;
        List<String> nearestDay = new ArrayList<>(w2v.wordsNearest("day", n));
        List<String> nearestSweden = new ArrayList<>(w2v.wordsNearest("sweden", n));
        List<String> nearestMan = new ArrayList<>(w2v.wordsNearest("man", n));
        
        System.err.printf("%30s %30s %30s%n", "day", "sweden", "man");
        for (int i = 0; i < n; ++i) {
            String day = nearestDay.get(i);
            String sweden = nearestSweden.get(i);
            String man = nearestMan.get(i);
            double daySim = w2v.similarity("day", day);
            double swedenSim = w2v.similarity("sweden", sweden);
            double manSim = w2v.similarity("man", man);
            System.err.printf("%20s %8.7f %20s %8.7f %20s %8.7f%n", day, daySim, sweden, swedenSim, man, manSim);
        }
        
    }
}
