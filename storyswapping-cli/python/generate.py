#!/usr/bin/env python
'''
Created on Mar 17, 2017

@author: reid
'''
import gzip
import numpy as np
import sys

from bidict import bidict
from neon.backends import gen_backend
from neon.initializers import GlorotUniform
from neon.layers import LSTM, Affine
from neon.models import Model
from neon.transforms import Logistic, Tanh, Softmax
from neon.util.argparser import NeonArgparser

# TODO put vocabulary in a different module
class Vocabulary(object):
    def __init__(self, filename):
        self.vocab = bidict()
        self.counts = dict()
        
        with gzip.open(filename, 'rb') as f:
            self.total_words = int(f.readline())
            for line in f:
                label, idx, count = line.strip().split()
                self.vocab[label] = int(idx)
                self.counts[idx] = int(count)
                
    def __str__(self):
        return "\n".join(["{0} {1}".format(x, y) for x, y in self.vocab.iteritems()])
    
    def indexOf(self, word):
        return self.vocab.get(word, -1)
    
    def wordAt(self, idx):
        return self.vocab.inv.get(idx, "<unk>")
    
    def size(self):
        return len(self.vocab)


def build_layers(vocab_size, args):
    init = GlorotUniform()
    #layers = [LookupTable(vocab_size=vocab_size, embedding_dim=args.embedding_dim, init=init)]
    layers = []
    layers += [LSTM(args.hidden, init, activation=Logistic(), gate_activation=Tanh()) for _ in xrange(args.num_layers)]
    layers += [Affine(vocab_size, init, bias=init, activation=Softmax())]
    
    return layers

def sample(prob):
    prob = prob / (prob.sum() + 1e-6)
    return np.argmax(np.random.multinomial(1, prob, 1))

parser = NeonArgparser(__doc__)

parser.add_argument("--vocab-file", dest="vocab", help="The vocabulary file")
parser.add_argument("--num-layers", dest="num_layers", type=int, help="The number of hidden LSTM layers in the saved model")
parser.add_argument("--hidden-units", dest="hidden", type=int, default=300, help="The number of hidden units in each LSTM layer")
parser.add_argument("--stderr", dest="stderr", help="Write the generated output to stderr instead of stdout")
parser.add_argument("--length", dest="length", type=int, default=100, help="The number of tokens to generate")
parser.add_argument("--input", dest="input", default="\x03 once upon a time", help="")

args = parser.parse_args()

gen_backend(backend=args.backend, rng_seed=args.rng_seed, batch_size=1)

vocab = Vocabulary(args.vocab)
nn = Model(layers=build_layers(vocab.size(), args))
nn.load_params(args.model_file, load_states=False)
nn.initialize(dataset=(vocab.size(), 1))

out = sys.stderr if args.stderr else sys.stdout

output_words = []
seed_words = [t for t in args.input.lower().split()]
x = nn.be.zeros((vocab.size(), 1))

for s in seed_words:
    x.fill(0)
    x[vocab.indexOf(s), 0] = 1
    y = nn.fprop(x)
    
for _ in xrange(args.length):
    pred = sample(y.get()[:, -1])
    output_words.append(vocab.wordAt(int(pred)))
    
    x.fill(0)
    x[int(pred), 0] = 1
    y = nn.fprop(x)

out.write("{}\n".format(" ".join(seed_words + output_words)))
out.flush()
