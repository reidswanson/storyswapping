'''
Created on May 27, 2017

This script imports the part of speech tagged spinn3r stories into a mysql 
database. This is a quick and dirty script with very little error checking.

@author: reid
'''

import argparse
import os
import backports.lzma
import sys

import MySQLdb as my

def listdir_absolute(root, dirname):
    """
    Simple utility function to return the absolute paths of all files in the 
    given directory.
    """
    return [os.path.join(root, f) for f in os.listdir(dirname)]

def read_properties(filename):
    """
    Read the database connection properties into a dictionary.
    """
    with open(filename) as f:
        return dict(line.strip().split('=', 1) for line in f)

def replace_last(original, old, new):
    idx = original.rfind(old)
    return original[:idx] + new + original[idx+1:]
    
def create_database(dbprops, drop_existing_db):
    """
    Create the database to store the stories (if it does not already exist).
    """
    db = my.connect(
        host=dbprops.get("host", "localhost"),
        port=int(dbprops.get("port", 3306)),
        user=dbprops.get("username", "root"),
        passwd=dbprops.get("password", None)
    )
    
    dbname = dbprops.get("database")
    table_name = dbprops.get("table")
    
    # NOTE: these are vulnerable to SQL injections, but it shouldn't be a problem here
    db_exists_sql = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '{}'"
    db_drop_sql = "DROP DATABASE {}"
    create_db_sql = "CREATE DATABASE {}"
    create_table_sql = (
        """
        CREATE TABLE {}.{} (
            `id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
            `story_id` INT UNSIGNED NOT NULL,
            `sentence_num` INT UNSIGNED NOT NULL,
            `sentence_pos` MEDIUMTEXT NOT NULL,
            
            INDEX (`story_id`),
            INDEX (`sentence_num`),
            INDEX (`story_id`, `sentence_num`)
        );
        """
    )
    cursor = db.cursor()
    cursor.execute(db_exists_sql.format(dbname))
    
    db_exists = True if cursor.fetchone() is not None else False
    if db_exists and drop_existing_db:
        cursor.execute(db_drop_sql.format(dbname))
    elif db_exists:
        sys.stderr.write("The database already exists. Please use a new database for importing.\n")
        sys.exit(-1)
    
    cursor.execute(create_db_sql.format(dbname))
    cursor.execute(create_table_sql.format(dbname, table_name))
    
    cursor.close()
    db.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    
    parser.add_argument("--input-dir", required=True, help="The base directory where the stories are located.")
    parser.add_argument("--connection-properties", required=True, help="A connection properties file (the same format as the Java connection properties file)")
    parser.add_argument("--drop-database", required=False, action='store_true', help="If this option is set and a database exists with the name given in the properties file it will be dropped and a new one will be created to populate with the given data.")
    
    args = parser.parse_args()
    
    files = filter(os.path.isfile, listdir_absolute(args.input_dir, args.input_dir))
    dbprops = read_properties(args.connection_properties)
    
    create_database(dbprops, args.drop_database)
    
    db = my.connect(
        host=dbprops.get("host", "localhost"),
        port=int(dbprops.get("port", 3306)),
        user=dbprops.get("username", "root"),
        passwd=dbprops.get("password", None)
    )
    cursor = db.cursor()
    
    database = dbprops.get("database")
    table = dbprops.get("table")
    
    story_id = 1
    sentence_num = 0
    batch = []
    for f in files:
        with open(f) as compressed:
            sys.stderr.write("Processing file: {}\n".format(f))
            with backports.lzma.LZMAFile(compressed) as uncompressed:
                for line in uncompressed:
                    sentence = line.strip()
                    
                    if not sentence:
                        story_id += 1
                        sentence_num = 0
                    else:
                        sentence = " ".join([replace_last(t, '/', '_') for t in sentence.split()])
                        batch.append((story_id, sentence_num, sentence))
                        sentence_num += 1
            
            sys.stderr.write("Inserting {} sentences\n".format(len(batch)))
            cursor.executemany(
                """
                INSERT INTO {}.{} 
                    (story_id, sentence_num, sentence_pos)
                VALUES (%s, %s, %s)
                """.format(database, table),
                batch
            )
            db.commit()
            batch = []
                        
    cursor.executemany(
        """
        INSERT INTO {}.{} 
            (story_id, sentence_num, sentence_pos)
        VALUES (%d, %d, %s)
        """,
        batch
    )
    
    cursor.close()
    db.close()
        
    