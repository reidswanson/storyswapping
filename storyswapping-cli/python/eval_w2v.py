'''
Created on Apr 3, 2017

@author: rswanson
'''

import argparse
import gensim
import logging

from gensim.models.keyedvectors import KeyedVectors

parser = argparse.ArgumentParser()
parser.add_argument("--w2v-file", dest="w2v", required=True, help="The vocabulary file")
parser.add_argument("--simlex-file", dest="simlex", required=True, help="The SimLex similarity file for performing the gensim evaluation")
parser.add_argument("--questions-file", dest="questions", required=True, help="The questions file for performing the gensim accuracy")

args = parser.parse_args()

logging.getLogger().setLevel(logging.INFO)
gensim.models.keyedvectors.logger.setLevel(logging.INFO)

# Load the Word2Vec file
logging.info("Evaluating")
w2v = KeyedVectors.load_word2vec_format(args.w2v, binary=True)
w2v.evaluate_word_pairs(args.simlex, delimiter=' ')
w2v.accuracy(args.questions)
