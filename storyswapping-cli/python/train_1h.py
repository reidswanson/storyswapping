#!/usr/bin/env python
'''
Created on Mar 13, 2017

@author: reid
'''

import itertools
import gzip
import numpy as np
import struct
import sys

from bidict import bidict
from collections import deque
from neon import logger as nlog
from neon import NervanaObject
from neon.backends import gen_backend, cleanup_backend
from neon.callbacks.callbacks import Callback, Callbacks
from neon.data import NervanaDataIterator
from neon.initializers import GlorotUniform
from neon.layers import GeneralizedCost, LSTM, Affine
from neon.models import Model
from neon.optimizers import Adam, RMSProp
from neon.transforms import Logistic, Tanh, Softmax, CrossEntropyMulti
from neon.util.argparser import NeonArgparser
from subprocess import Popen, PIPE


class Vocabulary(object):
    def __init__(self, filename):
        self.vocab = bidict()
        self.counts = dict()
        
        with gzip.open(filename, 'rb') as f:
            self.total_words = int(f.readline())
            for line in f:
                label, idx, count = line.strip().split()
                self.vocab[label] = int(idx)
                self.counts[idx] = int(count)
                
    def __str__(self):
        return "\n".join(["{0} {1}".format(x, y) for x, y in self.vocab.iteritems()])
    
    def indexOf(self, word):
        return self.vocab.get(word, -1)
    
    def wordAt(self, idx):
        return self.vocab.inv.get(idx, "<unk>")
    
    def size(self):
        return len(self.vocab)

class OneHotIterator(NervanaDataIterator):
    def __init__(self, filename, limit, time_steps):
        super(OneHotIterator, self).__init__(name=None)
        
        self.filename = filename
        self.file = None
        self.total_items = limit
        self.time_steps = time_steps
        self.extra_tokens = self.total_items % (self.be.bsz * self.time_steps)
        self.nbatches = (self.total_items - self.extra_tokens) // (self.be.bsz * self.time_steps)
        self.ndata = self.nbatches * self.be.bsz
        self.nfeatures = self._read_nfeatures();
        self.shape = (self.nfeatures, self.time_steps) 
        self.nclass = self.nfeatures
        self.batch_index = 0;
        self.sentinel_count = 0;
        self.document = deque()
        self.sequence = deque()
        
        self.dev_x = self.be.iobuf((self.nclass, self.time_steps))
        self.dev_y = self.be.iobuf((self.nclass, self.time_steps))
        self.dev_lbl = self.be.iobuf(self.time_steps, dtype=np.int32)
        self.dev_lblflat = self.dev_lbl.reshape((1, -1))
        self.reset()
        
    def __iter__(self):
        self.batch_index = 0
        while self.batch_index < self.nbatches:
            features = []
            labels = []
            for _ in xrange(self.be.bsz):
                features.append(np.asarray([x for x in itertools.islice(self.sequence, 0, self.time_steps)]))
                labels.append(np.asarray([x for x in itertools.islice(self.sequence, 1, self.time_steps + 1)]))
                
                for _ in xrange(self.time_steps): 
                    self.sequence.popleft()
                    if not self._add_to_sequence():
                        self.sequence.append(0)
                        self.sentinel_count += 1
            
            xbatch = np.stack(features, axis=1)
            ybatch = np.stack(labels, axis=1)
            
            self.dev_lbl.set(ybatch)
            self.dev_y[:] = self.be.onehot(self.dev_lblflat, axis=0)
            
            self.dev_lbl.set(xbatch)
            self.dev_x[:] = self.be.onehot(self.dev_lblflat, axis=0)
            
            yield self.dev_x, self.dev_y    
        
    def reset(self):
        self.batch_index = 0
        self.sentinel_count = 0
        
        if self.file is not None: self.file.close()
        
        self.file = gzip.open(self.filename, 'rb')
        _ = self._read_int(self.file)
        
        self.document.clear()
        self.sequence.clear()
        
        self._read_document()
        self._fill_sequence()
    
    def close(self):
        if self.file is not None: self.file.close()
        
    def _read_nfeatures(self):
        with gzip.open(self.filename, 'rb') as f:
            nfeatures = self._read_int(f)
        return nfeatures
    
    def _read_batch(self):
        size = self._read_int(self.file)
        
        return [self._read_int(self.file) for _ in xrange(size)]
    
    def _read_document(self):
        if self.sentinel_count > 0: return
        
        self.document.extend(self._read_batch())
        
    def _fill_sequence(self):
        while len(self.sequence) < self.time_steps + 1:
            if not self._add_to_sequence(): break
            
    def _add_to_sequence(self):
        if len(self.document) == 0: self._read_document()
        if len(self.document) == 0: return False
        
        self.sequence.append(self.document.popleft())
        
        return True
            
    def _read_int(self, f):
        return struct.unpack(">i", f.read(4))[0]
        
class GenerateCallback(Callback):
    def __init__(self, vocab, args, epoch_freq=1):
        super(GenerateCallback, self).__init__(epoch_freq, args.genfreq)
        self.vocab = vocab
        self.args = args
    
    def on_minibatch_end(self, callback_data, model, epoch, minibatch):
        if minibatch % self.minibatch_freq == 0:
            #nlog.display("Saving model to disk")
            model.serialize(fn=self.args.save_path, keep_states=True)
            
            #nlog.display("Loading model from disk")
            
            p = Popen(["./generate.py", "-b", "cpu", "--vocab-file", args.vocab, "--num-layers", str(args.num_layers), "--hidden-units", str(args.hidden), "--model_file", args.save_path, "--length", str(args.genlen)], stdout=PIPE)
            result = p.stdout.read()
            
            nlog.display("{:6d} {}".format(minibatch, result))
            # This seems to be causing a memory leak
            # TODO write a script that generates from a model and call it here as
            # a subprocess :-/
            
            
    def should_fire(self, callback_data, model, time, freq):
        return True
    
    def sample(self, prob):
        prob = prob / (prob.sum() + 1e-6)
        return np.argmax(np.random.multinomial(1, prob, 1))
    
class LogCostCallback(Callback):
    def __init__(self, minibatch_freq=10, lr_decay = 0.75):
        super(LogCostCallback, self).__init__(epoch_freq=1)
        
        self.minibatch_freq = minibatch_freq
        self.lr_decay = lr_decay
        self.sizes = [100, 1000, 5000, 10000]
        
    def on_train_begin(self, callback_data, model, epochs):
        self.prev_cost = 10000000.0
        self.cost_history = [deque([], maxlen=int(wsz)) for wsz in self.sizes]

    def on_minibatch_end(self, callback_data, model, epoch, minibatch):
        for i in xrange(len(self.sizes)): 
            self.cost_history[i].append(model.cost.cost)
        
        if minibatch % self.minibatch_freq == 0:
            output = []
            slopes = []
            for i in xrange(len(self.sizes)):
                x = np.array([j for j,_ in enumerate(self.cost_history[i])])
                y = np.array([float(v) for v in list(self.cost_history[i])])
                a = np.vstack([x, np.ones(len(x))]).T
                m, _ = np.linalg.lstsq(a, y)[0]
                slopes.append(m)
                mean_cost = sum(self.cost_history[i]) / len(self.cost_history[i])
                #output.append("- ({:d}) is {:6.3f}".format(self.sizes[i], float(mean_cost)))
                output.append("- ({:d}) is {:6.3f} [{:+2.1e}]".format(self.sizes[i], float(mean_cost), m))
            
            nlog.display("Training cost at ({:7d}) is {:6.3f} with lr {:3.2e} {}".format(minibatch, float(model.cost.cost), model.optimizer.learning_rate, " ".join(output)))
            
            if slopes[2] > 0:
                model.optimizer.learning_rate *= self.lr_decay
    
    def should_fire(self, callback_data, model, time, freq):
        return True

def build_layers(vocab_size, args):
    init = GlorotUniform()
    #layers = [LookupTable(vocab_size=vocab_size, embedding_dim=args.embedding_dim, init=init)]
    layers = []
    layers += [LSTM(args.hidden, init, activation=Logistic(), gate_activation=Tanh()) for _ in xrange(args.num_layers)]
    layers += [Affine(vocab_size, init, bias=init, activation=Softmax())]
    
    return layers
        
def train(args, vocab):
    limit = vocab.total_words if args.limit == 0 else args.limit
     
    gen_backend(backend=args.backend, rng_seed=args.rng_seed, batch_size=args.batch_size)
    itr = OneHotIterator(args.data, limit, args.steps)
    model = Model(layers=build_layers(vocab.size(), args=args))
    cost = GeneralizedCost(costfunc=CrossEntropyMulti(usebits=True))
    #optimizer = Adam(learning_rate=args.lr, gradient_clip_value=5, stochastic_round=args.rounding)
    optimizer = RMSProp(learning_rate=args.lr, decay_rate=args.rms_decay, gradient_clip_value=5, stochastic_round=args.rounding)
    callbacks = Callbacks(model, eval_set=None, **args.callback_args)
    
    callbacks.add_callback(GenerateCallback(vocab, args))
    callbacks.add_callback(LogCostCallback(args.minifreq, args.lr_decay))
    callbacks.callbacks[0].wsz = 50
    callbacks.callbacks[1].minibatch_freq = args.minifreq
    
    nlog.display("Start training model with {} hidden units in {} LSTM layers".format(args.hidden, args.num_layers))
    
    model.fit(itr, optimizer=optimizer, num_epochs=args.epochs, cost=cost, callbacks=callbacks)
    
    itr.close()

parser = NeonArgparser(__doc__)
parser.add_argument("--vocab-file", dest="vocab", help="The vocabulary file")
parser.add_argument("--data-file", dest="data", help="The input data file")
parser.add_argument("--limit", dest="limit", type=int, default=0, help="Limit the number of tokens to read in an epoch or 0 for the whole dataset")
parser.add_argument("--hidden-units", dest="hidden", type=int, default=300, help="The number of hidden units in each LSTM layer")
parser.add_argument("--num-layers", dest="num_layers", type=int, default=2, help="The number of hidden LSTM layers")
parser.add_argument("--time-steps", dest="steps", type=int, default=20, help="The number of time steps")
parser.add_argument("--minibatch-freq", dest="minifreq", type=int, default=100, help="How often (in minibatches) to output cost information")
parser.add_argument("--generate-freq", dest="genfreq", type=int, default=5000, help="Generate text from the model every this many minibatches")
parser.add_argument("--generate-length", dest="genlen", type=int, default=100, help="Generate text of this length")
parser.add_argument("--embedding-dim", dest="embedding_dim", type=int, default=100, help="The embedding dimension of the LookupTable")
parser.add_argument("--learning-rate", dest="lr", type=float, default=2e-3, help="The learning rate for training")
parser.add_argument("--learning-rate-decay", dest="lr_decay", type=float, default=0.99, help="The learning rate for training")
parser.add_argument("--rms-decay-rate", dest="rms_decay", type=float, default=0.95, help="The RMS decay rate")

args = parser.parse_args()

vocab = Vocabulary(args.vocab)

train(args, vocab)
