#!/usr/bin/env python
'''
Created on Mar 17, 2017

@author: reid
'''
import numpy as np
import sys

from gensim.models.keyedvectors import KeyedVectors
from neon.backends import gen_backend
from neon.initializers import GlorotUniform
from neon.layers import LSTM, Affine
from neon.models import Model
from neon.transforms import Logistic, Tanh, Softmax
from neon.util.argparser import NeonArgparser


def build_layers(vocab_size, args):
    init = GlorotUniform()
    layers = []
    layers += [LSTM(args.hidden, init, activation=Logistic(), gate_activation=Tanh()) for _ in xrange(args.num_layers)]
    layers += [Affine(vocab_size, init, bias=init, activation=Softmax())]
    
    return layers

def sample(prob):
    prob = prob / (prob.sum() + 1e-6)
    return np.argmax(np.random.multinomial(1, prob, 1))

parser = NeonArgparser(__doc__)

parser.add_argument("--w2v-file", dest="w2v", help="The vocabulary file")
parser.add_argument("--num-layers", dest="num_layers", type=int, help="The number of hidden LSTM layers in the saved model")
parser.add_argument("--hidden-units", dest="hidden", type=int, default=300, help="The number of hidden units in each LSTM layer")
parser.add_argument("--stderr", dest="stderr", help="Write the generated output to stderr instead of stdout")
parser.add_argument("--length", dest="length", type=int, default=100, help="The number of tokens to generate")
parser.add_argument("--input", dest="input", default="\x03 once upon a time", help="")

args = parser.parse_args(gen_be=False)

gen_backend(backend="cpu", rng_seed=args.rng_seed, batch_size=1)

w2v = KeyedVectors.load_word2vec_format(args.w2v, binary=True)
nn = Model(layers=build_layers(len(w2v.vocab), args))
nn.load_params(args.model_file, load_states=False)
nn.initialize(dataset=(w2v.syn0.shape[1], 1))

out = sys.stderr if args.stderr else sys.stdout

output_words = []
seed_words = [t for t in args.input.lower().split()]
x = nn.be.zeros((w2v.syn0.shape[1], 1))

for s in seed_words:
    x.fill(0)
    x[:, 0] = w2v[s]
    y = nn.fprop(x)
    
for _ in xrange(args.length):
    pred = sample(y.get()[:, -1])
    output_words.append(w2v.index2word[int(pred)])
    
    x.fill(0)
    x[:, 0] = w2v.syn0[int(pred)]
    y = nn.fprop(x)

out.write("{}\n".format(" ".join(seed_words + output_words)).encode("utf-8"))
out.flush()
