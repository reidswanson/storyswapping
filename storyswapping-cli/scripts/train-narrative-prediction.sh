#!/bin/bash

CP=../target/storyswapping-cli-0.0.1-SNAPSHOT.jar:../target/lib/*

# Make sure the graphics card is running in performance mode
nvidia-settings -a [gpu:0]/GPUPowerMizerMode=1

l=3
s=20
b=124
h=600
r=0.003

java -Xmx6G -cp $CP edu.usc.ict.nld.storyswapping.cli.TrainNarrativePredictionModel \
    --dataset-file /mnt/data/story-swapping/narrative-prediction.na.1h.v10000.data.ser.gz \
    --vocabulary-file /mnt/data/narrative-archive/narrative-archive.v10000.vocab.txt.gz \
    --model-file /mnt/data/story-swapping/narrative-prediction-v10k-s$s-l$l-h$h-r$r.nn.ser.gz \
    --one-hot \
    --batch-buffer-size 1 \
    --sequence-length $s \
    --minibatch-size $b \
    --hidden-layers $l \
    --hidden-units $h \
    --max-epochs 10 \
    --updater RMSProp \
    --learning-rate $r \
    --l2 0 \
    --lr-boost 1.25 \
    &> ../data/dl4j.train.1h.l$l.t$s.h$h.r$r.log