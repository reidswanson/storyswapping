#!/bin/bash

CP=../target/storyswapping-cli-0.0.1-SNAPSHOT.jar:../target/lib/*

# Make sure the graphics card is running in performance mode
nvidia-settings -a [gpu:0]/GPUPowerMizerMode=1

for b in $(seq 50 1 250)
do
    echo "$(date) - Batch size: $b"
    java -Xmx6G -cp $CP edu.usc.ict.nld.storyswapping.cli.TrainNarrativePredictionModel \
        --dataset-file /mnt/data/story-swapping/narrative-prediction.na.1h.v10000.data.ser.gz \
        --vocabulary-file /mnt/data/narrative-archive/narrative-archive.v10000.vocab.txt.gz \
        --model-file /mnt/data/story-swapping/narrative-prediction-v10k-s30-nl2-h400-lr0.3.nn.ser.gz \
        --one-hot \
        --batch-buffer-size 2 \
        --sequence-length 20 \
        --minibatch-size $b \
        --hidden-layers 2 \
        --hidden-units 300 \
        --max-epochs 10 \
        --updater Adam \
        --learning-rate 0.1 \
        --l2 0 \
        --lr-boost 1.5 \
        --lr-decay-policy Inverse \
        --lr-policy-power 1e-7 &> batch-size-$b.log
done