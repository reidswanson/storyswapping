#!/bin/bash

#perl -pe 's/.*?Training cost at \(\s*(\d+)\) is\s+(\S+).*?\(\d+\) is\s+(\S+).*\(1000\) is\s+(\S+).*\(5000\) is\s+(\S+).*\(10000\) is\s+(\S+).*/\1 \2 \3 \4 \5 \6/' | perl -ne '/^\d+ .*/ && print'
#perl -pe 's/.*?Score at iteration\s+\d+ is\s+(\S+).*?\(\d+\)\s+(\S+).*\(\S+\)\s+(\S+).*\(\S+\)\s+(\S+).*\(\d+\)\s+(\S+).*\(\d+\)\s+(\S+).*/\1 \2 \3 \4 \5 \6/' | perl -ne '/^\d+ .*/ && print'
grep 'Score at' | perl -pe 's/.*?Score at iteration (\d+) is\s+(\S+).*?\(\S+?\)\s+(\S+).*?\(\S+?\)\s+(\S+).*?\(\S+?\)\s+(\S+).*?\(\S+?\)\s+(\S+).*?\(\S+?\)\s+(\S+).*/\1 \2 \3 \4 \5 \6 \7/'
