#!/usr/bin/python           


"""
Author: Ignacio Martin Queralt & Sarah Fillwock
Date: 08/09/17

Script that integrates the POV transformer with the
attribute extraction system

"""

import socket               # Import socket module

from urllib import urlopen


s = socket.socket()         # Create a socket object
host = socket.gethostname() # Get local machine name
port = 12345                # Reserve a port for your service.

s.connect((host, port))     # connect to the port
utterance = raw_input("Enter an utterance: ") 
# example utterance: "my wife and i went on vacation to argentina"
s.send(utterance) #send a message on the port to the connected service

attr_pair = s.recv(1024)[1:-1].split("->")

attr_val = attr_pair[1].title()
attr_type = attr_pair[0].split(".")[1]
print "Type of location attribute: " + attr_pair[0].split(".")[0]
print "Value of attribute: " + attr_val

url = 'http://localhost:4567/' + attr_type + '/' + attr_val
story = urlopen(url)
story_words = []
for line in story:
	line_words = line.split()
	for words in line_words:
		story_words.append(words)
answer = ' '.join(story_words)

print answer
s.close                     # Close the socket when done