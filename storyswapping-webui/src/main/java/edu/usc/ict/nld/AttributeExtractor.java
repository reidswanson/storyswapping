package edu.usc.ict.nld;

import java.io.BufferedReader;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.List;
import java.util.Properties;

import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.usc.ict.nld.storyswap.PointOfView;
import edu.usc.ict.nld.storyswap.PovTransformer;
import edu.usc.ict.nld.storyswap.PersonalPronouns;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.simple.Sentence;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.stanford.nlp.util.CoreMap;


import edu.stanford.nlp.simple.Document;
import edu.stanford.nlp.simple.Sentence;

public class AttributeExtractor {

	public static void main(String[] args) throws IOException {

		String csvFile = "/home/igna/Documents/story-s/storyswapping/storyswapping-webui/data/ROCStories.csv";
	    String line = "";

	    PrintWriter pw = new PrintWriter(new File("UtterancesAndDates.csv"));
	    StringBuilder sb = new StringBuilder();
	    int counter = 0;
	    	    
	    try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
        	
            while ((line = br.readLine()) != null) {
            	counter = counter + 1;
            	if (line.charAt(0) == '"' && line.charAt(line.length() - 1) == '"') {
            		line = line.substring(1, line.length() - 1);
            	}
            	Document doc = new Document(line);
                List<Sentence> sentences = doc.sentences();
                for (int i = 0 ; i < sentences.size() ; i++) {
                	String utterance = sentences.get(i).toString();
                	String attr = getAttr(utterance, "ORGANIZATION");
                	if (!attr.equals("")) {
                		System.out.println(attr);
                		sb.append(utterance);
                        sb.append('+');
                        sb.append("Organization");
                        sb.append('+');
                        sb.append(attr);
                        sb.append("\n");
                	}
                }
        		if (counter % 1000 == 0) {
        			System.out.println(counter);
        		}
            }
            pw.write(sb.toString());
            pw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    
	}
	
    public static String getAttr(String story, String attribute) {
    	String answer = "";
    	Sentence sent = new Sentence(story);
    	List<String> nerTags = sent.nerTags();

    	for (int i = 0; i < nerTags.size(); i++) {
    		String tag = nerTags.get(i).toString();
			if (tag.equals(attribute)) {
				if (i + 1 < nerTags.size()) {
					String tag2 = nerTags.get(i + 1).toString();
					if (tag2.equals(attribute)) {
						return sent.word(i) + " " + sent.word(i + 1);
					} else {
						return sent.word(i);
					}
				} else {
					return sent.word(i);
				}
			}
		}
    	return answer;
    }

}
