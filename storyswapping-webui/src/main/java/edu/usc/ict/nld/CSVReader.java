package edu.usc.ict.nld;

import java.io.BufferedReader;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

import edu.stanford.nlp.simple.Sentence;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class CSVReader {

    public static void main(String[] args) throws IOException {
    	
    	String ORGANIZATION = "ORGANIZATION";
    	String LOCATION = "LOCATION";
    	String DATE = "DATE";
        String csvFile = "/home/igna/Documents/story-s/storyswapping/storyswapping-webui/data/ROCStories.csv";
        String line = "";
        
        JSONObject allData = new JSONObject();
		JSONArray stories = new JSONArray();
		int counter = 0;
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
        	
            while ((line = br.readLine()) != null) {
            	counter = counter + 1;
        		JSONObject story = new JSONObject();
            	if (line.charAt(0) == '"' && line.charAt(line.length() - 1) == '"') {
            		line = line.substring(1, line.length() - 1);
            	}
        		story.put("Text", line);
            	String location = getAttr(line,LOCATION); 
        		story.put("Location", location);
            	String organization = getAttr(line,ORGANIZATION); 
        		story.put("Organization", organization);
            	String date = getAttr(line,DATE); 
        		story.put("Date", date);
        		stories.add(story);
        		if (counter % 1000 == 0) {
        			System.out.println(counter);
        		}
            }
            allData.put("Data", stories);

        } catch (IOException e) {
            e.printStackTrace();
        }
        
        
		try (FileWriter file = new FileWriter("StoriesAndLocations.json")) {
			file.write(allData.toJSONString());
			System.out.println("Successfully Copied JSON Object to File...");
			//System.out.println("\nJSON Object: " + allData);
		}
        

    }
    
    public static String getAttr(String story, String attribute) {
    	String answer = "";
    	Sentence sent = new Sentence(story);
    	List<String> nerTags = sent.nerTags();

    	for (int i = 0; i < nerTags.size(); i++) {
    		String tag = nerTags.get(i).toString();
			if (tag.equals(attribute)) {
				if (i + 1 < nerTags.size()) {
					String tag2 = nerTags.get(i + 1).toString();
					if (tag2.equals(attribute)) {
						return sent.word(i) + " " + sent.word(i + 1);
					} else {
						return sent.word(i);
					}
				} else {
					return sent.word(i);
				}
			}
		}
    	return answer;
    }

}