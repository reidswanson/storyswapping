package edu.usc.ict.nld;
import static spark.Spark.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.Random;

import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.usc.ict.nld.storyswap.PointOfView;
import edu.usc.ict.nld.storyswap.PovTransformer;
import edu.usc.ict.nld.storyswap.io.IOUtils;
import edu.usc.ict.nld.storyswap.PersonalPronouns;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class Server {
    public static void main(String[] args) throws FileNotFoundException, IOException, ParseException {
    	
    	// Load the stories from JSON file
    	String jsonFile = "/home/igna/Documents/story-s/storyswapping/storyswapping-webui/data/StoriesAndLocations.json";
        JSONParser parser = new JSONParser();
        Object rawData = parser.parse(new FileReader(jsonFile));
        JSONObject jsonStories = (JSONObject) rawData;
        JSONArray Data = (JSONArray) jsonStories.get("Data");
        
        
        // Load the Stanford CoreNLP tool
        PovTransformer transformer = new PovTransformer();
        System.out.println("Start Pipeline Time");
        StanfordCoreNLP pipeline = loadPipeline();
        
        // Some information about the attributes of these stories
        int oneattr = 0;
        int twoattr = 0;
        int threeattr = 0;
    	for (int i = 0 ; i < Data.size() ; i++) {
            int counter = 0;
    		JSONObject story = (JSONObject) Data.get(i);
    		String date = (String) story.get("Date");
    		String location = (String) story.get("Location");
    		String organization = (String) story.get("Organization");
    		//System.out.println(date + location + organization);
    		
    		if (!date.equals("")) counter++;
    		if (!location.equals("")) counter++;
    		if (!organization.equals("")) counter++;
    		
    		if (counter == 1) {
    			oneattr++;
    		} else if (counter == 2) {
    			twoattr++;
    		} else if (counter == 3) {
    			threeattr++;
    		}
    	}
    	System.out.println("Stories with one attribute: " + oneattr);
    	System.out.println("Stories with two attributes: " + twoattr);
    	System.out.println("Stories with three attributes: " + threeattr);

        
        // Get story from particular attribute with fixed POV
		get("/:attribute1/:value1", (request, response) -> {
		//get("/:attribute1/:value1/:attribute2/:value2", (request, response) -> {

			String query1 = request.params(":value1");
			//String query2 = request.params(":value2");
			String answer = "";
			List<String> answers = new ArrayList<String>();

        	for (int i = 0 ; i < Data.size() ; i++) {
        		JSONObject story = (JSONObject) Data.get(i);
        		String attval1 = request.params(":attribute1");
        		String att1 = attval1.substring(0, 1).toUpperCase() + attval1.substring(1);
        		String attribute1 = (String) story.get(att1);
        		
        		//String attval2 = request.params(":attribute2");
        		//String att2 = attval2.substring(0, 1).toUpperCase() + attval2.substring(1);
        		//String attribute2 = (String) story.get(att2);

        		if (attribute1 != null && attribute1.equals(query1)) {
        			answer = (String) story.get("Text");
        			answers.add(answer);
        		}
        	}
        	Random randomizer = new Random();
        	answer = answers.get(randomizer.nextInt(answers.size()));

        	
        	// Baseline of changing POV of the Story
            PointOfView targetPov = new PointOfView(PersonalPronouns.Number.SINGULAR,
            		PersonalPronouns.Person.THIRD,PersonalPronouns.Gender.MASCULINE);
            List<String> sentences = new ArrayList<String>();
            sentences = loadStory(answer);
            List<Annotation> storyAnnotations = sentences.stream().map(s -> new Annotation(s)).collect(Collectors.toList());
            pipeline.annotate(storyAnnotations);
            String POVtransformedStory = "";
            
            // Changing the POV of the story
            for (Annotation story : storyAnnotations) {
                String transformed = transformer.transform(story, targetPov);
                POVtransformedStory = POVtransformedStory + transformed;
            }

        	
            // Baseline of adding protagonists of the story
            String charactersFile = "../storyswapping-webui/data/characters.txt";
            File file = new File(charactersFile);
            String[] newCharacters = loadNewCharacters(file);
            
            // Baseline of introducing a opening line of a protagonist
            String opener = loadOpener("A", newCharacters);
            
        	//return answer;
			return opener + " " + POVtransformedStory;
		});

		// Get story from attribute and then change the POV of it.
		get("/number/:numval/person/:persval/gender/:gendval/:attribute/:value", (request, response) -> {

			String query = request.params(":value");
			String answer = "";
			List<String> answers = new ArrayList<String>();

        	for (int i = 0 ; i < Data.size() ; i++) {
        		JSONObject story = (JSONObject) Data.get(i);
        		String attval = request.params(":attribute");
        		String att = attval.substring(0, 1).toUpperCase() + attval.substring(1);
        		String attribute = (String) story.get(att);
        		if (attribute != null && attribute.equals(query)) {
        			answer = (String) story.get("Text");
        			answers.add(answer);
        		}
        	}
        	Random randomizer = new Random();
        	answer = answers.get(randomizer.nextInt(answers.size()));
        	
        	// Changing POV of the Story
            PointOfView targetPov = getPOV(request.params(":numval"),request.params(":persval"),
            		request.params(":gendval"));
            List<String> sentences = new ArrayList<String>();
            sentences = loadStory(answer);
            List<Annotation> storyAnnotations = sentences.stream().map(s -> new Annotation(s)).collect(Collectors.toList());
            pipeline.annotate(storyAnnotations);
            String POVtransformedStory = "";
            
            //Changing the POV of the story
            for (Annotation story : storyAnnotations) {
                String transformed = transformer.transform(story, targetPov);
                POVtransformedStory = POVtransformedStory + transformed;
            }
            //TODO: Change/add protagonist of the story, change secondary characters/places/organizations?
            // talk to justin.
			return POVtransformedStory;
		});
		
		
        get("/hello", (req, res) -> "Hello World");
	     // matches "GET /hello/foo" and "GET /hello/bar"
        
    }
    
    private static PointOfView getPOV(String Number, String Person, String Gender) {
    	PointOfView targetPov = new PointOfView(PersonalPronouns.Number.valueOf(Number.toUpperCase()),
    			PersonalPronouns.Person.valueOf(Person.toUpperCase()),
    			PersonalPronouns.Gender.valueOf(Gender.toUpperCase()));
        return targetPov;
    }
    
    private static StanfordCoreNLP loadPipeline() {
        Properties props = new Properties();
        props.setProperty("annotators", "tokenize, ssplit, pos, lemma, ner, parse, depparse, dcoref");
        props.setProperty("coref.algorithm", "neural");
        
        StanfordCoreNLP pipeline = new StanfordCoreNLP(props);        
        return pipeline;
    }

    private static List<String> loadStory(String text) {
        List<String> stories = new ArrayList<>();
        
        StringBuilder sb = new StringBuilder();
        for (String line : text.split("\\r?\\n")) {
            if (line.matches("^\\s*$") && sb.length() > 0) {
                stories.add(sb.toString());
                sb = new StringBuilder();
            } else {
                sb.append(new String(line)).append('\n');
            }
        }
        
        if (sb.length() > 0) {
            stories.add(sb.toString());
        }
        
        return stories;
    }
    
    private static String[] loadNewCharacters(File file) throws FileNotFoundException, IOException {
    	String[] newCharacters = null;
    	// example path = "../storyswapping-lm/data/opening.lines.txt"
    	String text = IOUtils.toString(file);
        newCharacters = text.split(",");
    	
        return newCharacters;
    }
    
    private static String loadOpener(String type, String[] characters) throws FileNotFoundException, IOException {
    	String opener = "";
    	File openers = new File("../storyswapping-webui/data/opening.lines.txt");
    	String text = IOUtils.toString(openers);
    	
    	Map<String, List<String>> typeOpening = new HashMap<>();
    	
        for (String line : text.split("\\r?\\n")) {
        	//System.out.println(line);
        	String key = line.split("---")[0];
        	String value = line.split("---")[1];
        	if (typeOpening.get(key) != null) {
        		List<String> listOfOpeners = typeOpening.get(key);
        		listOfOpeners.add(value);
        		typeOpening.put(key, listOfOpeners);
        		
        	} else {
        		List<String> listOfOpeners = new ArrayList<String>();; 
        		listOfOpeners.add(value);
        		typeOpening.put(key, listOfOpeners);
        	}
        }
        // Introduces different openings
    	Random randomizer = new Random();
        int index = randomizer.nextInt(typeOpening.get(type).size());
        System.out.println(index);
        opener = typeOpening.get(type).get(index);
        opener = opener.replaceAll("<P>", characters[0]);
        return opener;
    }

}
