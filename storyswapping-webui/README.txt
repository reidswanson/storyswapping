Date: 8/16/2017
Author: Ignacio Queralt

Storyswapping-webui

#########################################################################################

- data/
	This folder contains the database files of the stories.
	
- lib/
	This folder is created by maven to keep the library files.
	
- resources/
	This contains the client.py script which communicates between the REST POV
	server and the attribute extractor server. This is meant to use with Sarah's
	Fillwock work.

	The client.py file inside this directory is the file used to try the project.
	To use this client, first you need too run the Attribute Server, inside Sarah's
	work. Then, you need to keep running the Server.java class (detailed down
	this file). Once you have these two servers running, you can lunch the
	client and try utterances. Example utterance: i went on vacation to argentina.

- src/
	This folder contains the source code of the webui part of the project.

- pom.xml: is the file for the maven configuration. Contains dependencies and 
	information like that.

There are basically three classes in this project:

- Server.java:
	This is the code for the Server class. This code loads the DB of stories, which is 
	stored as JSON file. Then, it receives the queries of the server using a Spark 
	library.
	There are some files used by this server, you can read more about them in the README
	inside the data/ directory.

- CSVReader.java:
	This is a file to parse stories from a CSV file (a comma separated value file). 
	Basically, this class reads a file and saves a JSON file with the stories 
	with a location, date and organization tag, if exists. The class Server then
	uses this file.

- AttributeExtractor.java
	This is a class used to extract attributes from stories. It's an auxiliar 
	file.