# Story Swapping
## Overview
This repository contains several related projects for retelling stories that could be useful in the context of a human-computer dialogue. The act of story swapping is envisioned as a three step process that begins with an existing story from some corpus. It doesn't matter where the existing story comes from, but for practical reasons it should be relatively short (e.g., under 500 words). For example, it could be authored explicitly for this purpose or drawn from a from [my thesis work](http://reidswanson.com/?page_id=761#resources), [a movie summary](http://www.cs.cmu.edu/~ark/personas/), or some other source. A novel retelling of this story is constructed as a three step process.

1. Adapt the point of view (POV) of the story.
2. Extract a schema from the adapted story.
3. Generate a novel story based on the schema that was extracted.

Currently, there is not an end to end system that implements all three of these components. Notably (2) is not implemented, however, a baseline approach would be easy to add. The functionality for components (1) and (3) are split between three projects.

* storyswapping-core
* storyswapping-cli
* storysapping-lm

It also has a dependency on the [dl4j-utils](https://bitbucket.org/reidswanson/dl4j-utils) project which is also publicly available on bitbucket. I don't think any of the relevant code uses this anymore, but may be required to compile the project.

This repository also contains a webui version (REST server) of the first part of the project (the POV adapter). There are instructions on how to run this server at the end of the page.

## Quickstart
### Project Dependencies
The dependencies for the Java projects should primarily be resolved using Maven. The one exception is the requirement on [dl4j-utils](https://bitbucket.org/reidswanson/dl4j-utils), which is a project that encapsulates some common utilities useful for working with [Deeplearning4j](https://github.com/Theano/Theano).
    
The python project ([storyswapping-lm](#markdown-header-storyswapping-lm)) requires [Keras](https://github.com/fchollet/keras) with the [Theano](https://github.com/Theano/Theano) backend (TensorFlow might also work but hasn't been tried).

### Detailed Installation Instructions
There are several ways to work with these projects. If you are familiar with Maven, then all you have to do is download (or clone) this repository and the [dl4j-utils](https://bitbucket.org/reidswanson/dl4j-utils) repository and build the projects in the following order: dl4j-utils, storyswapping-core, storyswapping-cli.

If you are not familiar with Maven and just want to run the command line tools, then follow the instructions below.

1. Download or clone the [dl4j-utils](https://bitbucket.org/reidswanson/dl4j-utils) project.
2. Download or clone this repository.
3. Navigate to the dl4j-utils base project directory (where the pom.xml file is) and run `mvn install` or `mvn install -DskipTests=true` (to avoid running any test programs, which are unlikely to be useful).
4. Navigate to the base directory of the storyswapping-core project and run `mvn install` or `mvn install -DskipTests=true`.
5. Navigate to the base directory of the storyswapping-cli project and run `mvn -P shade install` or `mvn -P shade install -DskipTests=true`. The option -P shade will create a jar containing all the dependencies so that you don't need to add each dependency separately (e.g., using the `copy-libs` profile to copy all the individual dependencies to the target/libs directory). The compiled/shaded jar will be in your maven repository (on unix like systems in `.m2/repository/edu/usc/ict/nld`.
6. You can now use this jar as the only argument to the java `-cp` argument to run any of the programs in the storyswapping-cil project.
7. Before using the python scripts, make sure you have installed both bidict and depq python libraries.
     
### Step 1: Point Of View Transformation
The first step in the retelling process can be performed using the command line program [TransformStory](storyswapping-cli/src/main/java/edu/usc/ict/nld/storyswapping/cli/TransformStory.java) in the [storyswapping-cli](project). To see the full list of options available, this program can be run with the following command (assuming the appropriate classpath is set):
    
    >java edu.usc.ict.nld.storyswapping.cli.TransformStory -h 

The primary usage of the program is controlled by 4 command line options:

* *-f*, *--input-text* specifies a file containing a list of stories each separated by a blank line.
* *-g*, *--target-gender* specifies the target gender for the output story (MASCULINE, FEMININE, NEUTER).
* *-n*, *--target-number* specifies the target number for the output story (SINGULAR, PLURAL).
* *-p*, *--target-person* specifies the target person for the output story (FIRST, SECOND, THIRD).

The program will process the input stories using the Stanford CoreNLP toolkit and apply the transformation rules to the annotated documents. Each transformed story will be written to stdout.

Given the input text file [mr-eberdeens-house.txt](storyswapping-lm/examples/mr-eberdeens-house.txt) (found in storyswapping-lm/examples and taken from Project Gutenberg) we can transform this story from 3rd person to 1st person with the following command.

    > java edu.usc.ict.nld.storyswapping.cli.TransformStory \
        --input-text mr-eberdeens-house.txt \
        --target-gender DEICTIC \
        --target-number SINGULAR \
        --target-person FIRST \
        > mr-eberdeens-house.1st.txt
**Note:** This example shows some existing problems with the verb conjugation heuristics.

Since it is time consuming to parse documents with the CoreNLP toolkit the program also has some options for preprocessing a set of documents and serializing them using the ProtobufAnnotationSerializer, which can also be read by the program. To preprocess a set of documents use the *--input-text* option as normal, but specify an output path with the *--output-annotations* option to write the processed documents instead of transforming them. To use these preprocessed documents use the option *--input-annotations* instead of the *--input-text* option when reading the stories for transformation.

### Step 2: Schema Extraction
This is currently not implemented. A simple baseline approach would be to keep the subject, object and primary verb of each dependency parse.

### Step 3: Story Regeneration
The regeneration of a story is done with [generate.py](storyswapping-lm/storyswapping/generate.py) script in the [storyswapping-lm](#markdown-header-storyswapping-lm) project. It requires a story template, a vocabulary file and a trained neural network file. The template is just a text file with an ordered set of words (tagged with their part of speech). A simple example [example-template.txt](storyswapping-lm/examples/example-template.txt) is provided in the [examples](storyswapping-lm/examples) folder. The vocabulary can be built with the [BuildVocab](#markdown-header-buildvocab) program (described below), but a sample vocabulary is provided in the [examples](storyswapping-lm/examples) folder. A neural network model can be trained with the [train.py](#markdown-header-trainpy) script (described below) but a sample model is also provided in the [examples](storyswapping-lm/examples) folder.

To generate a story from the template run the following command.

    > python generate.py \
        --model-file narrative-prediction.na.1h.v10000.m100.M1000.coherent.l2.e600.h1024.t50.keras.nn.pkl \
        --vocab-file narrative-archive.v10000.vocab.txt.gz \
        --num-layers 2 \
        --embedding-dim 600 \
        --hidden-units 1024 \
        --sequence-length 50 \
        example-template.txt

* *--model-file* specifies the path to the file containing the serialized neural network parameters. The name encodes the parameters used to train it. *1h* indicates the vocabulary was encoded using a 1-hot representation. *v10000* indicates that the vocabulary included the top 10,000 most common words (+ POS tags). *m100* and *M1000* indicate that the minimum and maximum story length used to train the model was between 100 and 1000 words. *coherent* indicates that a set of heuristics were used to filter out stories that were likely to be low quality. *l2* indicates that the model contains 2 hidden LSTM layers. *e600* indicates that the embedding size for the lookup table was 600 units. *h1024* indicates that each LSTM layer contained 1024 units. *t50* indicates the sequence length used to train the model.
* *--vocab-file* specifies the file containing the valid vocabulary words.
* *--num-layers*, *--embedding-dim*, *--hidden-units* and *--sequence-length* specify the topology of the network. These must be the same values as when the model was trained.

Given the example template:
>he\_prp ran\_vbd out\_in of\_in milk\_nn .\_. he\_prp store\_nn .\_. he\_prp home\_nn .\_.

The first few lines of the output from the command are:

    -46.164: he ran out of milk . he was at the store . he was home .
    -47.413: he ran out of milk . he was at the store . he got home .
    -46.625: he ran out of milk . he was at the store . he went home .
    -46.753: he ran out of milk . he was at the store . he came home .
    -47.800: he ran out of milk . he was at the store . he was n't home .
    -48.219: he ran out of milk . he was at the store . he is home .
    -49.391: he ran out of milk . he was at the store . he walked home .

Each line is a completed document with its log probability reported as the first token.

The full usage of the program is provided below.

    usage: generate.py [-h] [--model-file MODEL_FILE] --vocab-file VOCAB_FILE
                   [--sequence-length SEQUENCE_LENGTH]
                   [--num-layers NUM_LAYERS] [--embedding-dim EMBEDDING_DIM]
                   [--hidden-units HIDDEN_UNITS] [--heuristic HEURISTIC]
                   [--n-best N_BEST] [--branching BRANCHING]
                   [--beam-size BEAM_SIZE]
                   [template]
    positional arguments:
        template        An ordered list of tokens (pos tagged words separated
                        by an '_') that must be included in the final
                        generated story
    optional arguments:
        -h, --help            show this help message and exit
        --model-file MODEL_FILE
                        The trained model
        --vocab-file VOCAB_FILE
                        The vocabulary file
        --sequence-length SEQUENCE_LENGTH
                        The sequence length used to train the model
        --num-layers NUM_LAYERS
                        The number of hidden LSTM layers
        --embedding-dim EMBEDDING_DIM
                        The embedding dimension
        --hidden-units HIDDEN_UNITS
                        The number of hidden units in an LSTM layer
        --heuristic HEURISTIC
                        The weight used in the A* heuristic. Smaller (more
                        negative) values will speed up search but result in
                        less optimal solutions.
        --n-best N_BEST       The number of solutions to return (each on a separate
                        line).
        --branching BRANCHING
                        At each expansion only consider this many words to add
                        to the search queue.
        --beam-size BEAM_SIZE
                        The maximum size for the search queue.



## Training A Language Model
Training a new language model that can be used with [generate.py](#markdown-header-step-3-story-regeneration) is relatively easy but requires a few steps.

*  [Build a vocabulary](#markdown-header-buildvocab) : This creates a mapping from words as textual strings to integer values that will be used in the 1-hot encoding.
*  [Prepare a corpus](#markdown-header-writenarrativepredictiondataset): This transforms a text corpus into a format where all words are mapped to their integer values. This has two purposes. 1) It greatly reduces the size of the original corpus. 2) It is much faster to read when training the model.
*  [Train the model](#markdown-header-trainpy): This trains a neural network model from the prepared corpus.

## Project Details
### storyswapping-core
**storyswapping-core** is a Java project that implements the core functionality for the point of view transformations. 
To transform a story you simply call the *transform* method of the [PovTransformer](storyswapping-core/src/main/java/edu/usc/ict/nld/storyswap/PovTransformer.java) class. This method expects a document that is processed by the Stanford CoreNLP toolkit and a target [PointOfView](storyswapping-core/src/main/java/edu/usc/ict/nld/storyswap/PointOfView.java).  It will return a new document (as a string) that has replaced all the appropriate pronouns with the one specified by the target POV.

The algorithm is a basic rule-based approach. It first tries to identify the source POV of the story using a small set of heuristics (e.g., what is the most frequent pronoun in the longest coreference chain). It then tries to identify all of the instances where the protagonist is mentioned and change that mention to the appropriate target POV. This may have consequences on the verb conjugation, for example changing a pronoun from singular to plural. The target verb is looked up in [a large inflectional dictionary](http://unitexgramlab.org/language-resources) and the appropriate conjugation is selected using the appropriate attributes (e.g., tense, number, etc.) from the target POV.

**Note the other packages in this project contain a lot of dead code.**

### storyswapping-cli
**storyswapping-cli** is a Java project that contains several utility programs and a command line interface for applying the transformer to text documents. There are several programs in this project that might be useful for narrative generation. 

#### BuildVocab
[BuildVocab](storyswapping-cli/src/main/java/edu/usc/ict/nld/storyswapping/cli/BuildVocab.java) is a program that reads sentences from a database and creates a vocabulary from it. In addition to the words of the corpus all vocabularies built with this tool will include a special unknown word token (<UNK> by default) and tokens representing each part of speech in the Penn Treebank tagset. It currently only supports reading from the ICT Narrative Lab's corpus of personal stories through the [TaggedDocumentIterator](storyswapping-core/src/main/java/edu/usc/ict/nld/storyswap/nn/TaggedDocumentIterator.java) class, which reads sentences from a MySql database, but it would be easy to modify reading from other sources. The connection to a database is handled through a Java properties file and loaded via the [DatabaseConnectionProperties](storyswapping-core/src/main/java/edu/usc/ict/nld/storyswap/io/DatabaseConnectionProperties.java) class. The actual query for sentences is hard coded in the TaggedDocumentIterator class, but would be easy to adapt if the data is in a similar format. Currently, it is assumed that there are two columns of interest: *story\_id* and *sentence\_pos*. The first identifies which story the sentence belongs to. The second contains the textual data, which is assumed to be a single sentence that has already been tokenized and tagged with its part of speech. Each token is assumed to be of the form: word\_pos. For other types of input data implementing a new Iterator might be necessary.

The output of the process is a file with the following format:

    N
    <UNK> ID_unk 1
    <POS1> ID_POS1 1
    <POS2> ID_POS2 1
    ...
    WORD1 ID_WORD1 COUNT_WORD1
    WORD2 ID_WORD2 COUNT_WORD2
    ...
    WORDM ID_WORDM COUNT_WORDM
   
*N* is the total number of tokens in the corpus. Each additional line has 3 columns. The first column is the string representation of the token. The second is a unique numeric identifier for the token. The third column is the frequency in the corpus.
*<UNK>* is a special unknown word token that can be configured via the command line. *<POS>* represents a part of speech tag in the Penn tagset. 
The <UNK> token and each part of speech is assumed to have only been seen once in the corpus. **Note:** this may be one reason my custom word2vec models do not perform well.

The program has 2 required options and 2 additional optional parameters.

* *-u*, *--unk* (optional) specifies the unknown word token to use.
* *-n*, *--top-n* (optional) only report the top n most frequent words in the corpus.
* *-c*, *--connection-properties* (required) the path to the database connection properties used to connect to the MySql database.
* *-V*, *--vocab-file* (required) the path where the vocabulary will be saved (as a gzipped text file).

#### WriteNarrativePredictionDataSet
[WriteNarrativePredictionDataSet](storyswapping-cli/src/main/java/edu/usc/ict/nld/storyswapping/cli/WriteNarrativePredictionDataSet.java) is a program that transforms a textual corpus into a binary format that can be read quickly by the training script. This program is only designed to read from ICT Narrative Lab's corpus of personal stories. For instructions on its usage run the command:

    >java edu.usc.ict.nld.storyswapping.cli.WriteNarrativePredictionDataSet -h

The output format is simple and could easily be replicated by another script. It has the following structure:

    V N1 w1 w2 ... wN1 N2 w1 w2 ... wN2 ... 0

Each token is a 32 bit integer. *V* is the total vocabulary size. *N1* is the number of tokens in the first document. *w1* to *wN1* are the indexes of each word in the document according to the vocabulary used to create the file. There should be exactly *N1* tokens. *N2* is the number of tokens in the second document with its corresponding words and so on. When there are no more documents a 0 should be written as the last value in the document.

### storyswapping-lm
**storyswapping-lm** is a python project that contains a number of scripts for training and applying an LSTM language model for generating stories from a template.

#### train.py
[train.py](storyswapping-lm/storyswapping/train.py) is a python script for training an LSTM language model for use with [generate.py](#markdown-header-step-3-story-regeneration). Given a training file that has the format given [above](#markdown-header-writenarrativepredictiondataset) you can train a model using the following command:

    python train.py \
        --training-file training-file.gz \
        --validation-file validation-file.gz \
        --vocab-file vocab.txt.gz \
        --model-file output-model-file.pkl
        --limit 1000000 \
        --num-layers 2 \
        --embedding-dim 300 \
        --hidden-units 500 \
        --batch-size 100 \
        --max-epochs 1000 \
        --sequence-length 20
        
* The training and validation files should be binary files in the proper format described above. 
* The vocab file should be a text file in the format that [BuildVocab](#markdown-header-buildvocab) produces. 
* The model file is where the trained model parameters will be written to. 
* *--limit* will reset the input document iterator to the beginning after reading this many documents.
* *--num-layers* specifies how many LSTM layers the model will have.
* *--embedding-dim* specifies how many hidden units in the Embedding layer.
* *--hidden-units* specifies how many hidden units in each LSTM layer.
* *--batch-size* is the number of sequences to process in a mini-batch.
* *--max-epochs* is the maximum number of epochs (number of times to loop through the input data iterator) before terminating the training.
* *--sequence-length* specifies how long (i.e., number of tokens) in each training sequence.

The full usage of the command can be output by running the script with the *-h* option.


### storyswapping-webui
**storyswapping-webui** is a java project that uses the **storyswapping-core** project combined with a REST server, to change the POV of stories in a really short period of time. There are different ways of running the project.

Steps to run the server:

1. Follow the "Detailed Installation Instructions" at the beginning of this page.
2. Run the Server.java class as a Java Application.
3. Hit the server with a particular url.

The protocol used so far is HTTP, so to hit the server you must use an url with the following format:

http://localhost:4567/<AttributeType>/<AttributeValue>

The possible <AttributeTypes> are location, date and organization. Some examples for the attributes values are: Argentina, New York, California, Canada (for location). 1990, yesterday, last month (for date). Disney, FBI, CIA (for organization). Example URL: 

http://localhost:4567/location/Argentina

The server changes the POV of the story to the third person, masculine and singular by default. You can also change this configuration using a different URL, like the following one:

http://localhost:4567/number/<targetNumber>/person/<targetPerson>/gender/<targetGender>/<AttributeType>/<AttributeValue>

The fields are the same used in the other parts of this project. One example URL for this mode is the following one:

http://localhost:4567/number/SINGULAR/person/THIRD/gender/MASCULINE/location/Colombia

In addition to that, this server can be used with the work of Sarah Fillwock, to obtain stories with a particular attribute from utterances. In Sarah's work, there are instructions on how to run his server, in particular, with the AttributeSystemServer.py script. Once that script is running, you can run the client.py script provided in the storiswapping-webui/resources directory.
You can read the documentation files inside storyswapping-webui for more information.

## License
All files in these projects (with the exception noted below) are copyrighted by the University of Southern California licensed using the GPLv3 (see [License](LICENSE)). The file [dela-en-public-u8.dic.xml](storyswapping-core/src/main/resources/edu/usc/ict/nld/storyswap/dela/dela-en-public-u8.dic.xml) was copied (and slightly modified) from the lexical resources released as part of the [Unitex](http://unitexgramlab.org/) software package and originally licensed under the [LGPLLR](http://unitexgramlab.org/lgpllr).
