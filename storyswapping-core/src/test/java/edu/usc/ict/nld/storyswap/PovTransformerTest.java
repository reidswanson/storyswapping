/**
 * 
 */
package edu.usc.ict.nld.storyswap;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.ProtobufAnnotationSerializer;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.Pair;

/**
 * @author reid
 *
 */
public class PovTransformerTest {
    //-----------------------------------------------------------------------//
    //-- Nested Classes -----------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Static Fields ------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Managed Properties -------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Properties ---------------------------------------------------------//
    //-----------------------------------------------------------------------//
    //-- Stative ------------------------------------------------------------//

    //-- Functional ---------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Initialization -----------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Getters & Setters --------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Methods ------------------------------------------------------------//
    //-----------------------------------------------------------------------//
    //@Test
    public void testAnnotate() throws IOException {
        ProtobufAnnotationSerializer serializer = new ProtobufAnnotationSerializer();
        
        Properties props = new Properties();
        props.setProperty("annotators", "tokenize, ssplit, pos, lemma, ner, parse, depparse, dcoref");
        props.setProperty("coref.algorithm", "neural");
        
        StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
        
        Annotation a = null;
        try (
            InputStream is = this.getClass().getResourceAsStream("example-story.txt");
            BufferedInputStream bis = new BufferedInputStream(is);
        ) {
            String text = IOUtils.toString(bis);
            a = pipeline.process(text);
        }
        
        try (
            FileOutputStream fos = new FileOutputStream(new File("example-story.ser.gz"));
            GZIPOutputStream cos = new GZIPOutputStream(fos);
        ) {
            serializer.write(a, cos);
        }
    }
    
    @Test
    public void testPovTransformer() throws IOException, ClassNotFoundException, ClassCastException {
        Annotation a1 = load();
        Annotation a2 = load();
        Annotation a3 = load();
        
        PovTransformer transformer = new PovTransformer();
        
        PointOfView target3rdMascPov = new PointOfView(PersonalPronouns.Number.SINGULAR, PersonalPronouns.Person.THIRD, PersonalPronouns.Gender.MASCULINE);
        PointOfView target3rdFemPov = new PointOfView(PersonalPronouns.Number.SINGULAR, PersonalPronouns.Person.THIRD, PersonalPronouns.Gender.FEMININE);
        PointOfView target2ndPov = new PointOfView(PersonalPronouns.Number.SINGULAR, PersonalPronouns.Person.SECOND, PersonalPronouns.Gender.DEICTIC);
        
        String original = toString(a1);
        String thirdMasc = transformer.transform(a1, target3rdMascPov);
        String thirdFeminine = transformer.transform(a2, target3rdFemPov);
        String second = transformer.transform(a3, target2ndPov);
        
        System.err.printf("Original:%n%s%n", original);
        System.err.printf("3rd Person Masculine:%n%s%n", thirdMasc);
        System.err.printf("3rd Person Feminine:%n%s%n", thirdFeminine);
        System.err.printf("2nd Person:%n%s%n", second);
    }
    
    public static String toString(Annotation a) {
        StringBuilder sb = new StringBuilder();
        for (CoreMap sentence : a.get(SentencesAnnotation.class)) {
            sb.append(sentence.get(TextAnnotation.class)).append(System.lineSeparator());
        }
        
        return sb.toString();
    }

    //-----------------------------------------------------------------------//
    //-- Actions ------------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Utility Methods ----------------------------------------------------//
    //-----------------------------------------------------------------------//
    private Annotation load() throws IOException, ClassNotFoundException, ClassCastException {
        ProtobufAnnotationSerializer serializer = new ProtobufAnnotationSerializer();
        
        try (
            InputStream is = this.getClass().getResourceAsStream("example-story.ser.gz");
            GZIPInputStream cos = new GZIPInputStream(is);
        ) {
            Pair<Annotation, InputStream> pair = serializer.read(cos);
            return pair.first;
        }
    }
}
