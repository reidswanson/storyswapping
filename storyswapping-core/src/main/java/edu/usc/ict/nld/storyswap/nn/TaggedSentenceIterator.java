/**
 * 
 */
package edu.usc.ict.nld.storyswap.nn;

import java.util.List;

import org.deeplearning4j.text.sentenceiterator.BaseSentenceIterator;

/**
 * @author reid
 *
 */
public class TaggedSentenceIterator extends BaseSentenceIterator {
    //-----------------------------------------------------------------------//
    //-- Nested Classes -----------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Static Fields ------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Managed Properties -------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Properties ---------------------------------------------------------//
    //-----------------------------------------------------------------------//
    //-- Stative ------------------------------------------------------------//
    private TaggedDocumentIterator docItr;
    private List<String> currentDocument;
    private int currentSentence;

    //-- Functional ---------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Initialization -----------------------------------------------------//
    //-----------------------------------------------------------------------//
    /**
     * 
     * @param docItr
     */
    public TaggedSentenceIterator(TaggedDocumentIterator docItr) {
        this.docItr = docItr;
        
        reset();
    }

    //-----------------------------------------------------------------------//
    //-- Getters & Setters --------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Methods ------------------------------------------------------------//
    //-----------------------------------------------------------------------//
    /* (non-Javadoc)
     * @see org.deeplearning4j.text.sentenceiterator.SentenceIterator#nextSentence()
     */
    @Override
    public String nextSentence() {
        String result = currentDocument.get(currentSentence++);
        
        if (currentSentence >= currentDocument.size() && docItr.hasNext()) {
            currentDocument = docItr.next();
            currentSentence = 0;
        }
        
        return result;
    }

    /* (non-Javadoc)
     * @see org.deeplearning4j.text.sentenceiterator.SentenceIterator#hasNext()
     */
    @Override
    public boolean hasNext() {
        if (currentSentence < currentDocument.size()) { return true; }
        
        return docItr.hasNext();
    }

    /* (non-Javadoc)
     * @see org.deeplearning4j.text.sentenceiterator.SentenceIterator#reset()
     */
    @Override
    public void reset() {
        docItr.reset();
        
        currentSentence = 0;
        currentDocument = docItr.next();
    }

    //-----------------------------------------------------------------------//
    //-- Actions ------------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Utility Methods ----------------------------------------------------//
    //-----------------------------------------------------------------------//

}
