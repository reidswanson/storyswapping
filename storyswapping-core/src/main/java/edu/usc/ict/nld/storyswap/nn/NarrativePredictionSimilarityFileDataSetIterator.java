/**
 * 
 */
package edu.usc.ict.nld.storyswap.nn;

import static org.nd4j.linalg.indexing.NDArrayIndex.all;
import static org.nd4j.linalg.indexing.NDArrayIndex.point;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.GZIPInputStream;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.DataSetPreProcessor;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.INDArrayIndex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author rswanson
 *
 */
public class NarrativePredictionSimilarityFileDataSetIterator extends NarrativeIterator {
    private static final long serialVersionUID = 1L;
    static final Logger LOG = LoggerFactory.getLogger(NarrativePredictionSimilarityFileDataSetIterator.class);

    //-----------------------------------------------------------------------//
    //-- Nested Classes -----------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Static Fields ------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Managed Properties -------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Properties ---------------------------------------------------------//
    //-----------------------------------------------------------------------//
    //-- Stative ------------------------------------------------------------//
    private File file;
    private FileInputStream fis;
    private GZIPInputStream cis;
    private DataInputStream dis;
    private int sequenceLength;
    private int dimensions;
    private int sentinelCount;
    private Deque<INDArray> document;
    private Deque<INDArray> sequence;
    private INDArray documentStart;
    private INDArray documentEnd;

    //-- Functional ---------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Initialization -----------------------------------------------------//
    //-----------------------------------------------------------------------//

    
    /**
     * 
     */
    public NarrativePredictionSimilarityFileDataSetIterator(File file, int sequenceLength, int defaultBatchSize) {
        this.file = file;
        this.sequenceLength = sequenceLength;
        this.defaultBatchSize = defaultBatchSize;
        this.sequence = new ArrayDeque<>();
        this.document = new ArrayDeque<>();
        this.documentStart = null;
        this.documentEnd = null;
        
        reset();
    }

    //-----------------------------------------------------------------------//
    //-- Getters & Setters --------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Methods ------------------------------------------------------------//
    //-----------------------------------------------------------------------//
    /* (non-Javadoc)
     * @see java.util.Iterator#hasNext()
     */
    @Override
    public boolean hasNext() {
        return sentinelCount < sequenceLength;
    }

    /* (non-Javadoc)
     * @see java.util.Iterator#next()
     */
    @Override
    public DataSet next() {
        return next(defaultBatchSize);
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#next(int)
     */
    @Override
    public DataSet next(int num) {
        INDArray features = Nd4j.zeros(num, dimensions, sequenceLength);
        INDArray labels = Nd4j.zeros(num, dimensions, sequenceLength);
        
        AtomicInteger i = new AtomicInteger(0);
        AtomicInteger k = new AtomicInteger(0);
        synchronized (sequence) {
            for (; i.get() < num; i.incrementAndGet()) {
                k.set(0);
                sequence.stream()
                    .limit(sequenceLength)
                    .forEach(a -> features.put(new INDArrayIndex[] { point(i.get()),  all(), point(k.getAndIncrement()) }, a));
                
                k.set(0);
                sequence.stream()
                    .skip(1)
                    .forEach(a -> labels.put(new INDArrayIndex[] { point(i.get()),  all(), point(k.getAndIncrement()) }, a));
                
                
                sequence.pop();
                try {
                    if (!addToSequence()) { 
                        sequence.add(documentEnd);
                        ++sentinelCount;
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        return new DataSet(features, labels);
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#totalExamples()
     */
    @Override
    public int totalExamples() {
        throw new UnsupportedOperationException();
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#inputColumns()
     */
    @Override
    public int inputColumns() {
        return dimensions;
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#totalOutcomes()
     */
    @Override
    public int totalOutcomes() {
        return dimensions;
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#resetSupported()
     */
    @Override
    public boolean resetSupported() {
        return true;
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#asyncSupported()
     */
    @Override
    public boolean asyncSupported() {
        return true;
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#reset()
     */
    @Override
    public void reset() {
        // I'm being very conservative on the synchronization for now
        synchronized (sequence) {
            sentinelCount = 0;
            document.clear();
            sequence.clear();
            
            close();
            
            try {
                fis = new FileInputStream(file);
                cis = new GZIPInputStream(fis, 1<<18);
                dis = new DataInputStream(cis);
                
                dimensions = dis.readInt();
                
                if (documentStart == null) {
                    documentStart = Nd4j.zeros(dimensions);
                    documentEnd = Nd4j.zeros(dimensions);
                    documentStart.putScalar(2, 1.0);
                    documentEnd.putScalar(3, 1.0);
                }
                
                readDocument();
                fillSequence();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
    
    private void readDocument() throws IOException {
        if (sentinelCount > 0) { return; }
        
        int n = dis.readInt();
        
        for (int i = 0; i < n; ++i) {
            document.addLast(readArray(dis));
        }
    }

    private INDArray readArray(DataInputStream dis) throws IOException {
        INDArray array = Nd4j.zeros(dimensions);
        
        for (int i = 0; i < dimensions; ++i) {
            array.putScalar(i, dis.readFloat());
        }
        
        return array;
    }
    
    private void fillSequence() throws IOException {
        while (sequence.size() < sequenceLength + 1) {
            if (!addToSequence()) { break; }
        }
    }
    
    private boolean addToSequence() throws IOException {
        if (document.isEmpty()) { readDocument(); }
        if (document.isEmpty()) { return false; }
        
        sequence.add(document.pop());
        
        return true;
    }
    
    @Override
    public void close() {
        if (fis != null) {
            try {
                fis.close();
            } catch (IOException e) {
                // ignore
            } finally {
                fis = null;
            }
        }
        
        if (cis != null) {
            try {
                cis.close();
            } catch (IOException e) {
                // ignore
            } finally {
                cis = null;
            }
        }
        
        if (dis != null) {
            try {
                dis.close();
            } catch (IOException e) {
                // ignore
            } finally {
                dis = null;
            }
        }
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#batch()
     */
    @Override
    public int batch() {
        throw new UnsupportedOperationException();
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#cursor()
     */
    @Override
    public int cursor() {
        throw new UnsupportedOperationException();
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#numExamples()
     */
    @Override
    public int numExamples() {
        throw new UnsupportedOperationException();
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#setPreProcessor(org.nd4j.linalg.dataset.api.DataSetPreProcessor)
     */
    @Override
    public void setPreProcessor(DataSetPreProcessor preProcessor) {
        throw new UnsupportedOperationException();
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#getPreProcessor()
     */
    @Override
    public DataSetPreProcessor getPreProcessor() {
        throw new UnsupportedOperationException();
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#getLabels()
     */
    @Override
    public List<String> getLabels() {
        throw new UnsupportedOperationException();
    }

    //-----------------------------------------------------------------------//
    //-- Actions ------------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Utility Methods ----------------------------------------------------//
    //-----------------------------------------------------------------------//
}
