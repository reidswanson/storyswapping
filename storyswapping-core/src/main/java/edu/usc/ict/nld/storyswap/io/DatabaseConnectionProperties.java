/**
 * 
 */
package edu.usc.ict.nld.storyswap.io;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * @author reid
 *
 */
public class DatabaseConnectionProperties {
    private Properties properties;
    
    public enum Key {
        HOST("host", "localhost"),
        PORT("port", "3306"),
        USERNAME("username", ""),
        PASSWORD("password", ""),
        DATABASE("database", "narrative_archive"),
        TABLE("table", "sentence"),
        LIMIT("limit", "0")
        ;
        
        private final String label;
        private final String defaultValue;
        
        private Key(String label, String defaultValue) {
            this.label = label;
            this.defaultValue = defaultValue;
        }
        
        public String label() {
            return label;
        }
    }
    
    /**
     * 
     */
    public DatabaseConnectionProperties() {
        this.properties = new Properties();
    }
    
    /**
     * 
     * @param file
     * @return
     */
    public static DatabaseConnectionProperties load(File file) {
        DatabaseConnectionProperties dcp = new DatabaseConnectionProperties();
        
        try (
            FileInputStream fis = new FileInputStream(file);
            BufferedInputStream bis = new BufferedInputStream(fis);
        ) {
            dcp.properties.load(bis);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        
        return dcp;
    }
    
    /**
     * 
     * @param key
     * @return
     */
    public String value(Key key) {
        return properties.getProperty(key.label, key.defaultValue);
    }
    
    /**
     * 
     * @return
     */
    public Properties getProperties() {
        return this.properties;
    }
}
