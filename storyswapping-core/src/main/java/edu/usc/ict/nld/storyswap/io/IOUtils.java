/**
 * 
 */
package edu.usc.ict.nld.storyswap.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Serializable;
import java.io.UncheckedIOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Properties;
import java.util.stream.Stream;

import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.compress.compressors.CompressorInputStream;
import org.apache.commons.compress.compressors.CompressorOutputStream;
import org.apache.commons.compress.compressors.CompressorStreamFactory;

/**
 * @author rswanson
 *
 */
public class IOUtils {
	//-----------------------------------------------------------------------//
	//-- Nested Classes -----------------------------------------------------//
	//-----------------------------------------------------------------------//
    public static class DummyOutputStream extends OutputStream {
        @Override
        public void write(int b) throws IOException {
            // do nothing
        }
    }
    
    private static class UnclosableOutputStream extends FilterOutputStream {
        public UnclosableOutputStream(OutputStream os) {
            super(os);
        }
        
        /* (non-Javadoc)
         * @see java.io.FilterOutputStream#close()
         */
        @Override
        public void close() throws IOException {
            out.flush();
        }
    }
    
    private static class UnclosableInputStream extends FilterInputStream {
        public UnclosableInputStream(InputStream is) {
            super(is);
        }
        
        /* (non-Javadoc)
         * @see java.io.FilterInputStream#close()
         */
        @Override
        public void close() throws IOException {
            // do nothing
        }
    }

	//-----------------------------------------------------------------------//
	//-- Static Fields ------------------------------------------------------//
	//-----------------------------------------------------------------------//
	private static final int BUFFER_SIZE = 1 << 16;

	//-----------------------------------------------------------------------//
	//-- Static Methods -----------------------------------------------------//
	//-----------------------------------------------------------------------//
	/**
	 * Sets System.err to dummy PrintStream that doesn't write any bytes.
	 * @return a reference to the original System.err so you can reenable it with {@link #enableErr(PrintStream)}
	 */
	public static PrintStream disableErr() {
	    PrintStream original = System.err;
	    
	    System.setErr(new PrintStream(new DummyOutputStream()));
	    
	    return original;
	}
	
	/**
	 * Reenable System.err after disabling it with {@link #disableErr()}
	 * @param original The original reference to System.err
	 */
	public static void enableErr(PrintStream original) {
	    System.setErr(original);
	}
	
	/**
	 * Reads an entire {@link File} into a String that may or may not be compressed (using
	 * one of the formats supported by 
	 * <a href='https://commons.apache.org/proper/commons-compress/examples.html'>Apache Commons Compress</a>).
	 * 
	 * @param file The input file.
	 * @param charset The {@link Charset} to use.
	 * @return The resulting String
	 * @throws FileNotFoundException if the file does not exist.
	 * @throws IOException if there is a problem reading the file.
	 */
	public static String toString(File file, Charset charset) throws FileNotFoundException, IOException {
		try (
			InputStream is = file == null ? new UnclosableInputStream(System.in) : new FileInputStream(file);
		) {
			return toString(is, charset);
		}
	}
	
	/**
	 * Read an entire {@link File} into a String.
	 * @see #toString(File, Charset)
	 * @param file The file to read.
	 * @return The contents of the file as a String
	 * @throws FileNotFoundException If the file cannot be located.
	 * @throws IOException If there is an error reading the file.
	 */
	public static String toString(File file) throws FileNotFoundException, IOException {
		return toString(file, StandardCharsets.UTF_8);
	}
	
	/**
	 * Read an entire {@link InputStream} into a String. 
	 * @see #toString(File, Charset)
	 * @param is The input stream to read.
	 * @param charset The character encoding of the file.
	 * @return The contents of the input stream as a String
	 * @throws IOException If there is a problem reading from the stream.
	 */
	public static String toString(InputStream is, Charset charset) throws IOException {
		try (InputStream bis = new BufferedInputStream(is, BUFFER_SIZE)) {
			bis.mark(BUFFER_SIZE + 1);
			
			try (
				InputStream cis = new CompressorStreamFactory().createCompressorInputStream(bis);
				Reader isr = new InputStreamReader(cis, charset);
				Reader br = new BufferedReader(isr, BUFFER_SIZE);
			) {
				return toString(br);
			} catch (CompressorException ce) {
				bis.reset();
				try (
					Reader isr = new InputStreamReader(bis, charset);
					Reader br = new BufferedReader(isr, BUFFER_SIZE);
				) {
					return toString(br);
				}
			}
		}
	}
	
	/**
	 * Read an entire {@link InputStream} into a String assuming UTF-8.
	 * @see #toString(InputStream)
	 * @param is The input stream to read.
	 * @return The contents of the input stream as a String.
	 * @throws IOException If there is a problem reading from the stream.
	 */
	public static String toString(InputStream is) throws IOException {
		return toString(is, StandardCharsets.UTF_8);
	}
	
	/**
	 * Return a Stream of lines from the given file.
	 * Similar to {@link #toString(File, Charset)} optionally supports files that are compressed using
	 * GZip, BZip2 and XZ from the Apache Commons Compress library.
	 * <strong>Note</strong> you are responsible for closing the resource when finished
	 * (e.g., using a try-with-resources statement).
	 * 
	 * @param file  The file to read.
	 * @param charset The character encoding of the file.
	 * @return The lines of the file as a Stream.
	 * @throws IOException If there is a problem reading the file.
	 */
	public static Stream<String> lines(File file, Charset charset) throws IOException {
		InputStream is = new FileInputStream(file);
		
		return lines(is, charset).onClose(asUnchecked(is));
	}
	
	/**
	 * Return a Stream of lines from the given file assuming it is encoded with UTF-8.
	 * @see edu.usc.ict.storylab.io.IOUtils#lines(File, Charset)
	 * 
	 * @param file The file to read.
	 * @return The lines of the file as a Stream.
	 * @throws IOException If there is a problem reading the file.
	 */
	public static Stream<String> lines(File file) throws IOException {
		return lines(file, StandardCharsets.UTF_8);
	}
	
	/**
	 * Return each line of an InputStream that may optionally be compressed as a {@link Stream}
	 * of Strings. <strong>Note</strong> you are responsible for closing the resources when
	 * finished either by calling close on the Stream or by using a try-with-resources statement.
	 * 
	 * @param is The input stream to read from.
	 * @param charset The character encoding of the input.
	 * @return The lines of the input steam as a Stream.
	 * @throws IOException If there is a problem reading the input stream.
	 */
	public static Stream<String> lines(InputStream is, Charset charset) throws IOException {
		BufferedInputStream bis = new BufferedInputStream(is, BUFFER_SIZE);
		bis.mark(BUFFER_SIZE + 1);
		
		CompressorInputStream cis = null;
		InputStreamReader isr = null;
		BufferedReader br = null;
		
		try {
			cis = new CompressorStreamFactory().createCompressorInputStream(bis);
			isr = new InputStreamReader(cis, charset);
			br	= new BufferedReader(isr, BUFFER_SIZE);
		} catch (CompressorException ce) {
			bis.reset();
			
			isr = new InputStreamReader(bis, charset);
			br	= new BufferedReader(isr, BUFFER_SIZE);
		} catch (Error | RuntimeException e) {
			try {
				if (br != null) { br.close(); }
			} catch (IOException ioe) {
				try {
					e.addSuppressed(ioe);
				} catch (Throwable ignore) { }
			}
			
			throw e;
		}
		
		try {
			return br.lines().onClose(asUnchecked(br, isr, cis, bis));
		} catch (Error | RuntimeException e) {
			try {
				if (br != null) { br.close(); }
			} catch (IOException ioe) {
				try {
					e.addSuppressed(ioe);
				} catch (Throwable ignore) { }
			}
			
			throw e;
		}
	}
	
	/**
	 * Return each line of an InputStream assuming it is encoded as UTF-8.
	 * @see #lines(InputStream, Charset)
	 * 
	 * @param is The input stream to read.
	 * @return The lines of the input stream as a Stream.
	 * @throws IOException If there is a problem reading the input stream.
	 */
	public static Stream<String> lines(InputStream is) throws IOException {
		return lines(is, StandardCharsets.UTF_8);
	}
	
	/**
	 * 
	 * @param resource
	 * @return
	 */
	public static Properties propertiesFromClassPath(Class<?> clazz, String resource) {
	    Properties props = new Properties();
	    try (
	        InputStream is = clazz.getResourceAsStream(resource);
	        BufferedInputStream bis = new BufferedInputStream(is);
	    ) {
	       props.load(bis);
	    } catch (IOException ioe) {
	        throw new UncheckedIOException(ioe);
	    }
	    
	    return props;
	}
	
	/**
	 * Return a new PrintWriter that optionally compresses the output. If the file name ends with {@code .gz}
	 * it will be compressed using gzip. If it ends with with {@code .bz2} it will be compressed with bzip and if it
	 * ends with {@code .xz} it will be compressed with LZMA. If the file is {@code null} then
	 * the result will be written to stdout.
	 * 
	 * @param file The file to write to.
	 * @return The PrintWriter
	 * @throws FileNotFoundException If there is an error initializing the PrintWriter.
	 * @throws CompressorException If there is an error compressing the data.
	 */
	public static PrintWriter toPrintWriter(File file) throws FileNotFoundException, CompressorException {
		String compressionType = getCompressionType(file);
		
		// If this fails then no resource was acquired
		FileOutputStream fos = file == null ? null : new FileOutputStream(file);
		OutputStream os = fos == null ? new UnclosableOutputStream(System.out) : fos;
		
		// This doesn't throw any relevant exceptions
		BufferedOutputStream bos = new BufferedOutputStream(os, BUFFER_SIZE);

		// We'll need this if the compression type is not null
		CompressorOutputStream cos = null;
			
		// This will either wrap the bos or the cos
		OutputStreamWriter osw = null;
		
		// This will wrap the osw
		BufferedWriter bw = null;

		if (compressionType != null) {
			try {
				cos = new CompressorStreamFactory().createCompressorOutputStream(compressionType, bos);
			} catch (CompressorException ce) {
				try {
					bos.close();
				} catch (IOException e) {
					ce.addSuppressed(e);
				}
				
				try {
					if (fos != null) { fos.close(); }
				} catch (IOException e) {
					ce.addSuppressed(e);
				}
				
				throw ce;
			}
			
			// This doesn't throw an exception
			osw = new OutputStreamWriter(cos);
		} else {
			osw = new OutputStreamWriter(bos);
		}
		
		// This doesn't throw an exception
		bw = new BufferedWriter(osw, BUFFER_SIZE);
		
		return new PrintWriter(bw);
	}
	
	/**
	 * Construct an ObjectOutputStream given the input file.
	 * Similar to {@link #toPrintWriter(File)} the stream will be optionally
	 * compressed based on the suffix of the filename.
	 * 
	 * @param file The file where the object will be written to.
	 * @return the ObjectOutputStream.
	 * @throws CompressorException If there is an error compressing the data.
	 * @throws IOException If there is an error initializing the stream.
	 */
	public static ObjectOutputStream toObjectOutputStream(File file) throws CompressorException, IOException {
		String compressionType = getCompressionType(file);
		
		// If this throws an exception the resource was never opened.
		// If this fails then no resource was acquired
        FileOutputStream fos = file == null ? null : new FileOutputStream(file);
        OutputStream ios = fos == null ? new UnclosableOutputStream(System.out) : fos;
		
		// This doesn't throw an exception
		BufferedOutputStream bos = new BufferedOutputStream(ios, BUFFER_SIZE);
		
		// We'll need this if the compression type is not null
		CompressorOutputStream cos = null;
		
		OutputStream os = null;

		if (compressionType != null) {
			try {
				cos = new CompressorStreamFactory().createCompressorOutputStream(compressionType, bos);
			} catch (CompressorException ce) {
				try {
					bos.close();
				} catch (IOException e) {
					ce.addSuppressed(e);
				}
				
				try {
					fos.close();
				} catch (IOException e) {
					ce.addSuppressed(e);
				}
				
				throw ce;
			}
			
			os = cos;
		} else {
			os = bos;
		}
		
		try {
			return new ObjectOutputStream(os);
		} catch (IOException ioe) {
			try {
				if (cos != null) { cos.close(); }
			} catch (IOException closeEx) {
				ioe.addSuppressed(closeEx);
			}
			
			try {
				bos.close();
			} catch (IOException closeEx) {
				ioe.addSuppressed(closeEx);
			}
			
			try {
				fos.close();
			} catch (IOException closeEx) {
				ioe.addSuppressed(closeEx);
			}
			
			throw ioe;
		}
	}
	
	/**
	 * 
	 * @param file
	 * @return
	 * @throws CompressorException
	 * @throws IOException
	 */
	public static ObjectInputStream toObjectInputStream(File file) throws IOException {
	    InputStream iis = null;
        FileInputStream fis = null;
        if (file != null) {
            // If this throws an exception the resource was never opened.
            fis = new FileInputStream(file);
            iis = fis;
        } else {
            iis = new UnclosableInputStream(System.in);
        }
        
        // This doesn't throw an exception
        BufferedInputStream bis = new BufferedInputStream(iis, BUFFER_SIZE);
        
        // We'll need this if the compression type is not null
        CompressorInputStream cis = null;
        
        InputStream is = null;

        try {
            bis.mark(BUFFER_SIZE + 1);
            cis = new CompressorStreamFactory().createCompressorInputStream(bis);
            is = cis;
        } catch (CompressorException ce) {
            bis.reset();
            is = bis;
        }
        
        try {
            return new ObjectInputStream(is);
        } catch (IOException ioe) {
            try {
                if (cis != null) { cis.close(); }
            } catch (IOException closeEx) {
                ioe.addSuppressed(closeEx);
            }
            
            try {
                bis.close();
            } catch (IOException closeEx) {
                ioe.addSuppressed(closeEx);
            }
            
            try {
                fis.close();
            } catch (IOException closeEx) {
                ioe.addSuppressed(closeEx);
            }
            
            throw ioe;
        }
    }
	
	public static ObjectInputStream toObjectInputStream(InputStream is) throws IOException {
        // This doesn't throw an exception
        BufferedInputStream bis = new BufferedInputStream(is, BUFFER_SIZE);
        CompressorInputStream cis = null;
        InputStream iis = null;

        try {
            bis.mark(BUFFER_SIZE + 1);
            cis = new CompressorStreamFactory().createCompressorInputStream(bis);
            iis = cis;
        } catch (CompressorException ce) {
            bis.reset();
            iis = bis;
        }
        
        try {
            return new ObjectInputStream(iis);
        } catch (IOException ioe) {
            try {
                if (cis != null) { cis.close(); }
            } catch (IOException closeEx) {
                ioe.addSuppressed(closeEx);
            }
            
            try {
                bis.close();
            } catch (IOException closeEx) {
                ioe.addSuppressed(closeEx);
            }
            
            throw ioe;
        }
	}
	
	/**
	 * Serializes an object to a File optionally compressing it based on the file extension.
	 * 
	 * @param object The object to serialize.
	 * @param file The file where the object will be written.
	 * @throws IOException If there is a problem writing to the file.
	 * @throws CompressorException If there is a problem compressing the file.
	 */
	public static <T extends Serializable> void serialize(T object, File file) throws IOException, CompressorException {
	    try (ObjectOutputStream oos = toObjectOutputStream(file)) {
	        oos.writeObject(object);
	    }
	}
	
	/**
	 * Deserializes an object from a File that is optionally compressed.
	 * 
	 * @param file The file where the object is serialized.
	 * @return The deserialized object.
	 * @throws IOException If there is error reading from the file.
	 * @throws ClassNotFoundException If the serialized object is of an unknown class.
	 */
	@SuppressWarnings("unchecked")
    public static <T extends Serializable> T deserialize(File file) throws IOException, ClassNotFoundException {
	    try (ObjectInputStream ois = toObjectInputStream(file)) {
	        return (T) ois.readObject();
	    }
	}

	//-----------------------------------------------------------------------//
	//-- Managed Properties -------------------------------------------------//
	//-----------------------------------------------------------------------//

	//-----------------------------------------------------------------------//
	//-- Properties ---------------------------------------------------------//
	//-----------------------------------------------------------------------//
	//-- Stative ------------------------------------------------------------//

	//-- Functional ---------------------------------------------------------//

	//-----------------------------------------------------------------------//
	//-- Initialization -----------------------------------------------------//
	//-----------------------------------------------------------------------//

	//-----------------------------------------------------------------------//
	//-- Getters & Setters --------------------------------------------------//
	//-----------------------------------------------------------------------//

	//-----------------------------------------------------------------------//
	//-- Methods ------------------------------------------------------------//
	//-----------------------------------------------------------------------//

	//-----------------------------------------------------------------------//
	//-- Actions ------------------------------------------------------------//
	//-----------------------------------------------------------------------//

	//-----------------------------------------------------------------------//
	//-- Utility Methods ----------------------------------------------------//
	//-----------------------------------------------------------------------//

	//-----------------------------------------------------------------------//
	//-- Static Utility Methods ---------------------------------------------//
	//-----------------------------------------------------------------------//
	/**
	 * 		
	 * @param reader
	 * @return
	 * @throws IOException
	 */
	private static String toString(Reader reader) throws IOException {
		StringBuilder sb = new StringBuilder();
		
		char[] buffer = new char[BUFFER_SIZE];
		while (true) {
			int n = reader.read(buffer);
			if (n < 0) { break; }
			sb.append(buffer, 0, n);
		}
		
		return sb.toString();
	}
	
	/**
	 * 
	 * @param closeables
	 * @return
	 */
	private static Runnable asUnchecked(Closeable...closeables) {
		return
			() -> {
				for (Closeable c : closeables) {
					try {
						if (c != null) { c.close(); }
					} catch (IOException ioe) {
						throw new UncheckedIOException(ioe);
					}
				}
			};
	}
	
	private static String getCompressionType(File file) {
	    if (file == null) { return null; }
	    
		String compressionType = null;
		if (file.getName().endsWith(".gz")) {
			compressionType = CompressorStreamFactory.GZIP;
		} else if (file.getName().endsWith(".bz2")) {
			compressionType = CompressorStreamFactory.BZIP2;
		} else if (file.getName().endsWith(".xz")) {
			compressionType = CompressorStreamFactory.XZ;
		}
		
		return compressionType;
	}
}
