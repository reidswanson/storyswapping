/**
 * 
 */
package edu.usc.ict.nld.storyswap;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;

import edu.stanford.nlp.coref.CorefCoreAnnotations.CorefChainAnnotation;
import edu.stanford.nlp.coref.data.CorefChain;
import edu.stanford.nlp.coref.data.CorefChain.CorefMention;
import edu.stanford.nlp.coref.data.Dictionaries;
import edu.stanford.nlp.coref.data.Dictionaries.MentionType;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations.EnhancedPlusPlusDependenciesAnnotation;
import edu.stanford.nlp.trees.GrammaticalRelation;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.IntPair;
import edu.stanford.nlp.util.Pair;
import edu.stanford.nlp.util.TypesafeMap.Key;
import edu.usc.ict.nld.storyswap.PersonalPronouns.Gender;
import edu.usc.ict.nld.storyswap.PersonalPronouns.Person;
import edu.usc.ict.nld.storyswap.PersonalPronouns.Role;
import edu.usc.ict.nld.storyswap.dela.DelaDerivation;
import edu.usc.ict.nld.storyswap.dela.DelaEntry;
import edu.usc.ict.nld.storyswap.dela.DelaEntry.PartOfSpeech;
import edu.usc.ict.nld.storyswap.dela.DelaMorphologyDictionary;

/**
 * @author reid
 *
 */
public class PovTransformer implements Serializable {
    private static final long serialVersionUID = 1L;
    static final Logger LOG = LoggerFactory.getLogger(PovTransformer.class);

    //-----------------------------------------------------------------------//
    //-- Nested Classes -----------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Static Fields ------------------------------------------------------//
    //-----------------------------------------------------------------------//
    private static Class<? extends Key<List<CoreMap>>> SENTENCES = SentencesAnnotation.class;
    private static Class<? extends Key<Map<Integer, CorefChain>>> COREF = CorefChainAnnotation.class;

    //-----------------------------------------------------------------------//
    //-- Managed Properties -------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Properties ---------------------------------------------------------//
    //-----------------------------------------------------------------------//
    //-- Stative ------------------------------------------------------------//
    protected DelaMorphologyDictionary dela;
    protected Map<String, Long> wordCounts;
    

    //-- Functional ---------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Initialization -----------------------------------------------------//
    //-----------------------------------------------------------------------//
    /**
     * 
     * @param dela
     */
    public PovTransformer(DelaMorphologyDictionary dela) {
        if (dela == null) {
            this.dela = new DelaMorphologyDictionary();
        } else {
            this.dela = dela;            
        }
        
        try (
            InputStream is = this.getClass().getResourceAsStream("/edu/usc/ict/nld/storyswap/ngram/count_1w.txt");
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
        ) {
            wordCounts =
                br.lines()
                    .filter(l -> !l.matches("^\\s*$"))
                    .map(l -> l.split("\\s+"))
                    .collect(Collectors.toMap(p -> new String(p[0]), p -> Long.parseLong(p[1])));
        } catch (IOException ioe) {
            LOG.error("Error opening word count file", ioe);
            wordCounts = new HashMap<>();
        }
    }
    
    /**
     * 
     */
    public PovTransformer() {
        this(null);
    }

    //-----------------------------------------------------------------------//
    //-- Getters & Setters --------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Methods ------------------------------------------------------------//
    //-----------------------------------------------------------------------//
    /**
     * 
     * @param annotation
     * @param targetPov
     * @return
     */
    public String transform(Annotation annotation, PointOfView targetPov) {
        Preconditions.checkNotNull(annotation);
        Preconditions.checkArgument(annotation.containsKey(COREF));
        
        // Get the story's current point of view
        PointOfView sourcePov = getPointOfView(annotation);
        
        // If the target point of view is the same as the current story then there's
        // no need to change anything
        if (sourcePov.equals(targetPov)) {
            LOG.debug("The stories point of view is the same as the target point of view");
            return annotation.get(TextAnnotation.class); 
        }
        
        // Get the coref chain of the protagonist
        CorefChain protagonist = findProtagonist(annotation);
        CorefChain protagonistPlural = findProtagonistPlural(annotation, sourcePov);
        
        LOG.debug("Original point of view: {}", sourcePov);
        LOG.debug("Original protagonist: {}", protagonist.getRepresentativeMention().mentionSpan);
        
        // Get the lookup table that maps from the sentence/token index to the corefId
        List<CorefMention> protagMentions = protagonist.getMentionsInTextualOrder();
        if (protagonistPlural != null) { protagMentions.addAll(protagonistPlural.getMentionsInTextualOrder()); }

        if (LOG.isDebugEnabled()) {
            protagMentions.forEach(m -> LOG.debug("Protagonist mention in sentence {} index {}: {}", m.sentNum, m.startIndex, m.mentionSpan));
        }
        
        replaceSingularMentions(annotation, protagMentions, targetPov);
       
        StringBuilder transformed = new StringBuilder();
        for (CoreMap sentence : annotation.get(SENTENCES)) {
            SemanticGraph graph = sentence.get(EnhancedPlusPlusDependenciesAnnotation.class);
            List<String> words = graph.vertexListSorted().stream().map(iw -> iw.word()).collect(Collectors.toList());
            String s = PTBTokenizer.ptb2Text(String.join(" ", words));
            transformed.append(s).append(System.lineSeparator());
        }
        
        return transformed.toString();
    }
    
    protected boolean isStandardSubject(IndexedWord parentWord, GrammaticalRelation relation) {
        return 
            parentWord.tag().startsWith("V") && 
            (relation.getShortName().equals("nsubj") || relation.getShortName().equals("nsubjpass"));
    }
    
    protected boolean isCopularSubject(SemanticGraph graph, IndexedWord word, IndexedWord parent, GrammaticalRelation relation) {
        if (!(relation.getShortName().equals("nsubj") || relation.getShortName().equals("nsubjpass"))) { return false; }
        if (!(word.tag().startsWith("N") || word.tag().startsWith("PRP"))) { return false; }
        
        Pair<GrammaticalRelation, IndexedWord> pair = findCopularVerb(graph, parent);
        
        return pair != null;
    }
    
    protected Pair<GrammaticalRelation, IndexedWord> findAux(SemanticGraph graph, IndexedWord word) {
        List<Pair<GrammaticalRelation, IndexedWord>> children = graph.childPairs(word);
        for (Pair<GrammaticalRelation, IndexedWord> pair : children) {
            if (pair.first().getShortName().equals("aux") || pair.first().getShortName().equals("auxpass")) {
                return pair;
            }
        }
        
        return null;
    }
    
    protected Pair<GrammaticalRelation, IndexedWord> findCopularVerb(SemanticGraph graph, IndexedWord word) {
        List<Pair<GrammaticalRelation, IndexedWord>> children = graph.childPairs(word);
        for (Pair<GrammaticalRelation, IndexedWord> pair : children) {
            if (pair.first().getShortName().equals("cop")) {
                return pair;
            }
        }
        
        return null;
    }
    
    public PointOfView getPointOfView(Annotation annotation) {
        CorefChain protagChain = findProtagonist(annotation);
        LOG.debug("Protag chain: {}", protagChain);
        
        List<CorefMention> mentions = protagChain.getMentionsInTextualOrder();
        List<PersonalPronouns.Pronoun> pronouns =
            mentions.stream()
                .filter(m -> PersonalPronouns.isPronoun(m.mentionSpan))
                .map(m -> PersonalPronouns.Pronoun.lookup(m.mentionSpan))
                .collect(Collectors.toList());
        
        if (pronouns.isEmpty()) {
            LOG.warn("Not enough pronouns in the protagonists coref chain. Unable to determin point of view");
            return null;
        }
        
        Map<Person, Long> personCounts =
            pronouns.stream()
                .map(p -> p.person)
                .collect(groupingBy(identity(), counting()));
        
        Map<PersonalPronouns.Number, Long> numberCounts =
            mentions.stream()
                .map(m -> m.number == Dictionaries.Number.PLURAL ? PersonalPronouns.Number.PLURAL : PersonalPronouns.Number.SINGULAR)
                .collect(groupingBy(identity(), counting()));
        
        Map<PersonalPronouns.Gender, Long> genderCounts =
            mentions.stream()
                .map(m -> convert(m.gender))
                .collect(groupingBy(identity(), counting()));
        
        
        // This can't/shouldn't be empty because of the check above
        Person mostFrequentPerson =
            personCounts.entrySet().stream()
                .sorted((e1, e2) -> (int) (e2.getValue() - e1.getValue()))
                .map(e -> e.getKey())
                .limit(1)
                .collect(Collectors.toList())
                .get(0);
        
        PersonalPronouns.Number mostFrequentNumber =
            numberCounts.entrySet().stream()
                .sorted((e1, e2) -> (int) (e2.getValue() - e1.getValue()))
                .map(e -> e.getKey())
                .limit(1)
                .collect(Collectors.toList())
                .get(0);
        
        PersonalPronouns.Gender mostFrequentGender =
            genderCounts.entrySet().stream()
                .sorted((e1, e2) -> (int) (e2.getValue() - e1.getValue()))
                .map(e -> e.getKey())
                .limit(1)
                .collect(Collectors.toList())
                .get(0);
        
        return new PointOfView(mostFrequentNumber, mostFrequentPerson, mostFrequentGender);
    }
    
    /**
     * Finds the protagonist of the story using a few simple heuristics.
     * 
     * @param annotation
     * @return
     */
    public CorefChain findProtagonist(Annotation annotation) {
        Map<Integer, CorefChain> corefMap = annotation.get(COREF);

        // Likely properties of the protagonists chain
        // 1) It is probably the longest
        // 2) (the representative mention of a first person story is not
        // necessarily I)
        // 3) If it is a third person story the representative mention is
        // probably a proper noun
        // Other potentially properties:
        // Number of pronouns

        // TODO enhance this heuristic
        List<CorefChain> chains = new ArrayList<>(corefMap.values());
        Collections.sort(chains, (cc1, cc2) -> cc2.getMentionsInTextualOrder().size() - cc1.getMentionsInTextualOrder().size());
        //System.out.println(chains);
        // Return the longest chain that contains a personal pronoun
        //System.out.println(corefMap.values());
        //for (CorefChain chain : chains) {
        //	System.out.println(chain);
        //}
        for (CorefChain chain : chains) {
            if (containsPronoun(chain)) {
                return chain;
            }
        }
        
        return chains.get(0);
    }
    
    private boolean containsPronoun(CorefChain chain) {
        for (CorefMention m : chain.getMentionsInTextualOrder()) {
            if (m.mentionType == MentionType.PRONOMINAL) { return true; }
        }
        
        return false;
    }
    
    public CorefChain findProtagonistPlural(Annotation annotation, PointOfView sourcePov) {
        if (sourcePov.person != PersonalPronouns.Person.FIRST) {
            // TODO handle other points of view (note these will likely be trickier)
            return null;
        }
        Map<Integer, CorefChain> corefMap = annotation.get(COREF);
        List<CorefChain> chains = new ArrayList<>(corefMap.values());
        CorefChain targetChain = null;
        int maxCount = 0;
        for (CorefChain chain : chains) {
            List<CorefMention> mentions = chain.getMentionsInTextualOrder();
            List<PersonalPronouns.Pronoun> pronouns =
                mentions.stream()
                    .filter(m -> PersonalPronouns.isPronoun(m.mentionSpan))
                    .map(m -> PersonalPronouns.Pronoun.lookup(m.mentionSpan))
                    .collect(Collectors.toList());
            
            int count = (int)
                pronouns.stream()
                    .filter(p -> p.person == Person.FIRST && p.number == PersonalPronouns.Number.PLURAL)
                    .count();
            
            if (count > maxCount) {
                targetChain = chain;
            }
        }
        
        return targetChain;
    }

    //-----------------------------------------------------------------------//
    //-- Actions ------------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Utility Methods ----------------------------------------------------//
    //-----------------------------------------------------------------------//
    /**
     * Maps from a (sentNum, tokenIndex) -> corefId.
     * 
     * @param annotation
     * @return
     */
    protected Map<IntPair, Integer> createCorefLookup(Annotation annotation) {
        Map<IntPair, Integer> lookup = new HashMap<>();
        
        Map<Integer, CorefChain> corefGraph = annotation.get(CorefChainAnnotation.class);
        corefGraph.forEach((corefId, cc) -> {
            List<CorefMention> mentions = cc.getMentionsInTextualOrder();
            mentions.forEach(m -> {
                int sentNum = m.sentNum;
                int startIndex = m.startIndex;
                int endIndex = m.endIndex;
                
                for (int i = startIndex; i < endIndex; ++i) {
                    lookup.put(new IntPair(sentNum, i), corefId);
                }
            });
        });
        
        return lookup;
    }

    protected PersonalPronouns.Gender convert(Dictionaries.Gender gender) {
        if (gender == Dictionaries.Gender.MALE) { return PersonalPronouns.Gender.MASCULINE; }
        if (gender == Dictionaries.Gender.FEMALE) { return PersonalPronouns.Gender.FEMININE; }
        if (gender == Dictionaries.Gender.NEUTRAL) { return PersonalPronouns.Gender.NEUTER; }
        if (gender == Dictionaries.Gender.UNKNOWN) { return PersonalPronouns.Gender.DEICTIC; }
        
        return PersonalPronouns.Gender.DEICTIC;
    }
    
    protected String derive(IndexedWord word, PersonalPronouns.Number number, PersonalPronouns.Person person) {
        String w = word.word();
        String pennTag = word.tag();
        String delaTense = null;
        String delaNumber = null;
        String delaPerson = null;
        
        if (pennTag.equals("VB")) { delaTense = "inf"; }
        else if (pennTag.equals("VBD")) { delaTense = "spast"; }
        else if (pennTag.equals("VBG")) { delaTense = "gerondif"; }
        else if (pennTag.equals("VBN")) { delaTense = "ppast"; }
        else if (pennTag.equals("VBP")) { delaTense = "present"; }
        else if (pennTag.equals("VBZ")) { delaTense = "ind"; }
        else if (pennTag.equals("AUX")) { delaTense = "ind"; }
        else if (pennTag.equals("AUXPASS")) { delaTense = "ind"; }
        else { delaTense = "?"; }
        
        if (number == PersonalPronouns.Number.PLURAL) { delaNumber = "plural"; }
        else { delaNumber = "singular"; }
        
        if (person == PersonalPronouns.Person.FIRST) { delaPerson = "1"; }
        else if (person == PersonalPronouns.Person.SECOND) { delaPerson = "2"; }
        else if (person == PersonalPronouns.Person.THIRD) { delaPerson = "3"; }
        
        Set<DelaEntry> entries = dela.find(word.word());
        Map<DelaDerivation, Integer> derivationFeatureCount = new HashMap<>();
        
        //System.out.println("delaTense: ");
        //System.out.println(delaTense);
        //System.out.println("delaNumber: ");
        //System.out.println(delaNumber);
        //System.out.println("delaPerson: ");
        //System.out.println(delaPerson);
        
        // get the candidate replacements
        for (DelaEntry entry : entries) {
            if (entry.getPartOfSpeech().equals(PartOfSpeech.VERB)) {
                for (DelaDerivation derivation : entry.getDerivations()) {
                    String dtense = derivation.getFeature("tense");
                    String dnumber = derivation.getFeature("number");
                    String dperson = derivation.getFeature("person");
                    
                    // TODO this needs work
                    if (delaTense.equals("inf") && dtense != null && dtense.equals("inf")) {
                        return derivation.getForm();
                    }
                    
                    // TODO find the most specific match
                    //System.out.println("---");
                    //System.out.println(dtense);
                    //System.out.println(dnumber);
                    //System.out.println(dperson);
                    //System.out.println("---");
                    int matchingFeatures = 0;
                    if (dtense != null && dtense.equals(delaTense)) {
                        matchingFeatures++;
                    }
                    
                    if (dnumber != null && dnumber.equals(delaNumber)) {
                        matchingFeatures++;
                    }
                    
                    if (dperson != null && dperson.equals(delaPerson)) {
                        matchingFeatures++;
                    }
                    
                    derivationFeatureCount.put(derivation, matchingFeatures);
                }
            }
        }
        
        // Derivations with the most matching features are 
        if (derivationFeatureCount.isEmpty()) {
            LOG.warn("Unable to find derivation for word: {}/{} tense: {} number: {} person: {}", word.word(), word.tag(), delaTense, delaNumber, delaPerson);
            LOG.debug("{}", entries);
            return word.word();
        }
        
        int maxMatchingFeatures = Collections.max(derivationFeatureCount.values());
        List<String> candidates = 
            derivationFeatureCount.entrySet().stream()
                .filter(e -> e.getValue() == maxMatchingFeatures)
                .map(e -> e.getKey().getForm())
                .sorted((f1, f2) -> wordCounts.getOrDefault(f1.toLowerCase(), 0L).compareTo(wordCounts.getOrDefault(f2.toLowerCase(), 0L)))
                .collect(Collectors.toList());
        //System.out.println(candidates);
        if (candidates.isEmpty()) {
            LOG.warn("Unable to find derivation for word: {}/{} tense: {} number: {} person: {}", word.word(), word.tag(), delaTense, delaNumber, delaPerson);
            LOG.debug("{}", entries);
            return word.word();
        }
        
        String result = candidates.get(candidates.size() - 1); 
        LOG.trace("From {} to {}", w, result);
        
        return result;
    }
    
    protected void replaceSingularMentions(Annotation annotation, List<CorefMention> protagMentions, PointOfView targetPov) {
        List<CoreMap> sentences = annotation.get(SENTENCES);
        for (int i = 0; i < sentences.size(); ++i) {
            List<CorefMention> mentions = findInSentence(protagMentions, i);
            CoreMap sentence = annotation.get(SENTENCES).get(i);
            SemanticGraph oldGraph = sentence.get(EnhancedPlusPlusDependenciesAnnotation.class);
            // TODO: after this for I have to check if I have a mention of another character
            // If I do, I have to set a flag to change the following pronoun for a proper noun. 
            for (CorefMention m : mentions) {
                int startIndex = m.startIndex;
                int endIndex = m.endIndex;
                
                // Only try to transform single words for now
                if (endIndex - startIndex > 1) {
                	//System.out.println("Different words");
                	//System.out.println(oldGraph.getNodeByIndex(startIndex).word());
                	//System.out.println(oldGraph.getNodeByIndex(endIndex).word());
                    // TODO handle phrases
                    continue;
                }
                
                IndexedWord w = oldGraph.getNodeByIndex(startIndex);
                List<Pair<GrammaticalRelation, IndexedWord>> parents = oldGraph.parentPairs(w);
                
                String word = w.word();
                //System.out.println("The word is: " + word);
                String pos = w.tag();
                String targetWord = word;
                
                //PersonalPronouns.Role targetRole = PersonalPronouns.role(word);
                PersonalPronouns.Role targetRole = getRole(w, parents);
                PersonalPronouns.Person targetPerson = targetPov.person;
                PersonalPronouns.Number targetNumber = targetPov.number;
                PersonalPronouns.Gender targetGender = targetPov.gender;
                
                for (Pair<GrammaticalRelation, IndexedWord> pair : parents) {
                    GrammaticalRelation rel = pair.first;
                    IndexedWord pw = pair.second;
                  
                    LOG.trace("word: {} parent: {} relation: {}", w.word(),  pw.word(), rel.getShortName());
                    // TODO Improve these heuristics
                    if (isStandardSubject(pw, rel)) {
                        String newVerb = derive(pw, targetNumber, targetPerson);
                        
                        pw.setWord(newVerb);
                        
                        Pair<GrammaticalRelation, IndexedWord> aux = findAux(oldGraph, pw);
                        if (aux != null) {
                            String newAux = derive(aux.second(), targetNumber, targetPerson);
                            aux.second().setWord(newAux);
                        }
                        LOG.trace("Standard subject");
                    } else if (isCopularSubject(oldGraph, w, pw, rel)) {
                        Pair<GrammaticalRelation, IndexedWord> copularPair = findCopularVerb(oldGraph, pw);
                        //String oldVerb = copularPair.second.word();
                        //System.out.println("Copular papaaaa:");
                        //System.out.println(copularPair);
                        String newVerb = derive(copularPair.second(), targetNumber, targetPerson);
                        //System.out.println("New verb is:");
                        //System.out.println(newVerb);
                        copularPair.second.setWord(newVerb);
                        
                        LOG.trace("Copular");
                    } else {
                        LOG.trace("Some other relation");
                    }
                }
                  
                if (pos.startsWith("PRP")) {
                    PersonalPronouns.Number sourceNumber = PersonalPronouns.number(word);

                    targetRole = getRole(w, parents);
                    targetPerson = targetPov.person;
                    targetNumber = sourceNumber == PersonalPronouns.Number.PLURAL ? sourceNumber : targetPov.number;
                    targetGender = sourceNumber == PersonalPronouns.Number.PLURAL ? Gender.DEICTIC : targetPov.gender;
                    
                    //System.out.println("TargetRole is:");
                    //System.out.println(targetRole);
                    //System.out.println("TargetPerson is:");
                    //System.out.println(targetPerson);
                    //System.out.println("TargetNumber is:");
                    //System.out.println(targetNumber);
                    //System.out.println("TargetGender is: ");
                    //System.out.println(targetGender);
                    targetWord = PersonalPronouns.Pronoun.from(targetRole, targetPerson, targetNumber, targetGender);
                    //System.out.println(targetWord);
                    //if (targetWord == null) { System.out.println("Boo"); }
                    
                    if (startIndex == 1 || targetWord.equals("i")) { targetWord = StringUtils.capitalize(targetWord); }
                    w.setWord(targetWord);
                } else if (pos.startsWith("NNP")) {
                    // I use the CoreNLP library to get information about the propner noun in order to change it.
                	
                	PersonalPronouns.Number sourceNumber = toPersonalPronoun(m.number.toString());
                    targetRole = getRole(w, parents);

                    targetPerson = targetPov.person;
                    targetNumber = sourceNumber == PersonalPronouns.Number.PLURAL ? sourceNumber : targetPov.number;
                    targetGender = sourceNumber == PersonalPronouns.Number.PLURAL ? Gender.DEICTIC : targetPov.gender;
                    targetWord = PersonalPronouns.Pronoun.from(targetRole, targetPerson, targetNumber, targetGender);
                    
                    if (startIndex == 1 || targetWord.equals("i")) { targetWord = StringUtils.capitalize(targetWord); }
                    w.setWord(targetWord);
                }
                
                
            }
        }
    }
    
    private PersonalPronouns.Number toPersonalPronoun(String inputNumber) {
    	if (inputNumber == "SINGULAR") return PersonalPronouns.Number.SINGULAR;
    	else return PersonalPronouns.Number.PLURAL;
    }
    
    private PersonalPronouns.Role getRole(IndexedWord w, List<Pair<GrammaticalRelation, IndexedWord>> parents) {
        List<PersonalPronouns.Pronoun> candidates = PersonalPronouns.Pronoun.lookupMultiple(w.word());
        

        if (candidates != null && candidates.size() == 1) { return candidates.get(0).role; }
        
        // TODO improve
        for (Pair<GrammaticalRelation, IndexedWord> pair : parents) {
            String rel = pair.first.getShortName();
            if ("nsubj".equals(rel) || "nsubjpass".equals(rel)) { return Role.SUBJECT; }
            if ("dobj".equals(rel)) { return Role.OBJECT; }
            if ("iobj".equals(rel)) { return Role.OBJECT; }
            if ("nmod:poss".equals(rel)) { return Role.DEPENDENT_POSSESSIVE; }
        }
        
        // Not ideal, but if we can't identify the role, then guess that it is 
        // a SUBJECT
        return Role.SUBJECT;
    }
    
    private List<CorefMention> findInSentence(List<CorefMention> allMentions, int sentIdx) {
        return
            allMentions.stream()
                .filter(m -> m.sentNum - 1 == sentIdx)
                .collect(Collectors.toCollection(ArrayList::new));
    }
}
