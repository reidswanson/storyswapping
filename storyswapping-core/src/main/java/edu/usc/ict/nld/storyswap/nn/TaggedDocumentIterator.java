/**
 * 
 */
package edu.usc.ict.nld.storyswap.nn;

import static java.util.Arrays.asList;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;

import edu.usc.ict.nld.storyswap.io.DatabaseConnectionProperties;
import edu.usc.ict.nld.storyswap.io.DatabaseConnectionProperties.Key;

/**
 * @author reid
 *
 */
public class TaggedDocumentIterator implements Iterator<List<String>> {
    static final Logger LOG = LoggerFactory.getLogger(TaggedDocumentIterator.class);

    //-----------------------------------------------------------------------//
    //-- Nested Classes -----------------------------------------------------//
    //-----------------------------------------------------------------------//
    private final class QueryResult {
        public int storyId;
        public String sentence;

        public QueryResult(int storyId, String sentence) {
            this.storyId = storyId;
            this.sentence = sentence;
        }
        
        /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString() {
            return String.format("%8d %s", storyId, sentence);
        }
    }
    
    private final class Producer extends Thread {
        @Override
        public void run() {
            LOG.debug("Starting producer");
            while (!finished.get()) {
                List<QueryResult> batch = getBatch();
                
                queueLock.lock();
                try {
                    while (!finished.get() && queue.size() >= MAX_QUEUE_SIZE) {
                        queueNotFull.await();
                    }
                    
                    if (batch.isEmpty() || finished.get()) { 
                        break;
                    }
                    
                    int limit = Integer.parseInt(dbProps.value(Key.LIMIT));
                    limit = limit == 0 ? Integer.MAX_VALUE : limit;
                    for (int i = 0; i < batch.size() && totalCount < limit; ++i, ++totalCount) {
                        queue.add(batch.get(i));
                    }
                    
                    if (totalCount == limit) { finished.set(true); }
                    
                    queueNotEmpty.signalAll();
                } catch (InterruptedException e) {
                    LOG.error("Error putting element on queue", e);
                } finally {
                    queueLock.unlock();
                }
            }
            
            finished.set(true);
            
            queueLock.lock();
            try {
                queueNotEmpty.signalAll();
            } finally {
                queueLock.unlock();
            }
        }
    }

    //-----------------------------------------------------------------------//
    //-- Static Fields ------------------------------------------------------//
    //-----------------------------------------------------------------------//
    public static final Set<String> CURSE_WORDS =
        new HashSet<>(asList("porn", "fuck", "fucked", "fucking", "fucker", "fucks", "dick", "dicks", "pussy", "cunt", "twat", "sex", "xxx", "asshole", "bitch", "shit", "shits", "shitting", "shitted", "shitter", "dildo", "dildos", "vibrator", "cum", "cumming", "tit", "tits", "cock", "cocks"));
    public static final double UPPERCASE_THRESH = 0.142 - 0.12; // (mean - stdev)
    public static final double SENT_START_UPPERCASE_THRESH = 0.800; // a little less than the mean
    public static final double I_UPPERCASE_THRESH = 0.928; // mean
    public static final double PUNCT_THRESH = 0.05; // mean / 2
    public static final double CURSE_THRESH = 0.005; // mean * 5
    
    public static final String DOCUMENT_START = Character.toString((char) 2);
    public static final String DOCUMENT_END = Character.toString((char) 3);
    private static final int MAX_QUEUE_SIZE = 100000;
    private static final int BATCH_SIZE = 50000;

    //-----------------------------------------------------------------------//
    //-- Managed Properties -------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Properties ---------------------------------------------------------//
    //-----------------------------------------------------------------------//
    //-- Stative ------------------------------------------------------------//
    private DatabaseConnectionProperties dbProps;
    private String connectionUri;
    private String queryFmt;
    private int offset;
    private int minDocLen;
    private int maxDocLen;
    private boolean useHeuristics;
    private List<String> currentDocument;
    private Deque<QueryResult> queue;
    private AtomicBoolean finished;
    private Producer producer;
    private int totalCount;
    private Lock queueLock;
    private Condition queueNotFull;
    private Condition queueNotEmpty;

    //-- Functional ---------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Initialization -----------------------------------------------------//
    //-----------------------------------------------------------------------//
    /**
     * 
     * @param connectionProperties
     */
    public TaggedDocumentIterator(DatabaseConnectionProperties connectionProperties) {
        this(connectionProperties, 0, Integer.MAX_VALUE, false);
    }
    
    /**
     * 
     * @param connectionProperties
     * @param minDocLen
     * @param maxDocLen
     */
    public TaggedDocumentIterator(DatabaseConnectionProperties connectionProperties, int minDocLen, int maxDocLen, boolean useHeuristics) {
        this.connectionUri =
            String.format(
                "jdbc:mysql://%s:%s/%s?autoReconnect=true&netTimeoutForStreamingResults=6000000&user=%s&password=%s&useSSL=false",
                connectionProperties.value(Key.HOST),
                connectionProperties.value(Key.PORT),
                connectionProperties.value(Key.DATABASE),
                connectionProperties.value(Key.USERNAME),
                connectionProperties.value(Key.PASSWORD)
            );
        
        this.dbProps = connectionProperties;
        this.queryFmt = "select story_id, sentence_pos from sentence where is_spam is NULL and is_duplicate is NULL and story_id > %d and story_id < %d order by story_id, sentence_num";
        this.minDocLen = minDocLen;
        this.maxDocLen = maxDocLen;
        this.useHeuristics = useHeuristics;
        this.queueLock = new ReentrantLock();
        this.queueNotFull = this.queueLock.newCondition();
        this.queueNotEmpty = this.queueLock.newCondition();
        this.currentDocument = new ArrayList<>();
        this.queue = new ArrayDeque<>();
        this.finished = new AtomicBoolean(true);
        
        reset();
    }

    //-----------------------------------------------------------------------//
    //-- Getters & Setters --------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Methods ------------------------------------------------------------//
    //-----------------------------------------------------------------------//
    /* (non-Javadoc)
     * @see java.util.Iterator#hasNext()
     */
    @Override
    public boolean hasNext() {
        return currentDocument.size() > 2;
    }
    
    /*
     * (non-Javadoc)
     * @see java.util.Iterator#next()
     */
    @Override
    public List<String> next() {
        List<String> result = new ArrayList<>(currentDocument);
        
        readNext();
        
        return result;
    }
    
    public void reset() {
        close();
        
        this.offset = 0;
        this.totalCount = 0;
        
        finished.set(false);
        LOG.debug("Starting new producer");
        producer = new Producer();
        producer.start();
        
        readNext();
    }
    
    public void close() {
        finished.set(true);
        
        if (producer != null) {
            queueLock.lock();
            try {
                queueNotFull.signalAll();
            } finally {
                queueLock.unlock();
            }
            
            try {
                producer.join();
                producer = null;
                queue.clear();
            } catch (InterruptedException e) {
                LOG.error("Producer interrupted", e);
            }
        }
    }

    //-----------------------------------------------------------------------//
    //-- Actions ------------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Utility Methods ----------------------------------------------------//
    //-----------------------------------------------------------------------//
    private void readNext() {
        currentDocument.clear();
        currentDocument.add(DOCUMENT_START);
        
        int storyId = 0;
        QueryResult qr = null;
        
        queueLock.lock();
        try {
            while (!finished.get() && queue.isEmpty()) {
                queueNotEmpty.await();
            }
            
            if (queue.isEmpty()) {
                currentDocument.add(DOCUMENT_END);
                return;
            }
            
            qr = queue.pop();
            queueNotFull.signalAll();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } finally {
            queueLock.unlock();
        }
        
        storyId = qr.storyId;
        
        currentDocument.add(qr.sentence);
        QueryResult peeked = null;
        while ((peeked = peek()) != null && peeked.storyId == storyId) {
            queueLock.lock();
            try {
                qr = queue.pop();
                queueNotFull.signalAll();
            } finally {
                queueLock.unlock();
            }
                
            currentDocument.add(qr.sentence);
        }
        
        currentDocument.add(DOCUMENT_END);
        
        int doclen = (int) currentDocument.stream().map(l -> asList(l.split("\\s+"))).flatMap(s -> s.stream()).count() - 2;
        
        if (doclen < minDocLen || doclen > maxDocLen) { readNext(); }
        if (!isCoherent()) { readNext(); }
    }
    
    private QueryResult peek() {
        QueryResult peeked = null;
        
        queueLock.lock();
        try {
            while (!finished.get() && queue.isEmpty()) {
                queueNotEmpty.await();
            }
            
            peeked = queue.peekFirst();
        } catch (InterruptedException e) {
            Throwables.propagate(e);
        } finally {
            queueLock.unlock();
        }
        
        return peeked;
    }
    
    private List<QueryResult> getBatch() {
        List<QueryResult> result = new ArrayList<>();
        
        String query = String.format(queryFmt, offset, offset + BATCH_SIZE);        
        offset += BATCH_SIZE;

        try (
            Connection c = DriverManager.getConnection(connectionUri);
            PreparedStatement s = c.prepareStatement(query);
            ResultSet r = s.executeQuery();
        ) {
            while (r.next()) {
                int storyId = r.getInt(1);
                String sent = r.getString(2);
                
                result.add(new QueryResult(storyId, sent));
            }
        } catch (SQLException e) {
            Throwables.propagate(e);
        }
        
        return result;
    }
    
    private boolean isCoherent() {
        if (!useHeuristics) { return true; }
        
        double allUpperFreq = getUpperFreq();
        if (allUpperFreq < UPPERCASE_THRESH) { return false; }
        
        double sentStartUpperFreq = getSentStartUpperFreq();
        if (sentStartUpperFreq < SENT_START_UPPERCASE_THRESH) { return false; }
        
        double iUpperFreq = getIUpperFreq();
        if (iUpperFreq < I_UPPERCASE_THRESH) { return false; }
        
        double curseFreq = getCurseFreq();
        if (curseFreq > CURSE_THRESH) { return false; }
        
        return true;
    }
    
    private double getUpperFreq() {
        long upper =
            currentDocument.stream()
                .skip(1)
                .limit(currentDocument.size() - 1)
                .map(s -> Arrays.asList(s.split("\\s+")))
                .flatMap(s -> s.stream())
                .filter(t -> t.indexOf('_') != -1)
                .map(t -> t.substring(0, t.lastIndexOf('_')))
                .filter(w -> Character.isUpperCase(w.charAt(0)))
                .count();
        
        long tokens =
            currentDocument.stream()
            .skip(1)
            .limit(currentDocument.size() - 1)
            .map(s -> Arrays.asList(s.split("\\s+")))
            .flatMap(s -> s.stream())
            .count();
        
        return (double) upper / tokens;
    }
    
    private double getSentStartUpperFreq() {
        long upper =
            currentDocument.stream()
                .skip(1)
                .limit(currentDocument.size() - 1)
                .map(s -> Arrays.asList(s.split("\\s+")))
                .filter(s -> Character.isUpperCase(s.get(0).charAt(0)))
                .count();
        
        return (double) upper / (currentDocument.size() - 2);
    }
    
    private double getIUpperFreq() {
        List<String> is =
            currentDocument.stream()
                .skip(1)
                .limit(currentDocument.size() - 1)
                .map(s -> Arrays.asList(s.split("\\s+")))
                .flatMap(s -> s.stream())
                .filter(t -> t.indexOf('_') != -1)
                .map(t -> t.substring(0, t.lastIndexOf('_')))
                .filter(w -> w.toLowerCase().equals("i"))
                .collect(Collectors.toList());
        
        long count = is.stream().filter(i -> Character.isUpperCase(i.charAt(0))).count();
        
        return (double) count / is.size();
    }
    
    private double getCurseFreq() {
        long count =
            currentDocument.stream()
                .skip(1)
                .limit(currentDocument.size() - 1)
                .map(s -> Arrays.asList(s.split("\\s+")))
                .flatMap(s -> s.stream())
                .filter(t -> t.indexOf('_') != -1)
                .map(t -> t.substring(0, t.lastIndexOf('_')))
                .filter(w -> CURSE_WORDS.contains(w.toLowerCase()))
                .count();
        
        long tokens =
            currentDocument.stream()
            .skip(1)
            .limit(currentDocument.size() - 1)
            .map(s -> Arrays.asList(s.split("\\s+")))
            .flatMap(s -> s.stream())
            .count();
        
        return (double) count / tokens;
    }
}
