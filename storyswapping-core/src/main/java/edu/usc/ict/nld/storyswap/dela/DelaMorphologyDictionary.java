/**
 * 
 */
package edu.usc.ict.nld.storyswap.dela;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.io.StringReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import edu.usc.ict.nld.storyswap.exceptions.Unchecked;
import edu.usc.ict.nld.storyswap.hash.XXHashStrategy;
import edu.usc.ict.nld.storyswap.io.IOUtils;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenCustomHashMap;

/**
 * A class that encapsulates the Dela morphology dictionary from Intex/Unitex.
 * The original dictionary is available from the 
 * <a href='http://www-igm.univ-mlv.fr/~unitex/index.php?page=3&html=latest-rc.html'>
 * Unitex/GramLab website</a> under the LGPLLR.
 * 
 * @author rswanson
 *
 */
public class DelaMorphologyDictionary implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LoggerFactory.getLogger(DelaMorphologyDictionary.class);
    //-----------------------------------------------------------------------//
    //-- Nested Classes -----------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Static Fields ------------------------------------------------------//
    //-----------------------------------------------------------------------//
    /**
     * The filename on the classpath
     */
    private static final String DICT_RESOURCE = "dela-en-public-u8.dic.xml";
    
    /**
     * A reusable empty set.
     */
    private static final Set<DelaEntry> EMPTY_SET = Collections.emptySet();
    
    /**
     * The number of forms in the dictionary.
     */
    private static final int NUMBER_OF_FORMS = 602585;
    
    /**
     * A global instance.
     */
    private static DelaMorphologyDictionary instance = null;

    //-----------------------------------------------------------------------//
    //-- Static Methods -----------------------------------------------------//
    //-----------------------------------------------------------------------//
    /**
     * 
     * @return
     */
    public static DelaMorphologyDictionary defaultInstance() {
        if (instance == null) { instance = new DelaMorphologyDictionary(); }
        
        return instance;
    }

    //-----------------------------------------------------------------------//
    //-- Managed Properties -------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Properties ---------------------------------------------------------//
    //-----------------------------------------------------------------------//
    //-- Stative ------------------------------------------------------------//
    private final Object2ObjectOpenCustomHashMap <String, Set<DelaEntry>> dictionary;

    //-- Functional ---------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Initialization -----------------------------------------------------//
    //-----------------------------------------------------------------------//
    public DelaMorphologyDictionary() {
        dictionary = new Object2ObjectOpenCustomHashMap<>(NUMBER_OF_FORMS, new XXHashStrategy());
        loadDictionary();
    }

    //-----------------------------------------------------------------------//
    //-- Getters & Setters --------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Methods ------------------------------------------------------------//
    //-----------------------------------------------------------------------//
    /**
     * Find all the entries in which the given derivation appears.
     * 
     * @param derivation
     * @param pos
     * @return
     */
    public Set<DelaEntry> find(String derivation) {
        return Collections.unmodifiableSet(dictionary.getOrDefault(derivation, EMPTY_SET));
    }
    
    /**
     * Find all the possible lemmas for the given derivation.
     * 
     * @param derivation
     * @return
     */
    public Set<String> findLemmas(String derivation) {
        Set<DelaEntry> entries = find(derivation);
        
        return
            entries.stream()
                .map(entry -> entry.getLemma())
                .distinct()
                .collect(Collectors.toCollection(() -> new HashSet<>()));
    }

    //-----------------------------------------------------------------------//
    //-- Actions ------------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Utility Methods ----------------------------------------------------//
    //-----------------------------------------------------------------------//
    private void loadDictionary() {
        LOG.info("Started loading Dela dictionary");
        try(
            InputStream is = DelaMorphologyDictionary.class.getResourceAsStream(DICT_RESOURCE);
        ) {
            String xml = IOUtils.toString(is);
            XMLReader parser = XMLReaderFactory.createXMLReader();
            DelaMorphologyXmlHandler handler = new DelaMorphologyXmlHandler();
            
            parser.setContentHandler(handler);
            parser.parse(new InputSource(new StringReader(xml)));
            
            // Merge entries that have the same lemma/pos/features
            Map<DelaEntry, DelaEntry> unique = new HashMap<>();
            for (DelaEntry entry : handler.getEntries()) {
                DelaEntry found = unique.get(entry);
                if (found == null) {
                    unique.put(entry, entry);
                } else {
                    found.getDerivations().addAll(entry.getDerivations());
                }
            }
            
            // Add the merged entries into the dictionary
            for (DelaEntry entry : unique.keySet()) {
                add(entry.getLemma(), entry);
                for (DelaDerivation derivation : entry.getDerivations()) {
                    add(derivation.getForm(), entry);
                }
            }
        } catch (SAXException | IOException e) {
            LOG.error("Error parsing dela XML file");
            Unchecked.rethrow(e);
        }
        LOG.info("Finished loading Dela dictionary");
    }

    private void add(String form, DelaEntry entry) {
        dictionary
            .computeIfAbsent(form.toLowerCase().trim(), f -> new HashSet<>())
            .add(entry);
    }
    //-----------------------------------------------------------------------//
    //-- Static Utility Methods ---------------------------------------------//
    //-----------------------------------------------------------------------//
    
}
