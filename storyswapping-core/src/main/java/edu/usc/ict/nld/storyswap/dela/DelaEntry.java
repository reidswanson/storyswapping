/**
 * 
 */
package edu.usc.ict.nld.storyswap.dela;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.google.common.base.Preconditions;
import com.google.gson.GsonBuilder;

/**
 * @author rswanson
 *
 */
public class DelaEntry implements Serializable {
    private static final long serialVersionUID = 1L;

    //-----------------------------------------------------------------------//
    //-- Nested Classes -----------------------------------------------------//
    //-----------------------------------------------------------------------//
    /**
     * <pos name='ADVA'/>
      9   <pos name='GN'/>
     10   <pos name='NA'/>
     11   <pos name='NES'/> (only used for qwerty)
     12   <pos name='PART'/>
     13   <pos name='PRED'/>
     14   <pos name='PRON'/>
     15   <pos name='VA'/>
     16   <pos name='X'/>
     17   <pos name='XI'/>
     18   <pos name='adj'/>
     19   <pos name='adverb'/>
     20   <pos name='conj'/>
     21   <pos name='conjs'/>
     22   <pos name='det'/>
     23   <pos name='intj'/>
     24   <pos name='noun'/>
     25   <pos name='prefix'/>
     26   <pos name='prep'/>
     27   <pos name='pronoun'/>
     28   <pos name='verb'/>
     * @author rswanson
     *
     */
    public static enum PartOfSpeech {
        NOUN                ("noun"),       // Nouns
        VERB                ("verb"),       // Verbs
        ADJECTIVE           ("adj"),        // Adjectives
        ADVERB              ("adverb"),     // Adverbs (including pred?)
        PRONOUN             ("pronoun"),    // Pronouns (including PRON)
        CONJ                ("conj"),       // Conjunctions
        CONJS               ("conjs"),      // Subordinating conjunctions
        DETERMINER          ("det"),        // Determiners
        INTERJECTION        ("intj"),       // Interjections
        PREFIX              ("prefix"),     // Prefixes (e.g., cardio, aero, amino, avant)     
        PREPOSITION         ("prep"),       // Prepositions
        GN                  ("gn"),         // Collocations separated by an 'and'
        NA                  ("na"),         // Unknown words?
        PARTICLE            ("part"),       // Particles
        OTHER               ("other"),
        ;
        
        private final String name;
        private static final Map<String, PartOfSpeech> MAP = new HashMap<>();
        private PartOfSpeech(final String name) {
            this.name = name;
        }
        
        static {
            // OntoNotes and WordNet values
            for (PartOfSpeech pos : PartOfSpeech.values()) { MAP.put(pos.name, pos); }
            MAP.put("GN", GN);
            MAP.put("NA", NA);
            MAP.put("PART", PARTICLE);
            MAP.put("PRED", ADVERB);
            MAP.put("PRON", PRONOUN);
        }
        
        /**
         * Gets the String version of the part of speech.
         * 
         * @return the name
         */
        public String getName() {
            return name;
        }
        
        /**
         * Gets the {@link PartOfSpeech} from the String representation.
         * Unknown part-of-speech tags are mapped to {@link #OTHER}.
         * 
         * @param name The human readable string name of the tag.
         * @return The {@link PartOfSpeech}.
         */
        public static PartOfSpeech fromName(String name) {
            PartOfSpeech pos = MAP.get(name);
            
            return pos == null ? OTHER : pos;
        }
    }

    //-----------------------------------------------------------------------//
    //-- Static Fields ------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Static Methods -----------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Managed Properties -------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Properties ---------------------------------------------------------//
    //-----------------------------------------------------------------------//
    //-- Stative ------------------------------------------------------------//
    private String lemma;
    private PartOfSpeech partOfSpeech;
    private Map<String, String> features;
    private List<DelaDerivation> derivations;

    //-- Functional ---------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Initialization -----------------------------------------------------//
    //-----------------------------------------------------------------------//
    /**
     * 
     */
    public DelaEntry() {
        this.features = new HashMap<>();
        this.derivations = new ArrayList<>();
    }

    //-----------------------------------------------------------------------//
    //-- Getters & Setters --------------------------------------------------//
    //-----------------------------------------------------------------------//
    /**
     * @return the derivations
     */
    public List<DelaDerivation> getDerivations() {
        return derivations;
    }
    
    /**
     * @param derivations the derivations to set
     */
    public void setDerivations(List<DelaDerivation> derivations) {
        this.derivations = derivations;
    }
    
    /**
     * 
     * @return
     */
    public DelaDerivation getLastDerivation() {
        Preconditions.checkState(!derivations.isEmpty());
        
        return derivations.get(derivations.size() - 1);
    }
    
    /**
     * 
     * @param name
     * @param value
     */
    public void setFeature(String name, String value) {
        this.features.put(name, value);
    }
    
    /**
     * 
     * @param name
     * @return
     */
    public String getFeature(String name) {
        return this.features.get(name);
    }
    
    /**
     * 
     * @return
     */
    public Set<String> getFeatures() {
        return new HashSet<>(features.keySet());
    }
    
    /**
     * @return the lemma
     */
    public String getLemma() {
        return lemma;
    }
    
    /**
     * @param lemma the lemma to set
     */
    public void setLemma(String lemma) {
        this.lemma = lemma;
    }
    
    /**
     * @return the partOfSpeech
     */
    public PartOfSpeech getPartOfSpeech() {
        return partOfSpeech;
    }
    
    /**
     * 
     * @param partOfSpeech
     */
    public void setPartOfSpeech(PartOfSpeech partOfSpeech) {
        this.partOfSpeech = partOfSpeech;
    }
    
    /**
     * @param partOfSpeech the partOfSpeech to set
     */
    public void setPartOfSpeech(String partOfSpeech) {
        this.partOfSpeech = PartOfSpeech.fromName(partOfSpeech);
    }

    //-----------------------------------------------------------------------//
    //-- Methods ------------------------------------------------------------//
    //-----------------------------------------------------------------------//
    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) { return false; }
        if (obj == this) { return true; }
        if (!(obj instanceof DelaEntry)) { return false; }
        
        DelaEntry that = (DelaEntry) obj;
        
        return
            new EqualsBuilder()
                .append(this.lemma, that.lemma)
                .append(this.partOfSpeech, that.getPartOfSpeech())
                .append(this.features, that.features)
                .isEquals();
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return 
            new HashCodeBuilder()
                .append(this.lemma)
                .append(this.partOfSpeech)
                .append(this.features)
                .toHashCode();
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return
            new GsonBuilder()
                .setPrettyPrinting()
                .create()
                .toJson(this);
    }

    //-----------------------------------------------------------------------//
    //-- Actions ------------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Utility Methods ----------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Static Utility Methods ---------------------------------------------//
    //-----------------------------------------------------------------------//

}
