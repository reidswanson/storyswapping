/**
 * 
 */
package edu.usc.ict.nld.storyswap.nn;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.deeplearning4j.models.word2vec.wordstore.VocabCache;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.DataSetPreProcessor;
import org.nd4j.linalg.factory.Nd4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;

/**
 * @author reid
 *
 */
public class OneHotDocumentFileIterator extends NarrativeIterator {
    private static final long serialVersionUID = 1L;
    static final Logger LOG = LoggerFactory.getLogger(OneHotDocumentFileIterator.class);

    //-----------------------------------------------------------------------//
    //-- Nested Classes -----------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Static Fields ------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Managed Properties -------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Properties ---------------------------------------------------------//
    //-----------------------------------------------------------------------//
    //-- Stative ------------------------------------------------------------//
    private File file;
    private FileInputStream fis;
    private GZIPInputStream cis;
    private DataInputStream dis;
    private int sequenceLength;
    private int dimensions;
    private int sentinelCount;
    private int[] document;
    
    //-- Functional ---------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Initialization -----------------------------------------------------//
    //-----------------------------------------------------------------------//
    /**
     * 
     */
    public OneHotDocumentFileIterator(File file, int sequenceLength, int defaultBatchSize) {
        this.file = file;
        this.sequenceLength = sequenceLength;
        this.defaultBatchSize = defaultBatchSize;
        this.document = null;
        
        reset();
    }

    //-----------------------------------------------------------------------//
    //-- Getters & Setters --------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Methods ------------------------------------------------------------//
    //-----------------------------------------------------------------------//
    /* (non-Javadoc)
     * @see java.util.Iterator#hasNext()
     */
    @Override
    public boolean hasNext() {
        return sentinelCount < sequenceLength;
    }

    /* (non-Javadoc)
     * @see java.util.Iterator#next()
     */
    @Override
    public DataSet next() {
        return next(defaultBatchSize);
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#next(int)
     */
    @Override
    public DataSet next(int num) {
        List<int[]> featureStack = new ArrayList<>();
        List<int[]> labelStack = new ArrayList<>();
        
        int maxLength = 0;
        synchronized (file) {
            for (int i = 0; i < num; ++i) {
                readDocument();
                
                if (document.length == 0) { break; }
                
                featureStack.add(Arrays.copyOfRange(document, 0, document.length - 1));
                labelStack.add(Arrays.copyOfRange(document, 1, document.length));
                
                maxLength = Math.max(maxLength, document.length - 1);
            }
        }
        
        INDArray features = Nd4j.zeros(featureStack.size(), inputColumns(), maxLength);
        INDArray labels = Nd4j.zeros(featureStack.size(), inputColumns(), maxLength);
        
        INDArray featuresMask = Nd4j.zeros(featureStack.size(), maxLength);
        INDArray labelsMask = Nd4j.zeros(featureStack.size(), maxLength);
        
        for (int b = 0; b < featureStack.size(); ++b) {
            for (int t = 0; t < featureStack.get(b).length; ++t) {
                int f = featureStack.get(b)[t];
                int l = labelStack.get(b)[t];
                
                //String fw = vocab.wordAtIndex(f);
                //String lw = vocab.wordAtIndex(l);
                
                //System.err.printf("%2d %4d %6d %6d %s %s%n", b, t, f, l, fw, lw);
                
                features.putScalar(b, f, t, 1.0);
                labels.putScalar(b, l, t, 1.0);
                
                featuresMask.putScalar(b, t, 1.0);
                labelsMask.putScalar(b, t, 1.0);
            }
        }
        
        //System.err.println(features.shapeInfoToString());
        return new DataSet(features, labels, featuresMask, labelsMask);
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#totalExamples()
     */
    @Override
    public int totalExamples() {
        throw new UnsupportedOperationException();
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#inputColumns()
     */
    @Override
    public int inputColumns() {
        return dimensions;
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#totalOutcomes()
     */
    @Override
    public int totalOutcomes() {
        return dimensions;
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#resetSupported()
     */
    @Override
    public boolean resetSupported() {
        return true;
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#asyncSupported()
     */
    @Override
    public boolean asyncSupported() {
        return true;
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#reset()
     */
    @Override
    public void reset() {
        // I'm being very conservative on the synchronization for now
        synchronized (file) {
            sentinelCount = 0;
            document = null;
            
            close();
            
            try {
                fis = new FileInputStream(file);
                cis = new GZIPInputStream(fis, 1<<18);
                dis = new DataInputStream(cis);
                
                dimensions = dis.readInt();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
    
    private void readDocument() {
        if (sentinelCount > 0) { return; }
        
        try {
            int n = dis.readInt();
            
            document = new int[n];
            for (int i = 0; i < n; ++i) {
                document[i] = dis.readInt();
            }
        } catch (IOException e) {
            Throwables.propagate(e);
        }
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#batch()
     */
    @Override
    public int batch() {
        return defaultBatchSize;
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#cursor()
     */
    @Override
    public int cursor() {
        throw new UnsupportedOperationException();
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#numExamples()
     */
    @Override
    public int numExamples() {
        throw new UnsupportedOperationException();
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#setPreProcessor(org.nd4j.linalg.dataset.api.DataSetPreProcessor)
     */
    @Override
    public void setPreProcessor(DataSetPreProcessor preProcessor) {
        throw new UnsupportedOperationException();
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#getPreProcessor()
     */
    @Override
    public DataSetPreProcessor getPreProcessor() {
        throw new UnsupportedOperationException();
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#getLabels()
     */
    @Override
    public List<String> getLabels() {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public void close() {
        if (fis != null) {
            try {
                fis.close();
            } catch (IOException e) {
                // ignore
            } finally {
                fis = null;
            }
        }
        
        if (cis != null) {
            try {
                cis.close();
            } catch (IOException e) {
                // ignore
            } finally {
                cis = null;
            }
        }
        
        if (dis != null) {
            try {
                dis.close();
            } catch (IOException e) {
                // ignore
            } finally {
                dis = null;
            }
        }
    }

    //-----------------------------------------------------------------------//
    //-- Actions ------------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Utility Methods ----------------------------------------------------//
    //-----------------------------------------------------------------------//

}
