/**
 * 
 */
package edu.usc.ict.nld.storyswap.io;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.compress.compressors.gzip.GzipUtils;
import org.deeplearning4j.models.embeddings.inmemory.InMemoryLookupTable;
import org.deeplearning4j.models.embeddings.learning.impl.elements.SkipGram;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.word2vec.VocabWord;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.models.word2vec.wordstore.VocabCache;
import org.deeplearning4j.models.word2vec.wordstore.inmemory.AbstractCache;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author rswanson
 *
 */
public final class SerializationUtils {
    static final Logger LOG = LoggerFactory.getLogger(SerializationUtils.class);

    //-----------------------------------------------------------------------//
    //-- Nested Classes -----------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Static Fields ------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Managed Properties -------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Properties ---------------------------------------------------------//
    //-----------------------------------------------------------------------//
    //-- Stative ------------------------------------------------------------//

    //-- Functional ---------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Initialization -----------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Getters & Setters --------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Methods ------------------------------------------------------------//
    //-----------------------------------------------------------------------//
    public static final void writeCompressedBinary(Word2Vec w2v, File file) {
        try (
            FileOutputStream fos = new FileOutputStream(file);
            GZIPOutputStream gos = new GZIPOutputStream(fos, 1 << 18);
            DataOutputStream dos = new DataOutputStream(gos);
        ) {
            VocabCache<VocabWord> vocab = w2v.vocab();
            
            dos.write(Integer.toString(vocab.numWords()).getBytes(StandardCharsets.UTF_8));
            dos.write(" ".getBytes(StandardCharsets.UTF_8));
            dos.write(Integer.toString(w2v.getLayerSize()).getBytes(StandardCharsets.UTF_8));
            dos.write("\n".getBytes(StandardCharsets.UTF_8));
            
            for (String word : vocab.words()) {
                double[] vector = w2v.getWordVector(word);
                
                dos.write((word + " ").getBytes(StandardCharsets.UTF_8));
                for (int j = 0; j < vector.length; ++j) {
                    int bits = Float.floatToIntBits((float) vector[j]);
                    byte[] bytes = new byte[4];
                    bytes[0] = (byte)(bits & 0xff);
                    bytes[1] = (byte)((bits >> 8) & 0xff);
                    bytes[2] = (byte)((bits >> 16) & 0xff);
                    bytes[3] = (byte)((bits >> 24) & 0xff);
                    
                    dos.write(bytes);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    /**
     * Code adapted from {@link WordVectorSerializer}.
     * 
     * @param w2v
     * @param file
     * @return
     */
    @SuppressWarnings("deprecation")
    public static Word2Vec readCompressedBinary(File file) {
        LOG.debug("Start loading word2vec");
        InMemoryLookupTable<VocabWord> lookupTable;
        VocabCache<VocabWord> cache;
        INDArray syn0;
        int words, size;
        try (BufferedInputStream bis = new BufferedInputStream(
                GzipUtils.isCompressedFilename(file.getName())
                    ? new GZIPInputStream(new FileInputStream(file))
                    : new FileInputStream(file));
            
            DataInputStream dis = new DataInputStream(bis)) {
            words = Integer.parseInt(WordVectorSerializer.readString(dis));
            size = Integer.parseInt(WordVectorSerializer.readString(dis));
            syn0 = Nd4j.create(words, size);
            cache = new AbstractCache<>();

            lookupTable = (InMemoryLookupTable<VocabWord>) new InMemoryLookupTable.Builder<VocabWord>()
                .cache(cache)
                .useHierarchicSoftmax(false)
                .vectorLength(size).build();

            String word;
            for (int i = 0; i < words; i++) {
                word = WordVectorSerializer.readString(dis);

                float[] vector = new float[size];

                for (int j = 0; j < size; j++) {
                    vector[j] = WordVectorSerializer.readFloat(dis);
                }

                syn0.putRow(i, Nd4j.create(vector));

                VocabWord vw = new VocabWord(1.0, word);
                vw.setIndex(cache.numWords());

                cache.addToken(vw);
                cache.addWordToIndex(vw.getIndex(), vw.getLabel());

                cache.putVocabWord(word);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        lookupTable.setSyn0(syn0);

        Word2Vec ret = new Word2Vec.Builder()
                .useHierarchicSoftmax(false)
                .resetModel(false)
                .layerSize(syn0.columns())
                .allowParallelTokenization(true)
                .elementsLearningAlgorithm(new SkipGram<VocabWord>())
                .learningRate(0.025)
                .windowSize(5)
                .workers(1)
                .build();

        ret.setVocab(cache);
        ret.setLookupTable(lookupTable);

        LOG.debug("Finish loading word2vec");
        return ret;
    }

    //-----------------------------------------------------------------------//
    //-- Actions ------------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Utility Methods ----------------------------------------------------//
    //-----------------------------------------------------------------------//

}
