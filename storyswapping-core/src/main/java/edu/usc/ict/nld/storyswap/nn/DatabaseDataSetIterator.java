/**
 * 
 */
package edu.usc.ict.nld.storyswap.nn;

import static edu.stanford.nlp.process.PTBTokenizer.PTBTokenizerFactory.newCoreLabelTokenizerFactory;
import static java.lang.String.format;
import static java.sql.ResultSet.CONCUR_READ_ONLY;
import static java.sql.ResultSet.TYPE_FORWARD_ONLY;
import static org.nd4j.linalg.indexing.NDArrayIndex.all;
import static org.nd4j.linalg.indexing.NDArrayIndex.interval;
import static org.nd4j.linalg.indexing.NDArrayIndex.point;

import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.deeplearning4j.models.word2vec.Word2Vec;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.DataSetPreProcessor;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.INDArrayIndex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.process.PTBTokenizer.PTBTokenizerFactory;
import edu.stanford.nlp.process.Tokenizer;

/**
 * @author reid
 *
 */
public class DatabaseDataSetIterator implements DataSetIterator {
    private static final long serialVersionUID = 1L;
    static final Logger LOG = LoggerFactory.getLogger(DatabaseDataSetIterator.class);

    //-----------------------------------------------------------------------//
    //-- Nested Classes -----------------------------------------------------//
    //-----------------------------------------------------------------------//
    private final class Producer extends Thread {
        // TODO I'm almost positive there's still a race condition here
        /* (non-Javadoc)
         * @see java.lang.Thread#run()
         */
        @Override
        public void run() {
            try {
                while (cursor.next()) {
                    int storyId = cursor.getInt(1);
                    String s = cursor.getString(2);
                    
                    //LOG.debug("{} {}", storyId, s);
                    Tokenizer<CoreLabel> tokenizer = tokenizerFactory.getTokenizer(new StringReader(s));
                
                    if (storyId != previousStoryId) {
                        current.put(Character.toString(DOCUMENT_BOUNDARY));
                    }
                    
                    for (CoreLabel l : tokenizer.tokenize()) {
                        current.put(l.word());
                    }
                
                    
                    previousStoryId = storyId;
                    sentenceNum++;
                } 
                
                synchronized (current) {
                    hasNextSentence = false;
                }
            } catch (SQLException | InterruptedException e) {
                throw new RuntimeException(e);
            } finally {
                try {
                    cursor.close();
                    connection.close();
                    
                    cursor = null;
                    connection = null;
                } catch (Throwable t) {
                    throw new RuntimeException(t);
                }
            }
        }
    }

    //-----------------------------------------------------------------------//
    //-- Static Fields ------------------------------------------------------//
    //-----------------------------------------------------------------------//
    public static final char DOCUMENT_BOUNDARY = (char) 3;
    
    //-----------------------------------------------------------------------//
    //-- Managed Properties -------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Properties ---------------------------------------------------------//
    //-----------------------------------------------------------------------//
    //-- Stative ------------------------------------------------------------//
    protected Properties connectionProps;
    protected Connection connection;
    protected ResultSet cursor;
    protected int batchSize = 32;
    protected int sequenceLength = 100;
    protected int previousStoryId = 0;
    protected int sentenceNum = 0;
    protected ArrayBlockingQueue<String> current;
    protected Deque<INDArray> currentFeatureSequence;
    protected Deque<INDArray> currentLabelSequence;
    protected boolean hasNextSentence = false;
    protected PTBTokenizerFactory<CoreLabel> tokenizerFactory;
    protected Word2Vec w2v;
    protected INDArray[] characterArrays;
    

    //-- Functional ---------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Initialization -----------------------------------------------------//
    //-----------------------------------------------------------------------//
    
    /**
     * 
     */
    public DatabaseDataSetIterator() {
        
    }
    
    /**
     * 
     * @param connectionProps
     * @param w2v
     * @param batchSize
     */
    public DatabaseDataSetIterator(Properties connectionProps, Word2Vec w2v, int sequenceLength, int batchSize) {
        Preconditions.checkNotNull(connectionProps);
        Preconditions.checkNotNull(w2v);
        Preconditions.checkArgument(w2v.getLayerSize() >= getDefaultCharacterSet().length);
        this.connectionProps = connectionProps;
        this.w2v = w2v;
        this.batchSize = batchSize;
        this.current = new ArrayBlockingQueue<>(1000000);
        this.currentFeatureSequence = new ArrayDeque<>();
        this.currentLabelSequence = new ArrayDeque<>();
        this.tokenizerFactory = newCoreLabelTokenizerFactory("");
        this.characterArrays = new INDArray[getDefaultCharacterSet().length];
        for (int i = 0; i < getDefaultCharacterSet().length; ++i) {
            INDArray array = Nd4j.zeros(w2v.getLayerSize());
            array.putScalar((int) getDefaultCharacterSet()[i], 1.0);
            characterArrays[i] = array;
        }
        
        reset();
    }

    //-----------------------------------------------------------------------//
    //-- Getters & Setters --------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Methods ------------------------------------------------------------//
    //-----------------------------------------------------------------------//

    /**
     * The standard printable ASCII characters
     * @return
     */
    public static char[] getDefaultCharacterSet(){
        char[] validChars = new char[161];
        
        for (int i = 0; i < 161; ++i) {
            validChars[i] = (char) i;
        }

        return validChars;
    }

    
    /* (non-Javadoc)
     * @see java.util.Iterator#hasNext()
     */
    @Override
    public boolean hasNext() {
        synchronized (current) {
            return hasNextSentence || !current.isEmpty();
        }
    }

    /* (non-Javadoc)
     * @see java.util.Iterator#next()
     */
    @Override
    public DataSet next() {
        return next(batchSize);
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#asyncSupported()
     */
    @Override
    public boolean asyncSupported() {
        return false;
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#batch()
     */
    @Override
    public int batch() {
        return batchSize;
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#cursor()
     */
    @Override
    public int cursor() {
        return sentenceNum;
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#getLabels()
     */
    @Override
    public List<String> getLabels() {
        throw new UnsupportedOperationException();
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#getPreProcessor()
     */
    @Override
    public DataSetPreProcessor getPreProcessor() {
        throw new UnsupportedOperationException();
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#inputColumns()
     */
    @Override
    public int inputColumns() {
        return w2v.getLayerSize();
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#next(int)
     */
    @Override
    public DataSet next(int miniBatch) {
        INDArray features = Nd4j.create(miniBatch, inputColumns(), sequenceLength);
        INDArray labels = Nd4j.create(miniBatch, inputColumns(), sequenceLength);
        
        for (int i = 0; i < miniBatch; ++i) {
            String featureWord = nextToken();
            String labelWord = peekToken();
            
            //if (featureWord == null) { break; }
            if (featureWord == null) { featureWord = Character.toString(DOCUMENT_BOUNDARY); }
            
            currentFeatureSequence.pop();
            currentLabelSequence.pop();
            
            INDArray featureVector = toArray(featureWord);
            INDArray labelVector = toArray(labelWord);
            
            currentFeatureSequence.add(featureVector);
            currentLabelSequence.add(labelVector);
            
            features.put(new INDArrayIndex[] { point(i),  all(), all() }, Nd4j.vstack(currentFeatureSequence).transpose());
            labels.put(new INDArrayIndex[] { point(i),  all(), all() }, Nd4j.vstack(currentLabelSequence).transpose());
            //if (labelWord.charAt(0) == DOCUMENT_BOUNDARY && !hasNextSentence) { break; }
        }
        
//        System.err.println(features.shapeInfoToString());
//        System.err.println(features);
        
        return new DataSet(features, labels);
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#numExamples()
     */
    @Override
    public int numExamples() {
        throw new UnsupportedOperationException();
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#reset()
     */
    @Override
    public void reset() {
        // reset the iterator state
        sentenceNum = 0;
        hasNextSentence = true;
        current.clear();
        currentFeatureSequence.clear();
        currentLabelSequence.clear();
        
        // Construct database connection and query
        // TODO move this somewhere else
        String uri = 
            String.format(
                "jdbc:mysql://%s:%s/%s?autoReconnect=true&netTimeoutForStreamingResults=6000000&user=%s&password=%s", 
                connectionProps.getProperty("host", "localhost"), 
                connectionProps.getProperty("port", "3306"), 
                connectionProps.getProperty("database"), 
                connectionProps.getProperty("username"), 
                connectionProps.getProperty("password")
            );
        String limit = connectionProps.getProperty("limit", "0").equals("0") ? "" : String.format("limit %s", connectionProps.getProperty("limit", "10"));
        String sql = format(
            "select story_id, sentence_text from sentence where is_spam is NULL and is_duplicate is NULL order by story_id, sentence_num %s",
            limit
        );

        PreparedStatement stmt = null;        
        try {
            // Connect to the database making sure the result is streamed
            connection = DriverManager.getConnection(uri);
            stmt = connection.prepareStatement(sql, TYPE_FORWARD_ONLY, CONCUR_READ_ONLY);
            stmt.setFetchSize(Integer.MIN_VALUE);
            
            // Execute the query
            cursor = stmt.executeQuery();
        } catch (SQLException e) {
            LOG.error("Error getting sentences from the database", e);
            current.clear();
        }
        
        // Get the next batch of tokens
        new Producer().start();
        
        while (currentFeatureSequence.size() < sequenceLength) {
            currentFeatureSequence.add(toArray(nextToken()));
            currentLabelSequence.add(toArray(peekToken()));
        }
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#resetSupported()
     */
    @Override
    public boolean resetSupported() {
        return true;
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#setPreProcessor(org.nd4j.linalg.dataset.api.DataSetPreProcessor)
     */
    @Override
    public void setPreProcessor(DataSetPreProcessor arg0) {
        throw new UnsupportedOperationException();
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#totalExamples()
     */
    @Override
    public int totalExamples() {
        throw new UnsupportedOperationException();
    }

    /* (non-Javadoc)
     * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#totalOutcomes()
     */
    @Override
    public int totalOutcomes() {
        return w2v.getLayerSize();
    }
    
    public INDArray toArray(String s) {
        if (s.length() == 1 && (int) s.charAt(0) < 161) { return characterArrays[s.charAt(0)]; }
        
        String w = s.toLowerCase().replaceAll("\\d", "0");
        if (w2v.hasWord(w)) { return w2v.getWordVectorMatrixNormalized(w); }
       
        INDArray unk = w2v.getWordVectorMatrixNormalized(w2v.getUNK());
        
        return unk;
    }
    
    public INDArray toArray(Deque<String> sequence) {
        INDArray array = Nd4j.create(inputColumns(), sequenceLength);
        
        int i = 0;
        for (String w : sequence) {
            array.put(new INDArrayIndex[] { interval(0, inputColumns()), point(i) }, toArray(w));
            
            ++i;
        }
        
        return array;
    }

    //-----------------------------------------------------------------------//
    //-- Actions ------------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Utility Methods ----------------------------------------------------//
    //-----------------------------------------------------------------------//
    /**
     * 
     * @return
     */
    protected String nextToken() {
        // If the queue is empty, fill it with tokens from the next sentence
        String nextToken = null;
        try {
            nextToken = current.poll(1000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        
        return nextToken == null ? Character.toString(DOCUMENT_BOUNDARY) : nextToken;
    }
    
    /**
     * 
     * @return
     */
    protected String peekToken() {
        return current.peek() == null ? Character.toString(DOCUMENT_BOUNDARY) : current.peek();
    }
}
