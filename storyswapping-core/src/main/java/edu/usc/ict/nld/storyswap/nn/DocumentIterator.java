/**
 * 
 */
package edu.usc.ict.nld.storyswap.nn;

import static edu.stanford.nlp.process.PTBTokenizer.PTBTokenizerFactory.newCoreLabelTokenizerFactory;
import static java.sql.ResultSet.CONCUR_READ_ONLY;
import static java.sql.ResultSet.TYPE_FORWARD_ONLY;

import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.process.PTBTokenizer.PTBTokenizerFactory;
import edu.stanford.nlp.process.Tokenizer;
import edu.usc.ict.nld.storyswap.io.DatabaseConnectionProperties;
import edu.usc.ict.nld.storyswap.io.DatabaseConnectionProperties.Key;

/**
 * @author reid
 *
 */
public class DocumentIterator implements Iterator<List<String>> {
    static final Logger LOG = LoggerFactory.getLogger(DocumentIterator.class);

    //-----------------------------------------------------------------------//
    //-- Nested Classes -----------------------------------------------------//
    //-----------------------------------------------------------------------//
    private final class QueryResult {
        public int storyId;
        public List<String> tokens;
        
        public QueryResult(int storyId, List<String> tokens) {
            this.storyId = storyId;
            this.tokens = tokens;
        }
    }
    
    private final class Producer extends Thread {
        /* (non-Javadoc)
         * @see java.lang.Runnable#run()
         */
        @Override
        public void run() {
            try {
                while (cursor.next()) {
                    int storyId = cursor.getInt(1);
                    String s = cursor.getString(2);
                    
                    Tokenizer<CoreLabel> tokenizer = tokenizerFactory.getTokenizer(new StringReader(s));
                    List<String> tokens = 
                        tokenizer.tokenize().stream()
                            .map(l -> new String(l.word()))
                            .map(w -> w.toLowerCase().replaceAll("\\d", "0"))
                            .collect(Collectors.toCollection(ArrayList::new));
                    
                    queue.putLast(new QueryResult(storyId, tokens));
                }
                
                // Add a sentinel value
                queue.putLast(new QueryResult(0, null));
            } catch (SQLException | InterruptedException e) {
                throw new RuntimeException(e);
            }
            
        }
    }

    //-----------------------------------------------------------------------//
    //-- Static Fields ------------------------------------------------------//
    //-----------------------------------------------------------------------//
    public static final String DOCUMENT_START = Character.toString((char) 2);
    public static final String DOCUMENT_END = Character.toString((char) 3);
    private static final int MAX_QUEUE_SIZE = 100000;

    //-----------------------------------------------------------------------//
    //-- Managed Properties -------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Properties ---------------------------------------------------------//
    //-----------------------------------------------------------------------//
    //-- Stative ------------------------------------------------------------//
    protected PTBTokenizerFactory<CoreLabel> tokenizerFactory;
    private Connection connection;
    private ResultSet cursor;
    private String connectionUri;
    private String query;
    private List<String> currentDocument;
    private LinkedBlockingDeque<QueryResult> queue;
    //private Deque<QueryResult> queue;
    private boolean hasNext = true;
    
    

    //-- Functional ---------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Initialization -----------------------------------------------------//
    //-----------------------------------------------------------------------//
    /**
     * 
     */
    public DocumentIterator(DatabaseConnectionProperties connectionProperties) {
        this.tokenizerFactory = newCoreLabelTokenizerFactory("");
        this.connectionUri =
            String.format(
                "jdbc:mysql://%s:%s/%s?autoReconnect=true&netTimeoutForStreamingResults=6000000&user=%s&password=%s",
                connectionProperties.value(Key.HOST),
                connectionProperties.value(Key.PORT),
                connectionProperties.value(Key.DATABASE),
                connectionProperties.value(Key.USERNAME),
                connectionProperties.value(Key.PASSWORD)
            );
        this.query =
            String.format(
                "select story_id, sentence_text from sentence where is_spam is NULL and is_duplicate is NULL order by story_id, sentence_num %s",
                connectionProperties.value(Key.LIMIT).equals("0") ? "" : "limit " + connectionProperties.value(Key.LIMIT)
            );
        
        this.currentDocument = new ArrayList<>();
        this.queue = new LinkedBlockingDeque<>(MAX_QUEUE_SIZE);
        //this.queue = new ArrayDeque<>();
        
        reset();
    }

    //-----------------------------------------------------------------------//
    //-- Getters & Setters --------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Methods ------------------------------------------------------------//
    //-----------------------------------------------------------------------//
    /* (non-Javadoc)
     * @see java.util.Iterator#hasNext()
     */
    @Override
    public boolean hasNext() {
        return currentDocument.size() > 2;
    }
    
    /*
     * (non-Javadoc)
     * @see java.util.Iterator#next()
     */
    @Override
    public List<String> next() {
        List<String> result = new ArrayList<>(currentDocument);
        
        readNext();
        
        return result;
    }
    
    public void reset() {
        close();
        
        PreparedStatement stmt = null;        
        try {
            // Connect to the database making sure the result is streamed
            connection = DriverManager.getConnection(connectionUri);
            stmt = connection.prepareStatement(query, TYPE_FORWARD_ONLY, CONCUR_READ_ONLY);
            stmt.setFetchSize(Integer.MIN_VALUE);
            
            // Execute the query
            cursor = stmt.executeQuery();
            
            new Producer().start();
            
            readNext();
        } catch (SQLException e) {
            throw new RuntimeException("Error getting sentences from the database", e);
        }
    }
    
    public void close() {
        if (cursor != null) {
            try {
                cursor.close();
                cursor = null;
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        
        if (connection != null) {
            try {
                connection.close();
                connection = null;
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    //-----------------------------------------------------------------------//
    //-- Actions ------------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Utility Methods ----------------------------------------------------//
    //-----------------------------------------------------------------------//
    private void readNext() {
        currentDocument.clear();
        currentDocument.add(DOCUMENT_START);
        
        int storyId = 0;
        QueryResult qr = null;
        
        // Hack to avoid deadlock
        peek();
        
        try {
            qr = queue.takeFirst();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } finally {
            
        }
        
        storyId = qr.storyId;
        
        if (storyId == 0 || hasNext == false) {
            currentDocument.add(DOCUMENT_END);
            hasNext = false;
            return;
        }
        
        currentDocument.addAll(qr.tokens);
        
        // This is a total hack, but easier to get working that the correct synchronization
        while (peek().storyId == storyId) {
            try {
                qr = queue.takeFirst();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            } finally {
                
            }
            
            if (qr.storyId == 0) {
                hasNext = false;
                break; 
            }
                
            currentDocument.addAll(qr.tokens);
        }
        
        currentDocument.add(DOCUMENT_END);
    }
    
    private QueryResult peek() {
        QueryResult peeked = null;
        while ((peeked = queue.peek()) == null) {
            try {
                LOG.debug("Sleeping until item is available");
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        
        return peeked;
    }
}
