/**
 * 
 */
package edu.usc.ict.nld.storyswap.nn;

import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;

/**
 * @author reid
 *
 */
public abstract class NarrativeIterator implements DataSetIterator {
    private static final long serialVersionUID = 1L;

    public static final int DEFAULT_BATCH_SIZE = 50;
    
    protected int defaultBatchSize;
    
    /**
     * @return the defaultBatchSize
     */
    public int getDefaultBatchSize() {
        return defaultBatchSize;
    }
    
    /**
     * @param defaultBatchSize the defaultBatchSize to set
     */
    public void setDefaultBatchSize(int defaultBatchSize) {
        this.defaultBatchSize = defaultBatchSize;
    }
    
    /**
     * 
     */
    public abstract void close();
}
