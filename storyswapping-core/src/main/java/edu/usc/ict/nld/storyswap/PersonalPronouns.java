/**
 * 
 */
package edu.usc.ict.nld.storyswap;

import static edu.usc.ict.nld.storyswap.PersonalPronouns.Gender.DEICTIC;
import static edu.usc.ict.nld.storyswap.PersonalPronouns.Gender.FEMININE;
import static edu.usc.ict.nld.storyswap.PersonalPronouns.Gender.MASCULINE;
import static edu.usc.ict.nld.storyswap.PersonalPronouns.Gender.NEUTER;
import static edu.usc.ict.nld.storyswap.PersonalPronouns.Number.PLURAL;
import static edu.usc.ict.nld.storyswap.PersonalPronouns.Number.SINGULAR;
import static edu.usc.ict.nld.storyswap.PersonalPronouns.Person.FIRST;
import static edu.usc.ict.nld.storyswap.PersonalPronouns.Person.SECOND;
import static edu.usc.ict.nld.storyswap.PersonalPronouns.Person.THIRD;
import static edu.usc.ict.nld.storyswap.PersonalPronouns.Role.DEPENDENT_POSSESSIVE;
import static edu.usc.ict.nld.storyswap.PersonalPronouns.Role.INDEPENDENT_POSSESSIVE;
import static edu.usc.ict.nld.storyswap.PersonalPronouns.Role.OBJECT;
import static edu.usc.ict.nld.storyswap.PersonalPronouns.Role.REFLEXIVE;
import static edu.usc.ict.nld.storyswap.PersonalPronouns.Role.SUBJECT;
import static java.util.Arrays.asList;
import static java.util.Collections.unmodifiableList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author reid
 *
 */
public class PersonalPronouns {
    //-----------------------------------------------------------------------//
    //-- Nested Classes -----------------------------------------------------//
    //-----------------------------------------------------------------------//
    public enum Role {
        SUBJECT,
        OBJECT,
        DEPENDENT_POSSESSIVE,
        INDEPENDENT_POSSESSIVE,
        REFLEXIVE,
        ;
    }
    
    public enum Person {
        FIRST,
        SECOND,
        THIRD,
        ;
    }
    
    public enum Number {
        SINGULAR,
        PLURAL,
        ;
    }
    
    public enum Gender {
        MASCULINE,
        FEMININE,
        NEUTER,
        EPICENE,
        DEICTIC,
        ;
    }
    
    public enum Pronoun {
        I("i", SUBJECT, FIRST, SINGULAR, DEICTIC),
        ME("me", OBJECT, FIRST, SINGULAR, DEICTIC),
        MY("my", DEPENDENT_POSSESSIVE, FIRST, SINGULAR, DEICTIC),
        MINE("mine", INDEPENDENT_POSSESSIVE, FIRST, SINGULAR, DEICTIC),
        MYSELF("myself", REFLEXIVE, FIRST, SINGULAR, DEICTIC),
        
        WE("we", SUBJECT, FIRST, PLURAL, DEICTIC),
        US("us", OBJECT, FIRST, PLURAL, DEICTIC),
        OUR("our", DEPENDENT_POSSESSIVE, FIRST, PLURAL, DEICTIC),
        OURS("ours", INDEPENDENT_POSSESSIVE, FIRST, PLURAL, DEICTIC),
        OURSELVES("ourselves", REFLEXIVE, FIRST, PLURAL, DEICTIC),
        
        YOU_S_S1("you", SUBJECT, SECOND, SINGULAR, DEICTIC),
        YOU_S_O1("you", OBJECT, SECOND, SINGULAR, DEICTIC),
        YOUR_S("your", DEPENDENT_POSSESSIVE, SECOND, SINGULAR, DEICTIC),
        YOURS_S("yours", INDEPENDENT_POSSESSIVE, SECOND, SINGULAR, DEICTIC),
        YOURSELF("yourself", REFLEXIVE, SECOND, SINGULAR, DEICTIC),
        
        YOU_P_S1("you", SUBJECT, SECOND, PLURAL, DEICTIC),
        YOU_P_O1("you", OBJECT, SECOND, PLURAL, DEICTIC),
        YOUR_P("your", DEPENDENT_POSSESSIVE, SECOND, PLURAL, DEICTIC),
        YOURS_P("yours", INDEPENDENT_POSSESSIVE, SECOND, PLURAL, DEICTIC),
        YOURSELVES("yourselves", REFLEXIVE, SECOND, PLURAL, DEICTIC),
        
        HE("he", SUBJECT, THIRD, SINGULAR, MASCULINE),
        HIM("him", OBJECT, THIRD, SINGULAR, MASCULINE),
        HIS_D("his", DEPENDENT_POSSESSIVE, THIRD, SINGULAR, MASCULINE),
        HIS_I("his", INDEPENDENT_POSSESSIVE, THIRD, SINGULAR, MASCULINE),
        HIMSELF("himself", REFLEXIVE, THIRD, SINGULAR, MASCULINE),
        
        SHE("she", SUBJECT, THIRD, SINGULAR, FEMININE),
        HER_O("her", OBJECT, THIRD, SINGULAR, FEMININE),
        HER_D("her", DEPENDENT_POSSESSIVE, THIRD, SINGULAR, FEMININE),
        HERS("hers", INDEPENDENT_POSSESSIVE, THIRD, SINGULAR, FEMININE),
        HERSELF("herself", REFLEXIVE, THIRD, SINGULAR, FEMININE),
        
        IT_S("it", SUBJECT, THIRD, SINGULAR, NEUTER),
        IT_O("it", OBJECT, THIRD, SINGULAR, NEUTER),
        ITS_D("its", DEPENDENT_POSSESSIVE, THIRD, SINGULAR, NEUTER),
        ITS_O("its", INDEPENDENT_POSSESSIVE, THIRD, SINGULAR, NEUTER),
        ITSELF("itself", REFLEXIVE, THIRD, SINGULAR, NEUTER),
        
        THEY_P("they", SUBJECT, THIRD, PLURAL, DEICTIC),
        THEM_P("them", OBJECT, THIRD, PLURAL, DEICTIC),
        THEIR_P("their", DEPENDENT_POSSESSIVE, THIRD, PLURAL, DEICTIC),
        THEIRS_P("theirs", INDEPENDENT_POSSESSIVE, THIRD, PLURAL, DEICTIC),
        THEMSELVES_P("themselves", REFLEXIVE, THIRD, PLURAL, DEICTIC),
        
        THEY_S("they", SUBJECT, THIRD, SINGULAR, DEICTIC),
        THEM_S("them", OBJECT, THIRD, SINGULAR, DEICTIC),
        THEIR_S("their", DEPENDENT_POSSESSIVE, THIRD, SINGULAR, DEICTIC),
        THEIRS_S("theirs", INDEPENDENT_POSSESSIVE, THIRD, SINGULAR, DEICTIC),
        THEMSELVES_S("themselves", REFLEXIVE, THIRD, SINGULAR, DEICTIC),
        ;
        
        public final String pronoun;
        public final Role role;
        public final Person person;
        public final Number number;
        public final Gender gender;
        
        private static final Map<String, Pronoun> singleLookup = new HashMap<>();
        private static final Map<String, List<Pronoun>> multipleLookup = new HashMap<>();
        private static final Map<List<Object>, String> propertiesLookup = new HashMap<>();
        
        /**
         * 
         * @param pronoun
         * @param role
         * @param person
         * @param number
         * @param gender
         */
        private Pronoun(String pronoun, Role role, Person person, Number number, Gender gender) {
            this.pronoun = pronoun;
            this.role = role;
            this.person = person;
            this.number = number;
            this.gender = gender;
        }
        
        /**
         * 
         * @param pronoun
         * @return
         */
        public static Pronoun lookup(String pronoun) {
            return singleLookup.get(pronoun.toLowerCase());
        }
        
        /**
         * 
         * @param pronoun
         * @return
         */
        public static List<Pronoun> lookupMultiple(String pronoun) {
            return multipleLookup.get(pronoun.toLowerCase());
        }
        
        /**
         * 
         * @param role
         * @param person
         * @param number
         * @param gender
         * @return
         */
        public static String from(Role role, Person person, Number number, Gender gender) {
            List<Object> key = unmodifiableList(asList(role, person, number, gender));
            
            return propertiesLookup.get(key);
        }
        
        static {
            for (Pronoun p : values()) {
                if (!singleLookup.containsKey(p.pronoun)) {
                    singleLookup.put(p.pronoun, p);
                }
                
                multipleLookup
                    .computeIfAbsent(p.pronoun, k -> new ArrayList<>())
                    .add(p);
                
                List<Object> key = unmodifiableList(asList(p.role, p.person, p.number, p.gender));
                propertiesLookup.put(key, p.pronoun);
            }
        }
    }

    //-----------------------------------------------------------------------//
    //-- Static Fields ------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Managed Properties -------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Properties ---------------------------------------------------------//
    //-----------------------------------------------------------------------//
    //-- Stative ------------------------------------------------------------//
    

    //-- Functional ---------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Initialization -----------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Getters & Setters --------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Methods ------------------------------------------------------------//
    //-----------------------------------------------------------------------//
    /**
     * 
     * @param pronoun
     * @return
     */
    public static boolean isPronoun(String pronoun) {
        Pronoun p = Pronoun.lookup(pronoun.toLowerCase());
        
        return p != null;
    }
    
    /**
     * 
     * @param pronoun
     * @return
     */
    public static Role role(final String pronoun) {
        Pronoun p = Pronoun.lookup(pronoun.toLowerCase());
        
        return p == null ? null : p.role;
    }
    
    /**
     * 
     * @param pronoun
     * @return
     */
    public static Person person(final String pronoun) {
        Pronoun p = Pronoun.lookup(pronoun.toLowerCase());
        
        return p == null ? null : p.person;
    }
    
    /**
     * 
     * @param pronoun
     * @return
     */
    public static Number number(final String pronoun) {
        Pronoun p = Pronoun.lookup(pronoun.toLowerCase());
        
        return p == null ? null : p.number;
    }
    
    /**
     * 
     * @param pronoun
     * @return
     */
    public static Gender gender(final String pronoun) {
        Pronoun p = Pronoun.lookup(pronoun.toLowerCase());
        
        return p == null ? null : p.gender;
    }

    //-----------------------------------------------------------------------//
    //-- Actions ------------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Utility Methods ----------------------------------------------------//
    //-----------------------------------------------------------------------//

}
