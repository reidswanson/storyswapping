/**
 * 
 */
package edu.usc.ict.nld.storyswap.nn;

import org.deeplearning4j.text.tokenization.tokenizer.TokenPreProcess;

/**
 * @author rswanson
 *
 */
public class StandardTokenPreprocess implements TokenPreProcess {
    /* (non-Javadoc)
     * @see org.deeplearning4j.text.tokenization.tokenizer.TokenPreProcess#preProcess(java.lang.String)
     */
    @Override
    public String preProcess(String token) {
        return new String(token.toLowerCase().replaceAll("\\d", "0"));
    }
}
