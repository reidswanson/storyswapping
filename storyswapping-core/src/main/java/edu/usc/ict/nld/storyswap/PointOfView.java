/**
 * 
 */
package edu.usc.ict.nld.storyswap;

import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * @author reid
 *
 */
public final class PointOfView {
    public final PersonalPronouns.Number number;
    public final PersonalPronouns.Person person;
    public final PersonalPronouns.Gender gender;
    
    /**
     * 
     * @param number
     * @param person
     */
    public PointOfView(final PersonalPronouns.Number number, final PersonalPronouns.Person person, final PersonalPronouns.Gender gender) {
        this.number = number;
        this.person = person;
        this.gender = gender;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == this) { return true; }
        if (obj == null) { return false; }
        if (!(obj instanceof PointOfView)) { return false; }
        
        PointOfView that = (PointOfView) obj;
        
        return this.number == that.number && this.person == that.person && this.gender == that.gender;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return
            new HashCodeBuilder()
                .append(this.number)
                .append(this.person)
                .append(this.gender)
                .toHashCode();
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "POV(" + person + ", " + number + ", " + gender + ")";
    }
}
