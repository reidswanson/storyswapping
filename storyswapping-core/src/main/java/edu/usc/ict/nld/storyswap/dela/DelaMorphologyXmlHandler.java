/**
 * 
 */
package edu.usc.ict.nld.storyswap.dela;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * @author rswanson
 *
 */
public class DelaMorphologyXmlHandler extends DefaultHandler {
    private static final Logger LOG = LoggerFactory.getLogger(DelaMorphologyXmlHandler.class);

    //-----------------------------------------------------------------------//
    //-- Nested Classes -----------------------------------------------------//
    //-----------------------------------------------------------------------//
    public static enum XmlIdentifier {
        // Tag names
        DICO                ("dico"),
        ENTRY               ("entry"),
        LEMMA               ("lemma"),
        POS                 ("pos"),
        FEAT                ("feat"),
        INFLECTED           ("inflected"),
        FORM                ("form"),
        
        // Attributes
        NAME                ("name"),
        VALUE               ("value"),
        ;
        
        private static final Map<String, XmlIdentifier> MAP = new ConcurrentHashMap<>();
        private final String name;
        private XmlIdentifier(String name) {
            this.name = name;
        }

        static {
            for (XmlIdentifier t : XmlIdentifier.values()) { MAP.put(t.getName(), t); }
        }

        /**
         * Gets the name of the XML tag.
         * 
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * Gets a {@link XmlIdentifier} object from the name.
         * 
         * @param name The XML String name of the Tag.
         * @return The Tag object or {@code null} if there is no tag with the name.
         */
        public static XmlIdentifier fromName(String name) {
            return MAP.get(name);
        }
    }

    //-----------------------------------------------------------------------//
    //-- Static Fields ------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Static Methods -----------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Managed Properties -------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Properties ---------------------------------------------------------//
    //-----------------------------------------------------------------------//
    //-- Stative ------------------------------------------------------------//
    private StringBuilder buffer = new StringBuilder();
    private Deque<DelaEntry> entries = new ArrayDeque<>();
    private boolean isInDerivation = false;

    //-- Functional ---------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Initialization -----------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Getters & Setters --------------------------------------------------//
    //-----------------------------------------------------------------------//
    /**
     * @return the entries
     */
    public Deque<DelaEntry> getEntries() {
        return entries;
    }
    

    //-----------------------------------------------------------------------//
    //-- Methods ------------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Actions ------------------------------------------------------------//
    //-----------------------------------------------------------------------//
    /* (non-Javadoc)
     * @see org.xml.sax.helpers.DefaultHandler#startElement(java.lang.String, java.lang.String, java.lang.String, org.xml.sax.Attributes)
     */
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        XmlIdentifier tag = XmlIdentifier.fromName(qName);
        if (tag == null) {
            throw new SAXException("Unknown tag: " + qName);
        }
        
        buffer = new StringBuilder();
        
        switch (tag) {
        case ENTRY:
            startEntry(attributes);
            break;
            
        case POS:
            startPos(attributes);
            break;
            
        case FEAT:
            startFeat(attributes);
            break;
            
        case INFLECTED:
            startInflected(attributes);
            break;
            
        default:
            break;
        }
    }
    
    /* (non-Javadoc)
     * @see org.xml.sax.helpers.DefaultHandler#endElement(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        XmlIdentifier tag = XmlIdentifier.fromName(qName);
        if (tag == null) {
            throw new SAXException("Unknown tag: " + qName);
        }
        
        switch (tag) {
        case LEMMA:
            endLemma();
            break;
            
        case FORM:
            endForm();
            break;
        
        default:
            break;
        }
    }
    
    /* (non-Javadoc)
     * @see org.xml.sax.helpers.DefaultHandler#characters(char[], int, int)
     */
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        buffer.append(new String(ch, start, length));
    }

    //-----------------------------------------------------------------------//
    //-- Utility Methods ----------------------------------------------------//
    //-----------------------------------------------------------------------//
    private void startEntry(Attributes attributes) {
        if (attributes.getLength() > 0) {
            LOG.warn("Unknown attributes for <entry> tag: {}" , attributes);
        }
        entries.addLast(new DelaEntry());
        
        isInDerivation = false;
    }
    
    private void startPos(Attributes attributes) {
        if (attributes.getLength() > 1) {
            LOG.warn("Unknown attributes for <pos> tag: {}" , attributes);
        }
        
        String name = attributes.getValue(XmlIdentifier.NAME.getName());
        entries.getLast().setPartOfSpeech(name);
    }
    
    private void startFeat(Attributes attributes) {
        if (attributes.getLength() > 2) {
            LOG.warn("Unknown attributes for <feat> tag: {}" , attributes);
        }
        
        String name = attributes.getValue(XmlIdentifier.NAME.getName());
        String value = attributes.getValue(XmlIdentifier.VALUE.getName());
        
        DelaEntry entry = entries.getLast();
        if (isInDerivation) {
            entry.getLastDerivation().setFeature(name, value);
        } else {
            entry.setFeature(name, value);
        }
    }
    
    private void startInflected(Attributes attributes) {
        if (attributes.getLength() > 0) {
            LOG.warn("Unknown attributes for <inflected> tag: {}" , attributes);
        }
        
        entries.getLast().getDerivations().add(new DelaDerivation());
        
        isInDerivation = true; 
    }
    
    private void endLemma() {
        entries.getLast().setLemma(unescape(buffer));
    }
    
    private void endForm() {
        entries.getLast().getLastDerivation().setForm(unescape(buffer));
    }

    //-----------------------------------------------------------------------//
    //-- Static Utility Methods ---------------------------------------------//
    //-----------------------------------------------------------------------//
    private static String unescape(StringBuilder builder) {
        return builder.toString().trim().replaceAll("\\\\-", "-");
    }

}
