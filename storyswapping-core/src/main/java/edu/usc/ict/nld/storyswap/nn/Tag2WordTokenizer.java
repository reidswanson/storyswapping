/**
 * 
 */
package edu.usc.ict.nld.storyswap.nn;

import static edu.usc.ict.nld.storyswap.nn.DocumentIterator.DOCUMENT_END;
import static edu.usc.ict.nld.storyswap.nn.DocumentIterator.DOCUMENT_START;
import static java.util.stream.Collectors.toCollection;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.deeplearning4j.models.word2vec.wordstore.VocabCache;
import org.deeplearning4j.text.tokenization.tokenizer.TokenPreProcess;
import org.deeplearning4j.text.tokenization.tokenizer.Tokenizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.twelvemonkeys.lang.StringUtil;

/**
 * @author rswanson
 *
 */
public class Tag2WordTokenizer implements Tokenizer {
    static final Logger LOG = LoggerFactory.getLogger(Tag2WordTokenizer.class);

    //-----------------------------------------------------------------------//
    //-- Nested Classes -----------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Static Fields ------------------------------------------------------//
    //-----------------------------------------------------------------------//
    private static final Set<String> TAG_SET = new HashSet<>();

    //-----------------------------------------------------------------------//
    //-- Managed Properties -------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Properties ---------------------------------------------------------//
    //-----------------------------------------------------------------------//
    //-- Stative ------------------------------------------------------------//
    private String unk;
    private final Deque<String> queue;
    private TokenPreProcess tokenPreProcess;

    //-- Functional ---------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Initialization -----------------------------------------------------//
    //-----------------------------------------------------------------------//
    /**
     * 
     * @param tokens
     * @param tokenPreProcessor
     */
    public Tag2WordTokenizer(List<String> tokens, TokenPreProcess tokenPreProcessor, VocabCache<?> vocab, String unk) {
        Preconditions.checkNotNull(tokens);
        Preconditions.checkNotNull(tokenPreProcessor);
        
        this.unk = unk;
        this.tokenPreProcess = tokenPreProcessor;
        this.queue = 
            tokens.stream()
                .map(t -> toPair(t))
                .map(t -> vocab == null ? t[0] : vocab.hasToken(t[0]) ? t[0] : t[1])
                .map(w -> replace(w))   // remove non printable characters
                .map(w -> filter(w))    // replace empty strings with UNK
                .collect(toCollection(ArrayDeque::new));
    }
    
    private String replace(String w) {
        if (w.equals(DocumentIterator.DOCUMENT_START) || w.equals(DocumentIterator.DOCUMENT_END)) { return w; }
        
        return w.replaceAll("\\p{C}", "");
    }
    
    private String filter(String w) {
        if (w.equals(DocumentIterator.DOCUMENT_START) || w.equals(DocumentIterator.DOCUMENT_END)) { return w; }
        
        return StringUtil.isEmpty(w) ? unk : w;
    }
    
    private String[] toPair(String t) {
        String[] result = new String[2];
        
        int idx = t.lastIndexOf('_');
        
        if (t.charAt(0) == DOCUMENT_START.charAt(0) || t.charAt(0) == DOCUMENT_END.charAt(0)) {
            result[0] = t;
            result[1] = unk;
        } else if (idx < 0) {
            result[0] = tokenPreProcess.preProcess(t);
            result[1] = unk;
        } else {
            result[0] = tokenPreProcess.preProcess(t.substring(0, idx));
            result[1] = tokenPreProcess.preProcess("<" + t.substring(idx + 1) + ">");
            TAG_SET.add(result[1]);
        }
        
        return result;
    }

    //-----------------------------------------------------------------------//
    //-- Getters & Setters --------------------------------------------------//
    //-----------------------------------------------------------------------//
    /**
     * 
     * @return
     */
    public static List<String> getTagSet() {
        return Collections.unmodifiableList(new ArrayList<>(TAG_SET));
    }

    //-----------------------------------------------------------------------//
    //-- Methods ------------------------------------------------------------//
    //-----------------------------------------------------------------------//
    /* (non-Javadoc)
     * @see org.deeplearning4j.text.tokenization.tokenizer.Tokenizer#hasMoreTokens()
     */
    @Override
    public boolean hasMoreTokens() {
        return !queue.isEmpty();
    }

    /* (non-Javadoc)
     * @see org.deeplearning4j.text.tokenization.tokenizer.Tokenizer#countTokens()
     */
    @Override
    public int countTokens() {
        return queue.size();
    }

    /* (non-Javadoc)
     * @see org.deeplearning4j.text.tokenization.tokenizer.Tokenizer#nextToken()
     */
    @Override
    public String nextToken() {
        return queue.pop();
    }

    /* (non-Javadoc)
     * @see org.deeplearning4j.text.tokenization.tokenizer.Tokenizer#getTokens()
     */
    @Override
    public List<String> getTokens() {
        List<String> remaining = new ArrayList<>();
        while (hasMoreTokens()) { remaining.add(nextToken()); }
        
        return remaining;
    }

    /* (non-Javadoc)
     * @see org.deeplearning4j.text.tokenization.tokenizer.Tokenizer#setTokenPreProcessor(org.deeplearning4j.text.tokenization.tokenizer.TokenPreProcess)
     */
    @Override
    public void setTokenPreProcessor(TokenPreProcess tokenPreProcessor) {
        this.tokenPreProcess = tokenPreProcessor;
    }


    //-----------------------------------------------------------------------//
    //-- Actions ------------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Utility Methods ----------------------------------------------------//
    //-----------------------------------------------------------------------//

}
