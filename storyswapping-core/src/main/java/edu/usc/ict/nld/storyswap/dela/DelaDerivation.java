/**
 * 
 */
package edu.usc.ict.nld.storyswap.dela;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.google.gson.GsonBuilder;

/**
 * @author rswanson
 *
 */
public class DelaDerivation implements Serializable {
    private static final long serialVersionUID = 1L;

    //-----------------------------------------------------------------------//
    //-- Nested Classes -----------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Static Fields ------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Static Methods -----------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Managed Properties -------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Properties ---------------------------------------------------------//
    //-----------------------------------------------------------------------//
    //-- Stative ------------------------------------------------------------//
    private String form;
    private Map<String, String> features;

    //-- Functional ---------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Initialization -----------------------------------------------------//
    //-----------------------------------------------------------------------//
    /**
     * 
     */
    public DelaDerivation() {
        this.features = new HashMap<>();
    }

    //-----------------------------------------------------------------------//
    //-- Getters & Setters --------------------------------------------------//
    //-----------------------------------------------------------------------//
    /**
     * 
     * @param name
     * @param value
     */
    public void setFeature(String name, String value) {
        features.put(name, value);
    }
    
    /**
     * 
     * @param name
     * @return
     */
    public String getFeature(String name) {
        return features.get(name);
    }
    
    /**
     * 
     * @return
     */
    public Set<String> getFeatures() {
        return new HashSet<>(features.keySet());
    }
    
    /**
     * @return the form
     */
    public String getForm() {
        return form;
    }
    
    /**
     * @param form the form to set
     */
    public void setForm(String form) {
        this.form = form;
    }

    //-----------------------------------------------------------------------//
    //-- Methods ------------------------------------------------------------//
    //-----------------------------------------------------------------------//
    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) { return false; }
        if (obj == this) { return true; }
        if (!(obj instanceof DelaDerivation)) { return false; }
        
        DelaDerivation that = (DelaDerivation) obj;
        
        return
            new EqualsBuilder()
                .append(this.form, that.form)
                .append(this.features, that.features)
                .isEquals();
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return
            new HashCodeBuilder()
                .append(this.form)
                .append(this.features)
                .toHashCode();
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return
            new GsonBuilder()
                .setPrettyPrinting()
                .create()
                .toJson(this);
    }

    //-----------------------------------------------------------------------//
    //-- Actions ------------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Utility Methods ----------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Static Utility Methods ---------------------------------------------//
    //-----------------------------------------------------------------------//

}
