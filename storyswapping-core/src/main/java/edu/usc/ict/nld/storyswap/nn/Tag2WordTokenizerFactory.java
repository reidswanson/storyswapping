/**
 * 
 */
package edu.usc.ict.nld.storyswap.nn;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.deeplearning4j.models.word2vec.wordstore.VocabCache;
import org.deeplearning4j.text.tokenization.tokenizer.TokenPreProcess;
import org.deeplearning4j.text.tokenization.tokenizer.Tokenizer;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;

import com.google.common.base.Preconditions;

/**
 * @author rswanson
 *
 */
public class Tag2WordTokenizerFactory implements TokenizerFactory {
    //-----------------------------------------------------------------------//
    //-- Nested Classes -----------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Static Fields ------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Managed Properties -------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Properties ---------------------------------------------------------//
    //-----------------------------------------------------------------------//
    //-- Stative ------------------------------------------------------------//
    private String unk;
    private VocabCache<?> vocab;
    private TokenPreProcess tokenPreProcess;

    //-- Functional ---------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Initialization -----------------------------------------------------//
    //-----------------------------------------------------------------------//
    /**
     * 
     * @param tokenPreProcess
     */
    public Tag2WordTokenizerFactory(TokenPreProcess tokenPreProcess, VocabCache<?> vocab, String unk) {
        Preconditions.checkNotNull(tokenPreProcess);
        
        this.tokenPreProcess = tokenPreProcess;
        this.vocab = vocab;
        this.unk = unk;
    }
    
    /**
     * 
     * @param tokenPreProcess
     */
    public Tag2WordTokenizerFactory(TokenPreProcess tokenPreProcess, String unk) {
        this(tokenPreProcess, null, unk);
    }

    //-----------------------------------------------------------------------//
    //-- Getters & Setters --------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Methods ------------------------------------------------------------//
    //-----------------------------------------------------------------------//
    /* (non-Javadoc)
     * @see org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory#create(java.lang.String)
     */
    @Override
    public Tokenizer create(String toTokenize) {
        List<String> tokens =
            Arrays.stream(toTokenize.split("\\s+"))
                .map(t -> new String(t))
                .collect(Collectors.toCollection(ArrayList::new));
        
        return new Tag2WordTokenizer(tokens, tokenPreProcess, vocab, unk);
    }

    /* (non-Javadoc)
     * @see org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory#create(java.io.InputStream)
     */
    @Override
    public Tokenizer create(InputStream toTokenize) {
        throw new UnsupportedOperationException();
    }

    /* (non-Javadoc)
     * @see org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory#setTokenPreProcessor(org.deeplearning4j.text.tokenization.tokenizer.TokenPreProcess)
     */
    @Override
    public void setTokenPreProcessor(TokenPreProcess preProcessor) {
        this.tokenPreProcess = preProcessor;
    }

    /* (non-Javadoc)
     * @see org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory#getTokenPreProcessor()
     */
    @Override
    public TokenPreProcess getTokenPreProcessor() {
        return tokenPreProcess;
    }

    //-----------------------------------------------------------------------//
    //-- Actions ------------------------------------------------------------//
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//
    //-- Utility Methods ----------------------------------------------------//
    //-----------------------------------------------------------------------//

}
